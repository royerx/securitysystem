<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\System\Invoice;

class InvoiceTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Invoice::create([
            'ruc' => '',
            'businessName' => '',
            'address' => '',
            'phone' => '',
            'email' => '',
            'path' => '',
            'ticketSerie' => '001',
            'ticketCorrelative' => '00000001',
            'invoiceSerie' => '001',
            'invoiceCorrelative' => '00000001',
            'creditNoteTicketSerie' => '001',
            'creditNoteTicketCorrelative' => '00000001',
            'creditNoteInvoiceSerie' => '001',
            'creditNoteInvoiceCorrelative' => '00000001',
            'debitNoteTicketSerie' => '001',
            'debitNoteTicketCorrelative' => '00000001',
            'debitNoteInvoiceSerie' => '001',
            'debitNoteInvoiceCorrelative' => '00000001',
            'remittanceSerie' => '001',
            'remittanceCorrelative' => '00000001',
        ]);
    }
}
