<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\System\Igv;

class IgvTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Igv::create([
            'percent' => 18,
            'beginDate' => '2022-01-01',
            'endDate' => '9999-12-31',
        ]);
    }
}
