<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Management\Person;

class PersonTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Person::create([
            'personType' => 'Supplier',
            'firstName' => 'VARIOS',
            'documentType' => 'DNI',
            'documentNumber' => '00000000',
        ]);
    }
}
