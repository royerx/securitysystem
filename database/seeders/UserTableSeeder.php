<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Security\User;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user=User::create([
            'id' => '1',
            'lastName' => 'Admin',
            'firstName' => 'Super',
            'dni' => '00000000',
            'login' => 'admin',
            'picture' => User::DEFAULT_PICTURE,
            'email' => 'royerx.rodriguez@hotmail.com',
            'password' => bcrypt('admin123'),
        ]);
        $user->roles()->attach(1);
    }
}
