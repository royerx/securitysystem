<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Product\Presentation;

class PresentationTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Presentation::create([
            'name' => 'Individual',
            'detail' => 'Presentación de un producto por defecto',
        ]);
    }
}
