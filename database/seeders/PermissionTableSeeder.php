<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Security\Permission;
use App\Models\Security\PermissionAction;

class PermissionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //Permisos para menu
        $permission = Permission::create([
                        'name' => 'Menu Principal',
                        'slug' => 'menu',
                        'description' => 'Permite mostrar los diferentes menus de los módulos',
                    ]);
        $permission->permissionActions()->createMany([
            [
                'name' => 'Menu Ventas',
                'slug' => 'menu.sale',
                'description' => 'Mostrar el menú de administrar ventas',
            ],
            [
                'name' => 'Menu Compras',
                'slug' => 'menu.buy',
                'description' => 'Mostrar el menú de administrar compras',
            ],
            [
                'name' => 'Menu Productos',
                'slug' => 'menu.product',
                'description' => 'Mostrar el menú de administrar productos',
            ],
            [
                'name' => 'Menu Reportes',
                'slug' => 'menu.report',
                'description' => 'Mostrar el menú de reportes',
            ],
            [
                'name' => 'Menu Egresos',
                'slug' => 'menu.expense',
                'description' => 'Mostrar el submenú de seguridad',
            ],
            [
                'name' => 'Menu Seguridad',
                'slug' => 'menu.security',
                'description' => 'Mostrar el submenú de seguridad',
            ],
            [
                'name' => 'Menu Configuración',
                'slug' => 'menu.config',
                'description' => 'Mostrar el submenú de configuración',
            ],
        ]);

        //Permisos para modulo ventas
        $permission = Permission::create([
                        'name' => 'Módulo Ventas',
                        'slug' => 'sales',
                        'description' => 'Permisos para el módulo de ventas',
                    ]);
        $permission->permissionActions()->createMany([
            [
                'name' => 'Lista Ventas',
                'slug' => 'sales.list',
                'description' => 'Listar las ventas registradas y mostrar el submenu Ventas ',
            ],
            [
                'name' => 'Agregar Venta',
                'slug' => 'sales.create',
                'description' => 'Agregar una nueva venta',
            ],
            [
                'name' => 'Editar Venta',
                'slug' => 'sales.update',
                'description' => 'Editar una venta en específico',
            ],
            [
                'name' => 'Mostrar Venta',
                'slug' => 'sales.show',
                'description' => 'Mostrar una venta en específico',
            ],
            [
                'name' => 'Eliminar Venta',
                'slug' => 'sales.delete',
                'description' => 'Eliminar una venta en específico',
            ],
            [
                'name' => 'Agregar Detalle de Venta',
                'slug' => 'sales.createdetail',
                'description' => 'Agregar productos a una venta en específico',
            ],
            [
                'name' => 'Eliminar Detalle de Venta',
                'slug' => 'sales.deletedetail',
                'description' => 'Eliminar un producto de una venta en específico',
            ],
            [
                'name' => 'Imprimir Ticket',
                'slug' => 'sales.voucher',
                'description' => 'Mostrar ticket de una venta en específico en PDF',
            ],
        ]);
        
        //Permisos para modulo clientes
        $permission = Permission::create([
                        'name' => 'Módulo Clientes',
                        'slug' => 'clients',
                        'description' => 'Permisos para el módulo de clientes',
                    ]);
        $permission->permissionActions()->createMany([
            [
                'name' => 'Listar Clientes',
                'slug' => 'clients.list',
                'description' => 'Listar los clientes registrados y mostrar el submenu clientes',
            ],
            [
                'name' => 'Agregar Cliente',
                'slug' => 'clients.create',
                'description' => 'Agregar un nuevo cliente',
            ],
            [
                'name' => 'Editar Cliente',
                'slug' => 'clients.update',
                'description' => 'Editar un cliente en específico',
            ],
            [
                'name' => 'Mostrar Cliente',
                'slug' => 'clients.show',
                'description' => 'Mostrar un cliente en específico',
            ],
            [
                'name' => 'Eliminar Cliente',
                'slug' => 'clients.delete',
                'description' => 'Eliminar un cliente en específico',
            ],
        ]);

        //Permisos para estado de envio
        $permission = Permission::create([
                        'name' => 'Módulo Facturación Electrónica',
                        'slug' => 'sunatinvoices',
                        'description' => 'Permisos para el módulo de Facturación Electrónica',
                    ]);
        $permission->permissionActions()->createMany([
            [
                'name' => 'Listar Documentos Electrónicos Emitidos',
                'slug' => 'sunatinvoices.list',
                'description' => 'Listar los documentos electrónicos emitidos y mostrar el submenu Facturación Electrónica',
            ],
            [
                'name' => 'Imprimir Documento Electrónico',
                'slug' => 'sunatinvoices.voucher',
                'description' => 'Imprimir el Documento Electrónico',
            ],
            [
                'name' => 'Reenviar Documento Electrónico',
                'slug' => 'sunatinvoices.resend',
                'description' => 'Reenviar documento electrónico con errores',
            ],
        ]);
        //Permisos para modulo compras
        $permission = Permission::create([
                        'name' => 'Módulo Compras',
                        'slug' => 'buys',
                        'description' => 'Permisos para el módulo de Compras',
                    ]);
        $permission->permissionActions()->createMany([
            [
                'name' => 'Listar Compras',
                'slug' => 'buys.list',
                'description' => 'Listar las compras realizadas entre fechas y mostrar el submenu compras',
            ],
            [
                'name' => 'Agregar Compra',
                'slug' => 'buys.create',
                'description' => 'Agregar una nueva compra',
            ],
            [
                'name' => 'Editar Compra',
                'slug' => 'buys.update',
                'description' => 'Editar una compra en específico',
            ],
            [
                'name' => 'Mostrar Compra',
                'slug' => 'buys.show',
                'description' => 'Mostrar una compra en específico',
            ],
            [
                'name' => 'Eliminar Compra',
                'slug' => 'buys.delete',
                'description' => 'Eliminar una compra en específico',
            ],
            [
                'name' => 'Agregar Detalle de Compra',
                'slug' => 'buys.createdetail',
                'description' => 'Agregar un producto a una compra en específico',
            ],
            [
                'name' => 'Eliminar Detalle de Compra',
                'slug' => 'buys.deletedetail',
                'description' => 'Eliminar un producto a una compra en específico',
            ],
        ]);

        //Permisos para modulo proveedores
        $permission = Permission::create([
                        'name' => 'Módulo Proveedores',
                        'slug' => 'suppliers',
                        'description' => 'Permisos para el módulo de Proveedores',
                    ]);
        $permission->permissionActions()->createMany([
            [
                'name' => 'Listar Proveedores',
                'slug' => 'suppliers.list',
                'description' => 'Listar los proveedores registrados y mostrar el submenu proveedores',
            ],
            [
                'name' => 'Agregar Proveedor',
                'slug' => 'suppliers.create',
                'description' => 'Agregar un nuevo proveedor',
            ],
            [
                'name' => 'Editar Proveedor',
                'slug' => 'suppliers.update',
                'description' => 'Editar un proveedor en específico',
            ],
            [
                'name' => 'Mostrar Proveedor',
                'slug' => 'suppliers.show',
                'description' => 'Mostrar un proveedor en específico',
            ],
            [
                'name' => 'Eliminar Proveedor',
                'slug' => 'suppliers.delete',
                'description' => 'Eliminar un proveedor en específico',
            ],
        ]);

        //Permisos para modulo productos
        $permission = Permission::create([
                        'name' => 'Módulo Productos',
                        'slug' => 'products',
                        'description' => 'Permisos para el módulo de Productos',
                    ]);
        $permission->permissionActions()->createMany([
             [
                'name' => 'Listar Productos',
                'slug' => 'products.list',
                'description' => 'Listar los productos registrados y mostrar el submenu productos',
            ],
            [
                'name' => 'Agregar Producto',
                'slug' => 'products.create',
                'description' => 'Agregar un nuevo producto',
            ],
            [
                'name' => 'Editar Producto',
                'slug' => 'products.update',
                'description' => 'Editar un producto en específico',
            ],
            [
                'name' => 'Mostrar Producto',
                'slug' => 'products.show',
                'description' => 'Mostrar un producto en específico',
            ],
            [
                'name' => 'Eliminar Producto',
                'slug' => 'products.delete',
                'description' => 'Eliminar un producto en específico',
            ],
            [
                'name' => 'Editar Imagen Producto',
                'slug' => 'products.picture',
                'description' => 'Actualizar la imagen de un producto',
            ],
        ]);

        //Permisos para modulo presentaciones
        $permission = Permission::create([
                        'name' => 'Módulo Presentaciones',
                        'slug' => 'presentations',
                        'description' => 'Permisos para el módulo de Presentaciones',
                    ]);
        $permission->permissionActions()->createMany([
            [
                'name' => 'Listar Presentaciones',
                'slug' => 'presentations.list',
                'description' => 'Listar las presentaciones de productos registradas y mostrar el submenu presentaciones',
            ],
            [
                'name' => 'Agregar Presentación',
                'slug' => 'presentations.create',
                'description' => 'Agregar una nueva presentacion de producto',
            ],
            [
                'name' => 'Editar Presentación',
                'slug' => 'presentations.update',
                'description' => 'Editar una presentacion de producto en específico',
            ],
            [
                'name' => 'Mostrar Presentación',
                'slug' => 'presentations.show',
                'description' => 'Mostrar una presentacion de producto en específico',
            ],
            [
                'name' => 'Eliminar Presentación',
                'slug' => 'presentations.delete',
                'description' => 'Eliminar una presentacion de producto en específico',
            ],
        ]);
        
        //Permisos para modulo categorias
        $permission = Permission::create([
                        'name' => 'Módulo Categorías',
                        'slug' => 'categories',
                        'description' => 'Permisos para el módulo de Categorías',
                    ]);
        $permission->permissionActions()->createMany([
            [
                'name' => 'Listar Categorías',
                'slug' => 'categories.list',
                'description' => 'Listar las categorías registradas y mostrar el submenu categorías',
            ],
            [
                'name' => 'Agregar Categoría',
                'slug' => 'categories.create',
                'description' => 'Agregar una nueva categoría',
            ],
            [
                'name' => 'Editar Categoría',
                'slug' => 'categories.update',
                'description' => 'Editar una categoría en específico',
            ],
            [
                'name' => 'Mostrar Categoría',
                'slug' => 'categories.show',
                'description' => 'Mostrar una categoría en específico',
            ],
            [
                'name' => 'Eliminar Categoría',
                'slug' => 'categories.delete',
                'description' => 'Eliminar una categoría en específico',
            ],
        ]);
        //Permisos para labortorios
        $permission = Permission::create([
                        'name' => 'Módulo Laboratorios',
                        'slug' => 'laboratories',
                        'description' => 'Permisos para el módulo de laboratorios',
                    ]);
        $permission->permissionActions()->createMany([
            [
                'name' => 'Listar Laboratorios',
                'slug' => 'laboratories.list',
                'description' => 'Listar los laboratorios registrados y mostrar el submenu laboratorios',
            ],
            [
                'name' => 'Agregar Laboratorios',
                'slug' => 'laboratories.create',
                'description' => 'Agregar un nuevo laboratorio',
            ],
            [
                'name' => 'Editar Laboratorio',
                'slug' => 'laboratories.update',
                'description' => 'Editar un laboratorio en específico',
            ],
            [
                'name' => 'Mostrar Laboratorio',
                'slug' => 'laboratories.show',
                'description' => 'Mostrar un laboratorio en específico',
            ],
            [
                'name' => 'Eliminar Laboratorio',
                'slug' => 'laboratories.delete',
                'description' => 'Eliminar un laboratorio en específico',
            ],
        ]);

        //Permisos para reportes
        $permission = Permission::create([
                        'name' => 'Módulo Reportes',
                        'slug' => 'reports',
                        'description' => 'Permisos para el módulo de Reportes',
                    ]);
        $permission->permissionActions()->createMany([
            [
                'name' => 'Reporte de Ventas',
                'slug' => 'reports.sales',
                'description' => 'Mostrar submenu para el reporte de ventas',
            ],
            [
                'name' => 'Reporte de Compras',
                'slug' => 'reports.buys',
                'description' => 'Mostrar submenu para el reporte de compras',
            ],
            [
                'name' => 'Reporte de Productos por Día',
                'slug' => 'reports.dayproducts',
                'description' => 'Mostrar submenu para el reporte de productos por dia',
            ],
            [
                'name' => 'Reporte de Productos',
                'slug' => 'reports.products',
                'description' => 'Mostrar submenu para el reporte de productos',
            ],
            [
                'name' => 'Reporte de Egresos',
                'slug' => 'reports.expenses',
                'description' => 'Mostrar submenu para el reporte de egresos',
            ],
            [
                'name' => 'Reporte de Egresos e Ingresos',
                'slug' => 'reports.balance',
                'description' => 'Mostrar submenu para el reporte de egresos e ingresos',
            ],
        ]);

        //Permisos para gastos
        $permission = Permission::create([
                        'name' => 'Módulo Egresos',
                        'slug' => 'expenses',
                        'description' => 'Permisos para el módulo de Egresos',
                    ]);
        $permission->permissionActions()->createMany([
            [
                'name' => 'Listar Egresos',
                'slug' => 'expenses.list',
                'description' => 'Listar los egresos registrados y mostrar el submenu egresos',
            ],
            [
                'name' => 'Agregar Egreso',
                'slug' => 'expenses.create',
                'description' => 'Agregar un nuevo egreso',
            ],
            [
                'name' => 'Editar Egreso',
                'slug' => 'expenses.update',
                'description' => 'Editar un egreso en específico',
            ],
            [
                'name' => 'Mostrar Egreso',
                'slug' => 'expenses.show',
                'description' => 'Mostrar un rol en específico',
            ],
            [
                'name' => 'Eliminar Egreso',
                'slug' => 'expenses.delete',
                'description' => 'Eliminar un egreso en específico',
            ],
        ]);

        //Permisos para modulo usuarios
        $permission = Permission::create([
                        'name' => 'Módulo Usuarios',
                        'slug' => 'users',
                        'description' => 'Permisos para el módulo de usuarios',
                    ]);
        $permission->permissionActions()->createMany([
            [
                'name' => 'Lista Usuarios',
                'slug' => 'users.list',
                'description' => 'Listar los usuarios registrados y mostrar el submenu Usuarios ',
            ],
            [
                'name' => 'Agregar Usuario',
                'slug' => 'users.create',
                'description' => 'Agregar un nuevo usuario',
            ],
            [
                'name' => 'Editar Usuario',
                'slug' => 'users.update',
                'description' => 'Editar un usuario en específico',
            ],
            [
                'name' => 'Mostrar Usuario',
                'slug' => 'users.show',
                'description' => 'Mostrar un usuario en específico',
            ],
            [
                'name' => 'Eliminar Usuario',
                'slug' => 'users.delete',
                'description' => 'Eliminar un usuario en específico',
            ],
            [
                'name' => 'Editar imagen de Usuario',
                'slug' => 'users.picture',
                'description' => 'editar la imagen de un usuario en específico',
            ],
            [
                'name' => 'Asignar Rol',
                'slug' => 'users.role',
                'description' => 'Asignar roles a un usuario',
            ],
        ]);

        //Permisos para modulo roles
        $permission = Permission::create([
                        'name' => 'Módulo Roles',
                        'slug' => 'roles',
                        'description' => 'Permisos para el módulo de roles',
                    ]);
        $permission->permissionActions()->createMany([
            [
                'name' => 'Lista Roles',
                'slug' => 'roles.list',
                'description' => 'Listar los roles registrados y mostrar el submenu Roles ',
            ],
            [
                'name' => 'Agregar Rol',
                'slug' => 'roles.create',
                'description' => 'Agregar un nuevo rol',
            ],
            [
                'name' => 'Editar Rol',
                'slug' => 'roles.update',
                'description' => 'Editar un rol en específico',
            ],
            [
                'name' => 'Mostrar Rol',
                'slug' => 'roles.show',
                'description' => 'Mostrar un rol en específico',
            ],
            [
                'name' => 'Eliminar Rol',
                'slug' => 'roles.delete',
                'description' => 'Eliminar un rol en específico',
            ],
            [
                'name' => 'Asignar Permisos',
                'slug' => 'roles.permissions',
                'description' => 'Asignar permisos a un rol',
            ],
        ]);

        //Permisos para modulo parametros de facturacion
        $permission = Permission::create([
                        'name' => 'Parámetros de Facturación',
                        'slug' => 'invoice',
                        'description' => 'Permisos para el módulo de parametros de facturación',
                    ]);
        $permission->permissionActions()->createMany([
            [
                'name' => 'Lista Parametros',
                'slug' => 'invoice.list',
                'description' => 'Listar los parametros de facturación registrados y mostrar el submenu facturación',
            ],
            [
                'name' => 'Editar Parametros',
                'slug' => 'invoice.update',
                'description' => 'Editar parametros de facturación',
            ],
            [
                'name' => 'Mostrar Parametros',
                'slug' => 'invoice.show',
                'description' => 'Mostrar paramtros de facturación',
            ],
        ]);
        //Permisos para IGV
        $permission = Permission::create([
                        'name' => 'Módulo IGV',
                        'slug' => 'igvs',
                        'description' => 'Permisos para el módulo de IGV',
                    ]);
        $permission->permissionActions()->createMany([
            [
                'name' => 'Listar IGVs',
                'slug' => 'igvs.list',
                'description' => 'Listar los IGVs registrados y mostrar el submenu IGVs',
            ],
            [
                'name' => 'Agregar IGV',
                'slug' => 'igvs.create',
                'description' => 'Agregar un nuevo IGV',
            ],
            [
                'name' => 'Editar IGV',
                'slug' => 'igvs.update',
                'description' => 'Editar un IGV en específico',
            ],
            [
                'name' => 'Mostrar IGV',
                'slug' => 'igvs.show',
                'description' => 'Mostrar un IGV en específico',
            ],
            [
                'name' => 'Eliminar IGV',
                'slug' => 'igvs.delete',
                'description' => 'Eliminar un IGV en específico',
            ],
        ]);
    }
}
