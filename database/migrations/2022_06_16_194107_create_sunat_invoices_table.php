<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSunatInvoicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sunat_invoices', function (Blueprint $table) {
            $table->id();
            $table->string('type');
            $table->string('serie',4);
            $table->string('correlative',8);
            $table->string('documentNumber');
            $table->date('date');
            $table->time('time');
            $table->date('dueDate')->nullable();
            $table->string('currency');
            $table->string('wayPay');
            $table->string('quota');
            $table->double('quotaAmount',18,2);
            $table->string('methodPay')->nullable();
            $table->double('totalSaleGravada',18,2);
            $table->double('totalExonerated',18,2);
            $table->double('totalUnaffected',18,2);
            $table->double('igv',18,2);
            $table->double('total',18,2);
            $table->string('retention',2);
            $table->double('amountRetention',18,2);
            $table->double('pendingPay',18,2);
            $table->string('hash')->nullable();
            $table->string('reference')->nullable();
            $table->string('state')->nullable();
            $table->boolean('cancel');
            $table->unsignedBigInteger('sale_id')->nullable();
            $table->foreign('sale_id')->references('id')->on('sales')->onDelete('restrict');
            $table->unsignedBigInteger('user_id');
            $table->foreign('user_id')->references('id')->on('users')->onDelete('restrict');
            $table->unsignedBigInteger('client_id');
            $table->foreign('client_id')->references('id')->on('people')->onDelete('restrict');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sunat_invoices');
    }
}
