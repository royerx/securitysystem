<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSunatInvoiceDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sunat_invoice_details', function (Blueprint $table) {
            $table->id();
            $table->string('measure');
            $table->string('tax');
            $table->double('amountTax',18,2);
            $table->string('typeTaxAllocation');
            $table->double('unitPrice',18,2);
            $table->double('subTotal',18,2);
            $table->double('price',18,2);
            $table->double('quantity',18,4);
            $table->unsignedBigInteger('sunat_invoice_id');
            $table->foreign('sunat_invoice_id')->references('id')->on('sunat_invoices')->onDelete('restrict');
            $table->unsignedBigInteger('sale_detail_id')->nullable();
            $table->foreign('sale_detail_id')->references('id')->on('sale_details')->onDelete('restrict');
            $table->unsignedBigInteger('presentation_product_id');
            $table->foreign('presentation_product_id')->references('id')->on('presentation_product')->onDelete('restrict');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sunat_invoice_details');
    }
}
