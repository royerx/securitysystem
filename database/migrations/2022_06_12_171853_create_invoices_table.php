<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInvoicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('invoices', function (Blueprint $table) {
            $table->id();
            $table->string('ruc',11);
            $table->string('businessName');
            $table->string('address');
            $table->string('phone');
            $table->string('email');
            $table->string('path');
            $table->string('ticketSerie',3);
            $table->string('ticketCorrelative',8);
            $table->string('invoiceSerie',3);
            $table->string('invoiceCorrelative',8);
            $table->string('creditNoteTicketSerie',3);
            $table->string('creditNoteTicketCorrelative',8);
            $table->string('creditNoteInvoiceSerie',3);
            $table->string('creditNoteInvoiceCorrelative',8);
            $table->string('debitNoteTicketSerie',3);
            $table->string('debitNoteTicketCorrelative',8);
            $table->string('debitNoteInvoiceSerie',3);
            $table->string('debitNoteInvoiceCorrelative',8);
            $table->string('remittanceSerie',3);
            $table->string('remittanceCorrelative',8);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('invoices');
    }
}
