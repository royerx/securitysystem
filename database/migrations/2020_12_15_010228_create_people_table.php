<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePeopleTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('people', function (Blueprint $table) {
            $table->id();
            $table->enum('personType', ['supplier', 'client']);
            $table->string('lastName', 100)->nullable();
            $table->string('firstName', 100);
            $table->enum('documentType', ['DNI', 'RUC','Otro']);
            $table->string('documentNumber', 11)->unique();
            $table->string('address')->nullable();
            $table->string('phone',15)->nullable();
            $table->string('email')->nullable();
            $table->string('picture')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('people');
    }
}
