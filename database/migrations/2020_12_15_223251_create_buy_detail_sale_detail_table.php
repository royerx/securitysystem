<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBuyDetailSaleDetailTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('buy_detail_sale_detail', function (Blueprint $table) {
            $table->id();
            $table->double('quantitySale',18,4);
            $table->unsignedBigInteger('buy_detail_id');
            $table->foreign('buy_detail_id')->references('id')->on('buy_details')->onDelete('restrict');
            $table->unsignedBigInteger('sale_detail_id');
            $table->foreign('sale_detail_id')->references('id')->on('sale_details')->onDelete('restrict');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_details');
    }
}
