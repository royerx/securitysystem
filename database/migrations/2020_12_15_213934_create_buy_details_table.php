<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBuyDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('buy_details', function (Blueprint $table) {
            $table->id();
            $table->double('price',18,2);
            $table->double('quantity',18,4);
            $table->date('dueDate');
            $table->double('quantityAviable',18,4);
            $table->double('quantitySale',18,4);
            $table->unsignedBigInteger('buy_id');
            $table->foreign('buy_id')->references('id')->on('buys')->onDelete('restrict');
            $table->unsignedBigInteger('presentation_product_id');
            $table->foreign('presentation_product_id')->references('id')->on('presentation_product')->onDelete('restrict');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('buy_details');
    }
}
