<?php

namespace App\Providers;

use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Gate;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'App\Models\Security\Role' => 'App\Policies\Security\RolePolicy',
        'App\Models\Security\Permission' => 'App\Policies\Security\PermissionPolicy',
        'App\Models\Security\User' => 'App\Policies\Security\UserPolicy',
        'App\Models\Product\Product' => 'App\Policies\Product\ProductPolicy',
        'App\Models\Product\Presentation' => 'App\Policies\Product\PresentationPolicy',
        'App\Models\Product\Category' => 'App\Policies\Product\CategoryPolicy',
        'App\Models\Product\Laboratory' => 'App\Policies\Product\LaboratoryPolicy',
        'App\Models\Entry\Supplier' => 'App\Policies\Entry\SupplierPolicy',
        'App\Models\Entry\Buy' => 'App\Policies\Entry\BuyPolicy',
        'App\Models\Sale\Client' => 'App\Policies\Sale\ClientPolicy',
        'App\Models\Sale\Sale' => 'App\Policies\Sale\SalePolicy',
        'App\Models\Account\Expense' => 'App\Policies\Account\ExpensePolicy',
        'App\Models\System\Invoice' => 'App\Policies\System\InvoicePolicy',
        'App\Models\System\Igv' => 'App\Policies\System\IgvPolicy',
        'App\Models\System\SunatInvoice' => 'App\Policies\System\SunatInvoicePolicy',
        'App\Models\System\Condition' => 'App\Policies\System\ConditionPolicy',

    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        //
    }
}
