<?php

namespace App\Http\Requests\System\Invoice;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
use Illuminate\Support\Str;

class StoreRequest extends FormRequest
{
    /**
     * determina si un usuario esta  autorizado para ejecutar este request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Obtiene un array con las validaciones
     *
     * @return array
     */
    public function rules()
    {
        return [
            'ruc' => array("required","digits:11"),
            'businessName' => array("required","regex:/^([0-9a-zA-ZñÑáéíóúÁÉÍÓÚ_-])+((\s*)+([0-9a-zA-ZñÑáéíóúÁÉÍÓÚ\/'°&".Str::ascii('"')."#%&,._-]*)*)+$/","max:255"),
            'address' => array("required","regex:/^([0-9a-zA-ZñÑáéíóúÁÉÍÓÚ_-])+((\s*)+([0-9a-zA-ZñÑáéíóúÁÉÍÓÚ\/'°&".Str::ascii('"')."#%&,._-]*)*)+$/","max:255"),
            'phone' => array("required","digits_between:6,15"),
            'email' => array("required","email","max:255"),
            'path' => array("required","max:255"),
            'ticketSerie' => array("required","digits:3"),
            'ticketCorrelative' => array("required","digits:8"),
            'invoiceSerie' => array("required","digits:3"),
            'invoiceCorrelative' => array("required","digits:8"),
            'creditNoteTicketSerie' => array("required","digits:3"),
            'creditNoteTicketCorrelative' => array("required","digits:8"),
            'creditNoteInvoiceSerie' => array("required","digits:3"),
            'creditNoteInvoiceCorrelative' => array("required","digits:8"),
            'debitNoteTicketSerie' => array("required","digits:3"),
            'debitNoteTicketCorrelative' => array("required","digits:8"),
            'debitNoteInvoiceSerie' => array("required","digits:3"),
            'debitNoteInvoiceCorrelative' => array("required","digits:8"),
            'remittanceSerie' => array("required","digits:3"),
            'remittanceCorrelative' => array("required","digits:8"),
        ];
    }
    
    /**
     * Obtiene un array con los mensajes para los diferentes tipos de validaciones
     *
     * @return array
     */
    public function messages()
    {
        return[
            'ruc.required' => 'Debe de ingresar el RUC',
            'ruc.digits' => 'El RUC debe de tener 11 dígitos',
            'businessName.required' => 'Debe de ingresar la razón  social',
            'businessName.regex' => 'El formato del texto es inválido',
            'businessName.max' => 'No debe de superar los 255 caracteres',
            'address.required' => 'Debe de ingresar la dirección',
            'address.regex' => 'El formato del texto es inválido',
            'address.max' => 'No debe de superar los 255 caracteres',
            'phone.required' => 'Debe de ingresar el telefono',
            'phone.digits_between' => 'El telefono debe de tener entre 6 y 15 dígitos',
            'email.required' => 'Debe de ingresar el email',
            'email.email' => 'El formato de correo es inválido',
            'email.max' => 'El correr no debe de superar los 255 caracteres',
            'path.required' => 'Debe de ingresar la ruta del facturador',
            'path.max' => 'No debe de superar los 255 caracteres',
            
            'ticketSerie.required' => 'Debe de ingresar la serie',
            'ticketSerie.digits' => 'Formato inválido',
            'ticketCorrelative.required' => 'Debe de ingresar correlativo',
            'ticketCorrelative.digits' => 'Solo dígitos',

            'invoiceSerie.required' => 'Debe de ingresar la serie',
            'invoiceSerie.digits' => 'Formato inválido',
            'invoiceCorrelative.required' => 'Debe de ingresar correlativo',
            'invoiceCorrelative.digits' => 'Solo dígitos',

            'creditNoteTicketSerie.required' => 'Debe de ingresar la serie',
            'creditNoteTicketSerie.digits' => 'Formato inválido',
            'creditNoteTicketCorrelative.required' => 'Debe de ingresar correlativo',
            'creditNoteTicketCorrelative.digits' => 'Solo dígitos',

            'creditNoteInvoiceSerie.required' => 'Debe de ingresar la serie',
            'creditNoteInvoiceSerie.digits' => 'Formato inválido',
            'creditNoteInvoiceCorrelative.required' => 'Debe de ingresar correlativo',
            'creditNoteInvoiceCorrelative.digits' => 'Solo dígitos',

            'debitNoteTicketSerie.digits' => 'Debe de ingresar la serie',
            'debitNoteTicketSerie.regex' => 'Formato inválido',
            'debitNoteTicketCorrelative.required' => 'Debe de ingresar correlativo',
            'debitNoteTicketCorrelative.digits' => 'Solo dígitos',

            'debitNoteInvoiceSerie.required' => 'Debe de ingresar la serie',
            'debitNoteInvoiceSerie.digits' => 'Formato inválido',
            'debitNoteInvoiceCorrelative.required' => 'Debe de ingresar correlativo',
            'debitNoteInvoiceCorrelative.digits' => 'Solo dígitos',

            'remittanceSerie.required' => 'Debe de ingresar la serie',
            'remittanceSerie.digits' => 'Formato inválido',
            'remittanceCorrelative.required' => 'Debe de ingresar correlativo',
            'remittanceCorrelative.digits' => 'Solo dígitos',
        ];
    }
}
