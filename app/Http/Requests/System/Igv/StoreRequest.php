<?php

namespace App\Http\Requests\System\Igv;

use Illuminate\Foundation\Http\FormRequest;

class StoreRequest extends FormRequest
{
    /**
     * determina si un usuario esta  autorizado para ejecutar este request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Obtiene un array con las validaciones
     *
     * @return array
     */
    public function rules()
    {
        return [
            'percent' => array("required","regex:/^[\d]{0,5}(\.[\d]{1,2})?$/"),
            'beginDate' => array("required","date"),
            'endDate' => array("required","date"),
        ];
    }
    
    /**
     * Obtiene un array con los mensajes para los diferentes tipos de validaciones
     *
     * @return array
     */
    public function messages()
    {
        return[
            'percent.required' => 'Debe de ingresar el porcentaje',
            'percent.regex' => 'Formato no válido',
            'beginDate.required' => 'Debe de ingresar la fecha inicial',
            'beginDate.date' => 'Debe de ingresar una fecha válida',
            'endDate.required' => 'Debe de ingresar la fecha final',
            'endDate.date' => 'Debe de ingresar una fecha válida',
        ];
    }
}
