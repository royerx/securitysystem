<?php

namespace App\Http\Requests\Report;

use Illuminate\Foundation\Http\FormRequest;

class ReportRequest extends FormRequest
{
    /**
     * determina si un usuario esta  autorizado para ejecutar este request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Obtiene un array con las validaciones
     *
     * @return array
     */
    public function rules()
    {
        return [
            'beginDate' => array("required","date"),
            'endDate' => array("required","date"),
        ];
    }
    
    /**
     * Obtiene un array con los mensajes para los diferentes tipos de validaciones
     *
     * @return array
     */
    public function messages()
    {
        return[
            'beginDate.required' => 'Debe de ingresar la fecha incial',
            'beginDate.date' => 'Debe de ingresar una fecha válida',
            'endDate.required' => 'Debe de ingresar la fecha final',
            'endDate.date' => 'Debe de ingresar una fecha válida',
        ];
    }
}
