<?php

namespace App\Http\Requests\Product\Presentation;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Str;

class UpdateRequest extends FormRequest
{
    /**
     * determina si un usuario esta  autorizado para ejecutar este request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Obtiene un array con las validaciones
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => array("required","regex:/^([0-9a-zA-ZñÑáéíóúÁÉÍÓÚ_-])+((\s*)+([0-9a-zA-ZñÑáéíóúÁÉÍÓÚ\/'°".Str::ascii('"')."#%&,._-]*)*)+$/","max:100","unique:presentations,name,".$this->route('presentation')->id),
            'detail' => array("nullable","regex:/^([0-9a-zA-ZñÑáéíóúÁÉÍÓÚ_-])+((\s*)+([0-9a-zA-ZñÑáéíóúÁÉÍÓÚ\/'°".Str::ascii('"')."#%&,._-]*)*)+$/","max:255") 
        ];
    }
    
    /**
     * Obtiene un array con los mensajes para los diferentes tipos de validaciones
     *
     * @return array
     */
    public function messages()
    {
        return[
            'name.required' => 'Debe de ingresar el nombre de la presentación',
            'name.regex' => 'El nombre tiene caracteres no válidos',
            'name.unique' => 'El nombre ya se encuentra registrado',
            'name.max' => 'El nombre no debe de superar los 100 caracteres',
            'description.regex' => 'La descripción tiene caracteres no válidos',
            'description.max' => 'La descripción no debe de superar los 255 caracteres'
        ];
    }
}
