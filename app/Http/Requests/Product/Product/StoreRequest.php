<?php

namespace App\Http\Requests\Product\Product;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Str;

class StoreRequest extends FormRequest
{
    /**
     * determina si un usuario esta  autorizado para ejecutar este request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Obtiene un array con las validaciones
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => array("required","regex:/^([0-9a-zA-ZñÑáéíóúÁÉÍÓÚ_-])+((\s*)+([0-9a-zA-ZñÑáéíóúÁÉÍÓÚ\/'°".Str::ascii('"')."#%&,._-]*)*)+$/","max:255"),
            'minStock' => array("required","regex:/^[\d]{0,16}(\.[\d]{1,2})?$/"),
            'code' => array("required","unique:products","digits_between:4,10"),
            'detail' => array("nullable","regex:/^([0-9a-zA-ZñÑáéíóúÁÉÍÓÚ_-])+((\s*)+([0-9a-zA-ZñÑáéíóúÁÉÍÓÚ\/'°".Str::ascii('"')."#%&,._-]*)*)+$/","max:255"), 
            'buyPrice' => array("required","regex:/^[\d]{0,16}(\.[\d]{1,2})?$/"),
            'salePrice' => array("required","regex:/^[\d]{0,16}(\.[\d]{1,2})?$/","gt:buyPrice"),
        ];
    }
    
    /**
     * Obtiene un array con los mensajes para los diferentes tipos de validaciones
     *
     * @return array
     */
    public function messages()
    {
        return[
            'name.required' => 'Debe de ingresar el nombre del producto',
            'name.regex' => 'El nombre tiene caracteres no válidos',
            'name.max' => 'El nombre no debe de superar los 255 caracteres',
            'minStock.required' => 'Debe de ingresar el stock mínimo',
            'minStock.regex' => 'solo se aceptan números con máximo 2 decimales',
            'code.required' => 'Solo debe de ingresar dígitos',
            'code.digits_between' => 'Mínimo 4 dígitos',
            'code.unique' => 'El código ya a sido registrado',
            'description.regex' => 'La descripción tiene caracteres no válidos',
            'description.max' => 'La descripción no debe de superar los 255 caracteres',
            'buyPrice.required' => 'Debe de ingresar el precio de compra',
            'buyPrice.regex' => 'solo se aceptan números con máximo 2 decimales',
            'salePrice.required' => 'Debe de ingresar el precio de venta',
            'salePrice.regex' => 'solo se aceptan números con máximo 2 decimales',
            'salePrice.gt' => 'Tiene que ser mayor que el precio de compra',
        ];
    }
}
