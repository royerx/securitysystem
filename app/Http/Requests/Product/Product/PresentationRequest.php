<?php

namespace App\Http\Requests\Product\Product;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Str;

class PresentationRequest extends FormRequest
{
    /**
     * determina si un usuario esta  autorizado para ejecutar este request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Obtiene un array con las validaciones
     *
     * @return array
     */
    public function rules()
    {
        return [
            'quantity' => array("required","regex:/^[\d]{0,16}(\.[\d]{1,2})?$/"),
            'buyPrice' => array("required","regex:/^[\d]{0,16}(\.[\d]{1,2})?$/"),
            'salePrice' => array("required","regex:/^[\d]{0,16}(\.[\d]{1,2})?$/","gt:buyPrice"),
        ];
    }
    
    /**
     * Obtiene un array con los mensajes para los diferentes tipos de validaciones
     *
     * @return array
     */
    public function messages()
    {
        return[
            'quantity.required' => 'Debe de ingresar la cantidad',
            'quantity.regex' => 'solo se aceptan números con máximo 2 decimales',
            'buyPrice.required' => 'Debe de ingresar el precio de compra',
            'salePrice.regex' => 'solo se aceptan números con máximo 2 decimales',
            'buyPrice.required' => 'Debe de ingresar el precio de venta',
            'salePrice.regex' => 'solo se aceptan números con máximo 2 decimales',
            'salePrice.gt' => 'Tiene que ser mayor que el precio de compra',
        ];
    }
}
