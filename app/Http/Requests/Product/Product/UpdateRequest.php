<?php

namespace App\Http\Requests\Product\Product;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Str;

class UpdateRequest extends FormRequest
{
    /**
     * determina si un usuario esta  autorizado para ejecutar este request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Obtiene un array con las validaciones
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => array("required","regex:/^([0-9a-zA-ZñÑáéíóúÁÉÍÓÚ_-])+((\s*)+([0-9a-zA-ZñÑáéíóúÁÉÍÓÚ\/'°".Str::ascii('"')."#%&,._-]*)*)+$/","max:255"),
            'code' => array("required","digits_between:4,10","unique:products,code,".$this->route('product')->id),
            'minStock' => array("required","between:0,9999999999999999.99"),
            'detail' => array("nullable","regex:/^([0-9a-zA-ZñÑáéíóúÁÉÍÓÚ_-])+((\s*)+([0-9a-zA-ZñÑáéíóúÁÉÍÓÚ\/'°".Str::ascii('"')."#%&,._-]*)*)+$/","max:255") ,
        ];
    }
    
    /**
     * Obtiene un array con los mensajes para los diferentes tipos de validaciones
     *
     * @return array
     */
    public function messages()
    {
        return[
            'name.required' => 'Debe de ingresar el nombre del producto',
            'name.regex' => 'El nombre tiene caracteres no válidos',
            'name.max' => 'El nombre no debe de superar los 255 caracteres',
            'minStock.required' => 'Debe de ingresar el nombre de la presentación',
            'minStock.between' => 'solo se aceptan números con 2 decimales',
            'code.required' => 'Solo debe de ingresar dígitos',
            'code.digits_between' => 'Mínimo 4 dígitos',
            'code.unique' => 'El código ya a sido registrado',
            'description.regex' => 'La descripción tiene caracteres no válidos',
            'description.max' => 'La descripción no debe de superar los 255 caracteres',
        ];
    }
}
