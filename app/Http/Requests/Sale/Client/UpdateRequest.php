<?php

namespace App\Http\Requests\Sale\Client;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
use Illuminate\Support\Str;

class UpdateRequest extends FormRequest
{
    /**
     * determina si un usuario esta  autorizado para ejecutar este request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Obtiene un array con las validaciones
     *
     * @return array
     */
    public function rules()
    {
        return [
            'lastName' => array("nullable","regex:/^([0-9a-zA-ZñÑáéíóúÁÉÍÓÚ_-])+((\s*)+([0-9a-zA-ZñÑáéíóúÁÉÍÓÚ\/'°".Str::ascii('"')."#%&,._-]*)*)+$/","max:100"),
            'firstName' => array("required","regex:/^([0-9a-zA-ZñÑáéíóúÁÉÍÓÚ_-])+((\s*)+([0-9a-zA-ZñÑáéíóúÁÉÍÓÚ\/'°".Str::ascii('"')."#%&,._-]*)*)+$/","max:100"),
            'documentType' => array("required",Rule::in(['DNI', 'RUC','Otro'])),
            'documentNumber' => array("required","digits_between:8,11","unique:people,documentNumber,".$this->route('client')->id),
            'address' => array("nullable","regex:/^([0-9a-zA-ZñÑáéíóúÁÉÍÓÚ_-])+((\s*)+([0-9a-zA-ZñÑáéíóúÁÉÍÓÚ\/'°".Str::ascii('"')."#%&,._-]*)*)+$/","max:255"),
            'phone' => array("nullable","digits_between:6,15"),
            'photo' => array("nullable","image"),
            'email' => array("nullable","email","max:255","unique:people,email,".$this->route('client')->id),
        ];
    }
    
    /**
     * Obtiene un array con los mensajes para los diferentes tipos de validaciones
     *
     * @return array
     */
    public function messages()
    {
        return[
            //validación apellidos
            'lastName.regex' => 'El apellido tiene caracteres no válidos',
            'lastName.max' => 'El apellido no debe de superar los 100 caracteres',
            //validacion nombres
            'firstName.required' => 'Debe de ingresar el nombre del usuario',
            'firstName.regex' => 'El apellido tiene caracteres no válidos',
            'firstName.max' => 'El nombre no debe de superar los 100 caracteres',
            //validacion  tipo de documento
            'documentType.required' => 'Debe de ingresar el Tipo de Documento',
            'documentType.in' => 'El Tipo de Documento solo debe ser DNI,RUC u Otro',
            //validacion numero de documento
            'documentNumber.required' => 'Debe de ingresar el número de documento',
            'documentNumber.unique' => 'El número de documento ya se encuentra registrado',
            'documentNumber.digits_between' => 'El número de documento debe de tener entre 8 y 11 dígitos',
            //validacion dirección
            'address.regex' => 'La dirección tiene caracteres no válidos',
            'address.max' => 'La dirección no debe de superar los 255 caracteres',
            //validacion telefono
            'phone.digits_between' => 'El telefono debe de tener entre 6 y 15 dígitos',
            //validacion foto
            'photo.image' =>'El archivo debe de ser imagen',
            //validacion email
            'email.unique' => 'El correo ya se encuentra registrado',
            'email.email' => 'El formato de correo es inválido',
            'email.max' => 'El correr no debe de superar los 255 caracteres',
        ];
    }
}
