<?php

namespace App\Http\Requests\Sale\Sale;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Str;

class StoreRequest extends FormRequest
{
    /**
     * determina si un usuario esta  autorizado para ejecutar este request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Obtiene un array con las validaciones
     *
     * @return array
     */
    public function rules()
    {
        return [
            'date' => array("required","date"),
            'type' => array("required","regex:/^([0-9a-zA-ZñÑáéíóúÁÉÍÓÚ_-])+((\s*)+([0-9a-zA-ZñÑáéíóúÁÉÍÓÚ\/'°".Str::ascii('"')."#%&,._-]*)*)+$/","max:50"),
            'serie' => array("required","regex:/^[A-Z]\d{1,3}$|^\d{1,4}$/"),
            'correlative' => array("required","digits_between:1,8"),
            'detail' => array("nullable","regex:/^([0-9a-zA-ZñÑáéíóúÁÉÍÓÚ_-])+((\s*)+([0-9a-zA-ZñÑáéíóúÁÉÍÓÚ\/'°".Str::ascii('"')."#%&,._-]*)*)+$/","max:255"),
        ];
    }
    
    /**
     * Obtiene un array con los mensajes para los diferentes tipos de validaciones
     *
     * @return array
     */
    public function messages()
    {
        return[
            'date.required' => 'Debe de ingresar la fecha de la compra',
            'date.date' => 'Debe de ingresar una fecha válida',
            'type.required' => 'Debe de ingresar el tipo de documento',
            'type.regex' => 'El tipo de documento tiene caracteres no válidos',
            'type.max' => 'El tipo de documento no debe de superar los 50 caracteres',
            'serie.required' => 'Debe de ingresar la serie del documento',
            'serie.digits_between' => 'La serie del documento no es válida',
            'correlative.required' => 'Debe de ingresar el correlativo del documento',
            'correlative.digits_between' => 'El correlativo del documento debe de tener entre 1 y 8 dígitos',
            'detail.regex' => 'El formato del texto es inválido',
            'detail.max' => 'El detalle no debe de superar los 255 caracteres',
        ];
    }
}
