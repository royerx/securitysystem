<?php

namespace App\Http\Requests\Account\Expense;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Str;
use Illuminate\Validation\Rule;

class StoreRequest extends FormRequest
{
    /**
     * determina si un usuario esta  autorizado para ejecutar este request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Obtiene un array con las validaciones
     *
     * @return array
     */
    public function rules()
    {
        return [
            'type' => array("required",Rule::in(['Fijo', 'Variable'])),
            'name' => array("required","regex:/^([0-9a-zA-ZñÑáéíóúÁÉÍÓÚ_-])+((\s*)+([0-9a-zA-ZñÑáéíóúÁÉÍÓÚ\/'°".Str::ascii('"')."#%&,._-]*)*)+$/","max:255"),
            'amount' => array("required","regex:/^[\d]{0,16}(\.[\d]{1,2})?$/"),
            'beginDate' => array("required","date"),
            'endDate' => array("nullable","date"),
        ];
    }
    
    /**
     * Obtiene un array con los mensajes para los diferentes tipos de validaciones
     *
     * @return array
     */
    public function messages()
    {
        return[
            'type.required' => 'Debe de ingresar el tipo de egreso',
            'type.rule' => 'Debe de ingresar si es Fijo o Variable',
            'name.required' => 'Debe de ingresar el nombre del egreso',
            'name.regex' => 'El egreso tiene caracteres no válidos',
            'amount.required' => 'Debe de ingresar el monto',
            'amount.regex' => 'solo se aceptan números con máximo 2 decimales',
            'correlative.required' => 'Debe de ingresar el correlativo del documento',
            'correlative.digits_between' => 'El correlativo del documento debe de tener entre 6 y 8 dígitos',
            'detail.regex' => 'El formato del texto es inválido',
            'detail.max' => 'El detalle no debe de superar los 255 caracteres',
            'beginDate.required' => 'Debe de ingresar la fecha',
            'beginDate.date' => 'Debe de ingresar una fecha válida',
            'endDate.date' => 'Debe de ingresar una fecha válida',
        ];
    }
}
