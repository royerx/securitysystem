<?php

namespace App\Http\Controllers\Entry;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Entry\Supplier;
use App\Http\Requests\Entry\Supplier\StoreRequest;
use App\Http\Requests\Entry\Supplier\UpdateRequest;
use Illuminate\Support\Facades\DB;

class SupplierController extends Controller
{
    public function __construct()
    {
        $this->authorizeResource(Supplier::class, 'supplier');
    }

    #-------------------------------------------------------
    #METODOS DE RECURSOS CON AUTORIZACION: authorizeResource
    #-------------------------------------------------------

    /**
     * Listar proveedores
     * 
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response: Json con lista de proveedores
     */    
    public function index(Request $request)
    {
        if($request->ajax()){
            $suppliers = Supplier::orderBy('id','DESC')
                            ->firstName($request->find)
                            ->lastName($request->find)
                            ->documentNumber($request->find)
                            ->where('personType','supplier')
                            ->paginate(env("PAGE_NUMBER",20));
            
            return  response()->json($suppliers,200);
            
        }

        return redirect('home');
        
    }

    /**
     * Guardar un proveedor
     *
     * @param  \App\Http\Requests\Entry\Supplier\StoreRequest  $request: request para guardado
     * @param  \App\Models\Entry\Supplier  $supplier
     * @return \Illuminate\Http\Response: Json con mensaje de confirmacion y proveedor guardado
     */
    public function store(StoreRequest $request,Supplier $supplier)
    {
        if($request->ajax()){
            $supplier= $supplier->store($request);
            return response()->json([
                'message' => 'Proveedor creado correctamente.',
                'supplier' => $supplier
            ],200);
        }

        return redirect('home');
    }

    /**
     * Mostrar un proveedor
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Models\Entry\Supplier  $supplier
     * @return \Illuminate\Http\Response: Json con el proveedor encontrado
     */
    public function show(Request $request,Supplier $supplier)
    {
        if($request->ajax()){
            return response()->json([
                'supplier' => $supplier
            ],200);
        }

        return redirect('home');
    }

    /**
     * Modificar un proveedor
     *
     * @param  \App\Http\Requests\Entry\Supplier\UpdateRequest  $request: request para modificado
     * @param  \App\Models\Entry\Supplier  $supplier
     * @return \Illuminate\Http\Response: Json con mensaje de confirmacion y el proveedor modificado
     */
    public function update(UpdateRequest $request, Supplier $supplier)
    {
        if($request->ajax()){
            if($request->documentType=="RUC")
                $request['lastName']="";
            $supplier=$supplier->update($request->all());
            return response()->json([
                'message' => 'Proveedor actualizado correctamente.',
                'supplier' => $supplier
            ],200);
        }

        return redirect('home');
    }

    /**
     * Eliminar un proveedor
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Models\Entry\Supplier  $supplier
     * @return \Illuminate\Http\Response: Json con mensaje de confirmacion
     */
    public function destroy(Request $request,Supplier $supplier)
    {
        if($request->ajax()){
            $supplier=$supplier->delete();
            return response()->json([
                'message' => 'Proveedor eliminado correctamente.',
                'supplier' => $supplier
            ],200);
        }

        return redirect('home');
    }

    #-----------------------------------
    #METODOS QUE REQUIEREN AUTORIZACION
    #-----------------------------------

    /**
     * Listar todas los proveedores dada una cantidad determinada
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response: Json con mensaje de confirmacion con la lista de proveedores
     */
    public function list(Request $request)
    {
        $this->authorize('list', Supplier::class);
        if($request->ajax()){

            if($request->quantity)
                $suppliers = Supplier::orderBy('id','DESC')
                            ->firstName($request->find)
                            ->lastName($request->find)
                            ->documentNumber($request->find)
                            ->where('personType','supplier')
                            ->take($request->quantity)->get();
            else
                $suppliers = Supplier::orderBy('id','DESC')
                            ->firstName($request->find)
                            ->lastName($request->find)
                            ->documentNumber($request->find)
                            ->where('personType','supplier')
                            ->get();

            return  response()->json($suppliers,200);
        }
        
        return redirect('home');
    }
}
