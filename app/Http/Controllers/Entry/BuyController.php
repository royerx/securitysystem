<?php

namespace App\Http\Controllers\Entry;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Entry\Buy;
use App\Models\Entry\BuyDetail;
use App\Http\Requests\Entry\Buy\StoreRequest;

use App\Models\Product\Product;

class BuyController extends Controller
{
    public function __construct()
    {
        $this->authorizeResource(Buy::class, 'buy');
    }

    #-------------------------------------------------------
    #METODOS DE RECURSOS CON AUTORIZACION: authorizeResource
    #-------------------------------------------------------

    /**
     * Listar compras
     * 
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response: Json con lista de compras
     */    
    public function index(Request $request)
    {
        if($request->ajax()){
            Buy::doesntHave('buyDetails')->delete();
            $buys=Buy::orderBy('id','DESC')->supplier($request->find)->serie($request->find)->correlative($request->find)
                                ->whereBetween('date',[$request->beginDate,$request->endDate])
                                ->with('supplier','user')
                                ->paginate(env("PAGE_NUMBER",20));
            return  response()->json($buys,200);
        }

        return redirect('home');
        
    }

    /**
     * Guardar una compra
     *
     * @param  \App\Http\Requests\Entry\Buy\StoreRequest  $request: request para guardado
     * @param  \App\Models\Entry\Buy  $buy
     * @return \Illuminate\Http\Response: Json con mensaje de confirmacion y compra guardado
     */
    public function store(StoreRequest $request,Buy $buy)
    {
        if($request->ajax()){
            $buy= $buy->store($request);
            if($buy){
                return response()->json([
                    'message' => 'Compra creada correctamente.',
                    'buy' => $buy->load('supplier','user','buyDetails.presentationProduct.product','buyDetails.presentationProduct.presentation')
                ],200);
            }else{
                return response()->json([
                    'message' => 'Error interno',
                ],500);
            }
        }

        return redirect('home');
    }

    /**
     * Mostrar una compra
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Models\Entry\Buy  $buy
     * @return \Illuminate\Http\Response: Json con la compra encontrado
     */
    public function show(Request $request,Buy $buy)
    {
        if($request->ajax()){
            return response()->json([
                'buy' => $buy->load('supplier','user','buyDetails.presentationProduct.product','buyDetails.presentationProduct.presentation')
            ],200);
        }

        return redirect('home');
    }

    /**
     * Modificar una compra
     *
     * @param  \App\Http\Requests\Entry\Buy\StoreRequest  $request: request para guardado
     * @param  \App\Models\Entry\Buy  $buy
     * @return \Illuminate\Http\Response: Json con mensaje de confirmacion y la compra modificado
     */
    public function update(StoreRequest $request, Buy $buy)
    {
        if($request->ajax()){
            $buy=$buy->my_update($request);
            if($buy){
                return response()->json([
                    'message' => 'Compra actualizada correctamente.',
                    'buy' => $buy
                ],200);
            }else{
                return response()->json([
                    'message' => 'Error interno',
                ],500);
            }
        }

        return redirect('home');
    }

    /**
     * Eliminar una compra
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Models\Entry\Buy  $buy
     * @return \Illuminate\Http\Response: Json con mensaje de confirmacion
     */
    public function destroy(Request $request,Buy $buy)
    {
        if($request->ajax()){
            $buy=$buy->remove($buy);
            if($buy){
                return response()->json([
                    'message' => 'Compra eliminada correctamente.',
                    'answer' => 'yes'
                ],200);
            }else{
                return response()->json([
                    'message' => 'Tiene productos con ventas.',
                    'answer' => 'no'
                ],200);
            }
        }

        return redirect('home');
    }

    #-----------------------------------
    #METODOS QUE REQUIEREN AUTORIZACION
    #-----------------------------------
    
}
