<?php

namespace App\Http\Controllers\System;

use App\Models\System\SunatInvoice;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\System\Cancel;

class SunatInvoiceController extends Controller
{
    public function __construct()
    {
        $this->authorizeResource(SunatInvoice::class, 'sunatinvoice');
    }

    #-------------------------------------------------------
    #METODOS DE RECURSOS CON AUTORIZACION: authorizeResource
    #-------------------------------------------------------

    /**
     * Listar documetos electronicos
     * 
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response: Json con lista de docuemtos
     */    
    public function index(Request $request)
    {
        if($request->ajax()){
            $sunatInvoices=SunatInvoice::orderBy('id','DESC')->client($request->find)->serie($request->find)->correlative($request->find)
                                ->whereBetween('date',[$request->beginDate,$request->endDate])
                                ->with('client','user','cancel')
                                ->paginate(env("PAGE_NUMBER",20));
            return  response()->json($sunatInvoices,200);
            //return  response()->json(Sale::whereBetween('date',[$request->beginDate,$request->endDate])->with('client','user','saleDetails.presentationProduct.product.measure','saleDetails.presentationProduct.presentation')->orderBy('id','DESC')->get(),200);
        }

        return redirect('home');
        
    }

    /**
     * Mostrar un documento electronico
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Models\System\SunatInvoice  $sunatInvoice
     * @return \Illuminate\Http\Response: Json con el documento electronico encontrado
     */
    public function show(Request $request, SunatInvoice $sunatinvoice)
    {
        if($request->ajax()){
            return response()->json([
                'sunatInvoice' => $sunatinvoice->load('cancel')
            ],200);
        }

        return redirect('home');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\System\SunatInvoice  $sunatInvoice
     * @return \Illuminate\Http\Response
     */
    public function edit(SunatInvoice $sunatInvoice)
    {
        //
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\System\SunatInvoice  $sunatInvoice
     * @return \Illuminate\Http\Response
     */
    public function destroy(SunatInvoice $sunatInvoice)
    {
        //
    }

    #-----------------------------------
    #METODOS QUE REQUIEREN AUTORIZACION
    #-----------------------------------


    /**
     * Guardar una cancelacion de un documento
     *
     * @param  \App\Http\Requests\System\Cancel\StoreRequest $request: request para guardado
     * @param  \App\Models\System\Cancel  $cancel
     * @return \Illuminate\Http\Response: Json con mensaje de confirmacion y comunicacion de baja guardado
     */
    public function cancel(Request $request,SunatInvoice $sunatInvoice)
    {
        $this->authorize('cancel', $sunatInvoice);
        if($request->ajax()){
            $cancel = new Cancel();
            $cancel= $cancel->store($request);
            $sunatInvoice->load('cancel');
            if($cancel){
                return response()->json([
                    'message' => 'Comunicacion de baja guardada y enviada correctamente.',
                    'sunatInvoice' => $sunatInvoice
                ],200);
            }else{
                return response()->json([
                    'message' => 'Error interno',
                ],500);
            }
        }

        return redirect('home');
    }
}
