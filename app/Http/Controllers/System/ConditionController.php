<?php

namespace App\Http\Controllers\System;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\System\Condition;
use App\Models\System\SunatInvoice;

class ConditionController extends Controller
{
    public function __construct()
    {
        $this->authorizeResource(Condition::class, 'condition');
    }

    #-------------------------------------------------------
    #METODOS DE RECURSOS CON AUTORIZACION: authorizeResource
    #-------------------------------------------------------

    /**
     * Listar documetos electronicos
     * 
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response: Json con lista de docuemtos
     */    
    public function index(Request $request)
    {
        if($request->ajax()){
            $sunatInvoices=Condition::orderBy('NUM_DOCU','DESC')->documentNumber($request->find)->remarks($request->remark)
                                //->whereBetween('date',[$request->beginDate,$request->endDate])
                                ->with('sunatInvoice.client','sunatInvoice.sale')
                                ->paginate(env("PAGE_NUMBER",20));
            return  response()->json($sunatInvoices,200);
            //return  response()->json(Sale::whereBetween('date',[$request->beginDate,$request->endDate])->with('client','user','saleDetails.presentationProduct.product.measure','saleDetails.presentationProduct.presentation')->orderBy('id','DESC')->get(),200);
        }

        return redirect('home');
        
    }

    /**
     * Mostrar un documento electronico
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Models\System\SunatInvoice  $sunatInvoice
     * @return \Illuminate\Http\Response: Json con el documento electronico encontrado
     */
    public function show(Request $request, SunatInvoice $sunatinvoice)
    {
        if($request->ajax()){
            return response()->json([
                'sunatInvoice' => $sunatinvoice->load('cancel')
            ],200);
        }

        return redirect('home');
    }

    /**
     * Volver a generar y enviar documento electronico
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Models\System\SunatInvoice  $sunatInvoice
     * @return \Illuminate\Http\Response: Json con el documento electronico generado y enviado
     */
    public function resend(Request $request, SunatInvoice $sunatInvoice)
    {
        $this->authorize('resend', Condition::class);
        if($request->ajax()){
            $sunatInvoice->updateTable();
            $generate = $sunatInvoice->generateInvoice();
            if($generate['validacion']=="EXITO" && strlen($generate['mensaje'])>0){
                return response()->json([
                    "send"=>"No",
                    "message"=>$generate['mensaje']
                ],200);
            }
            $sunatInvoice->sendInvoice();
            $sunatInvoice->hash=$sunatInvoice->getHash();
            $sunatInvoice->save();
            $sunatInvoice->load('condition');
            $condition = Condition::where('NUM_DOCU',$sunatInvoice->condition['NUM_DOCU'])->with('sunatInvoice')->first();
            return response()->json([
                'send' => 'yes',
                'message' => 'Documento enviado satisfactoriamente',
                'condition' => $condition
            ],200);
        }

        return redirect('home');
    }
}
