<?php

namespace App\Http\Controllers\System;

use App\Models\System\SunatInvoiceDetail;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SunatInvoiceDetailController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\System\SunatInvoiceDetail  $sunatInvoiceDetail
     * @return \Illuminate\Http\Response
     */
    public function show(SunatInvoiceDetail $sunatInvoiceDetail)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\System\SunatInvoiceDetail  $sunatInvoiceDetail
     * @return \Illuminate\Http\Response
     */
    public function edit(SunatInvoiceDetail $sunatInvoiceDetail)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\System\SunatInvoiceDetail  $sunatInvoiceDetail
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, SunatInvoiceDetail $sunatInvoiceDetail)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\System\SunatInvoiceDetail  $sunatInvoiceDetail
     * @return \Illuminate\Http\Response
     */
    public function destroy(SunatInvoiceDetail $sunatInvoiceDetail)
    {
        //
    }
}
