<?php

namespace App\Http\Controllers\System;

use App\Models\System\Invoice;
use Illuminate\Http\Request;
use App\Http\Requests\System\Invoice\StoreRequest;
use App\Http\Controllers\Controller;

class InvoiceController extends Controller
{
    public function __construct()
    {
        $this->authorizeResource(Invoice::class, 'invoice');
    }

    #-------------------------------------------------------
    #METODOS DE RECURSOS CON AUTORIZACION: authorizeResource
    #-------------------------------------------------------

     /**
     * Listar parametros de facturacion
     * 
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response: Json con lista de parametros de facturacion
     */    
    public function index(Request $request)
    {
        if($request->ajax()){
            $invoices = Invoice::all();
            return  response()->json($invoices,200);
        }

        return redirect('home');
    }

     /**
     * Mostrar un parametro de facturacion
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Models\Security\Invoice  $invoice
     * @return \Illuminate\Http\Response: Json con el parametro de facturacion encontrado
     */
    public function show(Request $request,Invoice $invoice)
    {
        if($request->ajax()){
            return response()->json([
                'invoice' => $invoice
            ],200);
        }

        return redirect('home');
    }

    /**
     * Modificar un parametro de facturacion
     *
     * @param  \App\Http\Requests\Security\Invoice\UpdateRequest  $request: request para modificado
     * @param  \App\Models\Security\Invoice  $invoice
     * @return \Illuminate\Http\Response: Json con mensaje de confirmacion y el parametro de facturacion modificado
     */
    public function update(StoreRequest $request, Invoice $invoice)
    {
        if($request->ajax()){
            $invoice=$invoice->update($request->all());
            return response()->json([
                'message' => 'Parametros actualizados correctamente.',
                'invoice' => $invoice
            ],200);
        }

        return redirect('home');
    }
}
