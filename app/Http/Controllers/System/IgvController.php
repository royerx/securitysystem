<?php

namespace App\Http\Controllers\System;

use App\Models\System\Igv;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\System\Igv\StoreRequest;

class IgvController extends Controller
{
    public function __construct()
    {
        $this->authorizeResource(Igv::class, 'igv');
    }

    #-------------------------------------------------------
    #METODOS DE RECURSOS CON AUTORIZACION: authorizeResource
    #-------------------------------------------------------

    /**
     * Listar IGVs
     * 
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response: Json con lista de IGVs
     */    
    public function index(Request $request)
    {
        if($request->ajax()){
            $igvs = Igv::orderBy('id','DESC')->paginate(env("PAGE_NUMBER",20));
            return  response()->json($igvs,200);
        }

        return redirect('home');
        
    }

    /**
     * Guardar un IGV
     *
     * @param  \App\Http\Requests\System\Igv\StoreRequest  $request: request para guardado
     * @param  \App\Models\System\Igv  $igv
     * @return \Illuminate\Http\Response: Json con mensaje de confirmacion e IGV guardado
     */
    public function store(StoreRequest $request,Igv $igv)
    {
        if($request->ajax()){
            $igv= Igv::create($request->all());
            return response()->json([
                'message' => 'IGV creado correctamente.',
                'igv' => $igv
            ],200);
        }

        return redirect('home');
    }

    /**
     * Mostrar un IGV
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Models\System\Igv  $igv
     * @return \Illuminate\Http\Response: Json con el IGV encontrado
     */
    public function show(Request $request,Igv $igv)
    {
        if($request->ajax()){
            return response()->json([
                'igv' => $igv
            ],200);
        }

        return redirect('home');
    }

    /**
     * Modificar un IGV
     *
     * @param  \App\Http\Requests\System\Igv\UpdateRequest  $request: request para modificado
     * @param  \App\Models\System\Igv  $igv
     * @return \Illuminate\Http\Response: Json con mensaje de confirmacion y el IGV modificado
     */
    public function update(StoreRequest $request, Igv $igv)
    {
        if($request->ajax()){
            $igv=$igv->update($request->all());
            return response()->json([
                'message' => 'IGV actualizado correctamente.',
                'igv' => $igv
            ],200);
        }

        return redirect('home');
    }

    /**
     * Eliminar un IGV
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Models\System\Igv  $igv
     * @return \Illuminate\Http\Response: Json con mensaje de confirmacion
     */
    public function destroy(Request $request,Igv $igv)
    {
        if($request->ajax()){
            $igv=$igv->delete();
            return response()->json([
                'message' => 'eliminado correctamente.',
                'igv' => $igv
            ],200);
        }

        return redirect('home');
    }
}
