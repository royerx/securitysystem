<?php

namespace App\Http\Controllers\Account;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\Account\Expense\StoreRequest;
use App\Models\Account\Expense;

class ExpenseController extends Controller
{
    public function __construct()
    {
        $this->authorizeResource(Expense::class, 'expense');
    }

    #-------------------------------------------------------
    #METODOS DE RECURSOS CON AUTORIZACION: authorizeResource
    #-------------------------------------------------------

    /**
     * Listar gastos
     * 
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response: Json con lista de gastos
     */    
    public function index(Request $request)
    {

        if($request->ajax()){
            $beginDate=$request->beginDate;
            $endDate=$request->endDate;
            if($request->beginDate && $request->endDate){
                $expenses=Expense::whereHas('expenseDates', function ($query) use($beginDate,$endDate){
                                        $query->whereBetween('date',[$beginDate,$endDate]);
                                    })->with(['expenseDates'=>function($query){
                                        $query->orderBy('date');
                                    }])->where('type',$request->type)
                                    ->paginate(env("PAGE_NUMBER",20));
                return response()->json($expenses,200);
            }else{
                $expenses=Expense::with(['expenseDates'=>function($query){
                                        $query->orderBy('date');
                                    }])->where('type',$request->type)->orderBy('id','DESC')
                                    ->paginate(env("PAGE_NUMBER",20));
                return  response()->json($expenses,200);
            }  
        }

        return redirect('home');
        
    }

    /**
     * Guardar un gasto
     *
     * @param  \App\Http\Requests\Account\Expense\StoreRequest  $request: request para guardado
     * @param  \App\Models\Account\Expense  $expense
     * @return \Illuminate\Http\Response: Json con mensaje de confirmacion y gasto guardado
     */
    public function store(StoreRequest $request,Expense $expense)
    {
        if($request->ajax()){
            $expense= $expense->store($request);
            return response()->json([
                'message' => 'Egreso creado correctamente.',
                'expense' => $expense->load('expenseDates')
            ],200);
        }

        return redirect('home');
    }

    /**
     * Mostrar un gasto
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Models\Account\Expense  $expense
     * @return \Illuminate\Http\Response: Json con el gasto encontrado
     */
    public function show(Request $request,Expense $expense)
    {
        if($request->ajax()){
            return response()->json([
                'expense' => $expense->load('expenseDates')
            ],200);
        }

        return redirect('home');
    }

    /**
     * Modificar un gasto
     *
     * @param  \App\Http\Requests\Account\Expense\UpdateRequest  $request: request para modificado
     * @param  \App\Models\Account\Expense  $expense
     * @return \Illuminate\Http\Response: Json con mensaje de confirmacion y el gasto modificado
     */
    public function update(StoreRequest $request, Expense $expense)
    {
        if($request->ajax()){
            $expense->my_update($request,$expense);
            return response()->json([
                'message' => 'Egreso actualizado correctamente.',
                'expense' => $expense
            ],200);
        }

        return redirect('home');
    }

    /**
     * Eliminar un gasto
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Models\Account\Expense  $expense
     * @return \Illuminate\Http\Response: Json con mensaje de confirmacion
     */
    public function destroy(Request $request,Expense $expense)
    {
        if($request->ajax()){
            $expense=$expense->remove($expense);
            return response()->json([
                'message' => 'Egreso eliminado correctamente.',
                'expense' => $expense
            ],200);
        }

        return redirect('home');
    }

    #-----------------------------------
    #METODOS QUE REQUIEREN AUTORIZACION
    #-----------------------------------
}
