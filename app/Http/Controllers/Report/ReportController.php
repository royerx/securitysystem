<?php

namespace App\Http\Controllers\Report;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ReportController extends Controller
{
    /**
     * Obtener reporte de ventas por dias
     * 
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response: Json con el reporte
     */    
    public function salesReport(Request $request)
    {
        if($request->ajax()){
            $salesReport = DB::table('sales')
                 ->join('sale_details','sales.id', '=', 'sale_details.sale_id')
                 ->selectRaw('date,sum(quantity*price) as total')
                 ->whereBetween('date', [$request->beginDate,$request->endDate])
                 ->groupByRaw('sales.date')
                 ->get();
            return response()->json($salesReport,200);
        }

        return redirect('home');
    }
    /**
     * Obtener reporte de compras por dias
     * 
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response: Json con el reporte
     */    
    public function buysReport(Request $request)
    {
        if($request->ajax()){
            $buysReport = DB::table('buys')
                 ->join('buy_details','buys.id', '=', 'buy_details.buy_id')
                 ->selectRaw('date,sum(quantity*price) as total')
                 ->whereBetween('date', [$request->beginDate,$request->endDate])
                 ->groupByRaw('buys.date')
                 ->get();
            return response()->json($buysReport,200);
        }

        return redirect('home');
    }
    /**
     * Obtener reporte de productos por categorias y por laboratorios
     * 
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response: Json con el reporte
     */    
    public function productsReport(Request $request)
    {
        if($request->ajax()){
            switch ($request->type) {
                case 'all':
                    $productsReport = DB::table('sale_details as SD')
                        ->join('sales as SA','SA.id','=','SD.sale_id')
                        ->join('buy_detail_sale_detail as BS','SD.id','=','BS.sale_detail_id')
                        ->join('buy_details as BD','BD.id','=','BS.buy_detail_id')
                        ->join('presentation_product as PP','PP.id', '=', 'SD.presentation_product_id')
                        ->join('presentations as PT','PT.id','=','PP.presentation_id')
                        ->join('products as PR','PR.id','=','PP.product_id')
                        ->join('categories as CA','CA.id','=','PR.category_id')
                        ->join('laboratories as LB','LB.id','=','PR.laboratory_id')
                        ->selectRaw("PR.name as product, 
                                    PT.name as presentation,
                                    CONCAT(PP.quantity,' unidades') as presentationQuantity,
                                    SD.price as salePrice,
                                    SUM(BS.quantitySale)/PP.quantity as quantitySale,
                                    SUM((BS.quantitySale)*(SD.price*SD.quantity)/(SD.quantity*PP.quantity)) as totalSalePrice,
                                    SUM((BS.quantitySale)*(BD.quantity*BD.price)/(BD.quantity*(SELECT quantity FROM presentation_product where id = BD.presentation_product_id))) as totalBuyPrice")
                        ->groupByRaw("PR.id,PR.name,PT.name,CONCAT(PP.quantity,' unidades'),SD.price,PP.quantity")
                        ->whereBetween('SA.date', [$request->beginDate,$request->endDate])
                        ->get();
                    break;
                case 'category':
                    $productsReport = DB::table('sale_details as SD')
                        ->join('sales as SA','SA.id','=','SD.sale_id')
                        ->join('buy_detail_sale_detail as BS','SD.id','=','BS.sale_detail_id')
                        ->join('buy_details as BD','BD.id','=','BS.buy_detail_id')
                        ->join('presentation_product as PP','PP.id', '=', 'SD.presentation_product_id')
                        ->join('presentations as PT','PT.id','=','PP.presentation_id')
                        ->join('products as PR','PR.id','=','PP.product_id')
                        ->join('categories as CA','CA.id','=','PR.category_id')
                        ->join('laboratories as LB','LB.id','=','PR.laboratory_id')
                        ->selectRaw("PR.name as product, 
                                    PT.name as presentation,
                                    CONCAT(PP.quantity,' unidades') as presentationQuantity,
                                    SD.price as salePrice,
                                    SUM(BS.quantitySale)/PP.quantity as quantitySale,
                                    SUM((BS.quantitySale)*(SD.price*SD.quantity)/(SD.quantity*PP.quantity)) as totalSalePrice,
                                    SUM((BS.quantitySale)*(BD.quantity*BD.price)/(BD.quantity*(SELECT quantity FROM presentation_product where id = BD.presentation_product_id))) as totalBuyPrice")
                        ->groupByRaw("PR.id,PR.name,PT.name,CONCAT(PP.quantity,' unidades'),SD.price,PP.quantity")
                        ->whereBetween('SA.date', [$request->beginDate,$request->endDate])
                        ->where('CA.name','=',$request->category)
                        ->get();
                    break;
                case 'laboratory':
                    $productsReport = DB::table('sale_details as SD')
                        ->join('sales as SA','SA.id','=','SD.sale_id')
                        ->join('buy_detail_sale_detail as BS','SD.id','=','BS.sale_detail_id')
                        ->join('buy_details as BD','BD.id','=','BS.buy_detail_id')
                        ->join('presentation_product as PP','PP.id', '=', 'SD.presentation_product_id')
                        ->join('presentations as PT','PT.id','=','PP.presentation_id')
                        ->join('products as PR','PR.id','=','PP.product_id')
                        ->join('categories as CA','CA.id','=','PR.category_id')
                        ->join('laboratories as LB','LB.id','=','PR.laboratory_id')
                        ->selectRaw("PR.name as product, 
                                    PT.name as presentation,
                                    CONCAT(PP.quantity,' unidades') as presentationQuantity,
                                    SD.price as salePrice,
                                    SUM(BS.quantitySale)/PP.quantity as quantitySale,
                                    SUM((BS.quantitySale)*(SD.price*SD.quantity)/(SD.quantity*PP.quantity)) as totalSalePrice,
                                    SUM((BS.quantitySale)*(BD.quantity*BD.price)/(BD.quantity*(SELECT quantity FROM presentation_product where id = BD.presentation_product_id))) as totalBuyPrice")
                        ->groupByRaw("PR.id,PR.name,PT.name,CONCAT(PP.quantity,' unidades'),SD.price,PP.quantity")
                        ->whereBetween('SA.date', [$request->beginDate,$request->endDate])
                        ->where('LB.name','=',$request->laboratory)
                        ->get();
                    break;
                case 'categoryLaboratory':
                    $productsReport = DB::table('sale_details as SD')
                        ->join('sales as SA','SA.id','=','SD.sale_id')
                        ->join('buy_detail_sale_detail as BS','SD.id','=','BS.sale_detail_id')
                        ->join('buy_details as BD','BD.id','=','BS.buy_detail_id')
                        ->join('presentation_product as PP','PP.id', '=', 'SD.presentation_product_id')
                        ->join('presentations as PT','PT.id','=','PP.presentation_id')
                        ->join('products as PR','PR.id','=','PP.product_id')
                        ->join('categories as CA','CA.id','=','PR.category_id')
                        ->join('laboratories as LB','LB.id','=','PR.laboratory_id')
                        ->selectRaw("PR.name as product, 
                                    PT.name as presentation,
                                    CONCAT(PP.quantity,' unidades') as presentationQuantity,
                                    SD.price as salePrice,
                                    SUM(BS.quantitySale)/PP.quantity as quantitySale,
                                    SUM((BS.quantitySale)*(SD.price*SD.quantity)/(SD.quantity*PP.quantity)) as totalSalePrice,
                                    SUM((BS.quantitySale)*(BD.quantity*BD.price)/(BD.quantity*(SELECT quantity FROM presentation_product where id = BD.presentation_product_id))) as totalBuyPrice")
                        ->groupByRaw("PR.id,PR.name,PT.name,CONCAT(PP.quantity,' unidades'),SD.price,PP.quantity")
                        ->whereBetween('SA.date', [$request->beginDate,$request->endDate])
                        ->where('CA.name','=',$request->category)
                        ->where('LB.name','=',$request->laboratory)
                        ->get();
            }
            return response()->json($productsReport,200);
        }

        return redirect('home');
    }
    /**
     * Obtener reporte de egresos entre fechas
     * 
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response: Json con el reporte
     */    
    public function expensesReport(Request $request)
    {
       if($request->ajax()){
            $expensesReport = DB::table('expenses as E')
                 ->join('expense_dates as D','E.id','=','D.expense_id')
                 ->selectRaw('E.name,D.date,E.amount as total')
                 ->whereBetween('D.date', [$request->beginDate,$request->endDate])
                 ->get();
            return response()->json($expensesReport,200);
        }

        return redirect('home');
    }
    /**
     * Obtener reporte de productos vendidos por dia
     * 
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response: Json con el reporte
     */    
    public function dayProductsReport(Request $request)
    {
       if($request->ajax()){
                 $productsReport = DB::table('sale_details as SD')
                        ->join('sales as SA','SA.id','=','SD.sale_id')
                        ->join('buy_detail_sale_detail as BS','SD.id','=','BS.sale_detail_id')
                        ->join('buy_details as BD','BD.id','=','BS.buy_detail_id')
                        ->join('presentation_product as PP','PP.id', '=', 'SD.presentation_product_id')
                        ->join('presentations as PT','PT.id','=','PP.presentation_id')
                        ->join('products as PR','PR.id','=','PP.product_id')
                        ->join('categories as CA','CA.id','=','PR.category_id')
                        ->join('laboratories as LB','LB.id','=','PR.laboratory_id')
                        ->selectRaw("PR.name as product, 
                                    PT.name as presentation,
                                    CONCAT(PP.quantity,' unidades') as presentationQuantity,
                                    SD.price as salePrice,
                                    SUM(BS.quantitySale)/PP.quantity as quantitySale,
                                    SUM((BS.quantitySale)*(SD.price*SD.quantity)/(SD.quantity*PP.quantity)) as totalSalePrice")
                        ->groupByRaw("PR.id,PR.name,PT.name,CONCAT(PP.quantity,' unidades'),SD.price,PP.quantity")
                        ->where('SA.date',$request->date)
                        ->get();
            return response()->json($productsReport,200);
        }

        return redirect('home');
    }

    /**
     * Obtener reporte de egresos e ingresos entre fechas
     * 
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response: Json con el reporte
     */    
    public function balanceReport(Request $request)
    {   
       if($request->ajax()){
            $totalIncome =0;
            $totalExpense =0;
            $utility = 0;
            
            if($request->type=="detail"){
                $productsReport = DB::table('sale_details as SD')
                            ->join('sales as SA','SA.id','=','SD.sale_id')
                            ->join('buy_detail_sale_detail as BS','SD.id','=','BS.sale_detail_id')
                            ->join('buy_details as BD','BD.id','=','BS.buy_detail_id')
                            ->join('presentation_product as PP','PP.id', '=', 'SD.presentation_product_id')
                            ->join('presentations as PT','PT.id','=','PP.presentation_id')
                            ->join('products as PR','PR.id','=','PP.product_id')
                            ->join('categories as CA','CA.id','=','PR.category_id')
                            ->join('laboratories as LB','LB.id','=','PR.laboratory_id')
                            ->selectRaw("PR.name as product, 
                                        PT.name as presentation,
                                        CONCAT(PP.quantity,' unidades') as presentationQuantity,
                                        SD.price as salePrice,
                                        SA.date,
                                        SUM(BS.quantitySale)/PP.quantity as quantitySale,
                                        SUM((BS.quantitySale)*(SD.price*SD.quantity)/(SD.quantity*PP.quantity)) as totalSalePrice,
                                        SUM((BS.quantitySale)*(BD.quantity*BD.price)/(BD.quantity*(SELECT quantity FROM presentation_product where id = BD.presentation_product_id))) as totalBuyPrice")
                            ->groupByRaw("PR.id,PR.name,PT.name,CONCAT(PP.quantity,' unidades'),SD.price,SA.date,PP.quantity")
                            ->whereBetween('SA.date', [$request->beginDate,$request->endDate])
                            ->orderBy("SA.date")
                            ->get();
                $expensesReport = DB::table('expenses as E')
                    ->join('expense_dates as D','E.id','=','D.expense_id')
                    ->selectRaw('E.name,E.type,D.date,E.amount as total')
                    ->whereBetween('D.date', [$request->beginDate,$request->endDate])
                    ->orderBy("D.date")
                    ->get();
            }else{
                $productsReport = DB::table('sale_details as SD')
                            ->join('sales as SA','SA.id','=','SD.sale_id')
                            ->join('buy_detail_sale_detail as BS','SD.id','=','BS.sale_detail_id')
                            ->join('buy_details as BD','BD.id','=','BS.buy_detail_id')
                            ->join('presentation_product as PP','PP.id', '=', 'SD.presentation_product_id')
                            ->join('presentations as PT','PT.id','=','PP.presentation_id')
                            ->join('products as PR','PR.id','=','PP.product_id')
                            ->join('categories as CA','CA.id','=','PR.category_id')
                            ->join('laboratories as LB','LB.id','=','PR.laboratory_id')
                            ->selectRaw("SA.date,
                                        SUM((BS.quantitySale)*(SD.price*SD.quantity)/(SD.quantity*PP.quantity)) as totalSalePrice,
                                        SUM((BS.quantitySale)*(BD.quantity*BD.price)/(BD.quantity*(SELECT quantity FROM presentation_product where id = BD.presentation_product_id))) as totalBuyPrice")
                            ->groupByRaw("SA.date")
                            ->whereBetween('SA.date', [$request->beginDate,$request->endDate])
                            ->get();
                $expensesReport = DB::table('expenses as E')
                    ->join('expense_dates as D','E.id','=','D.expense_id')
                    ->selectRaw('D.date,SUM(E.amount) as total')
                    ->groupBy("D.date")
                    ->whereBetween('D.date', [$request->beginDate,$request->endDate])
                    ->orderBy("D.date")
                    ->get();
            }
            
            foreach ($productsReport as $product) {
                $totalIncome+=$product->totalSalePrice-$product->totalBuyPrice;
            }
            foreach ($expensesReport as $expense) {
                $totalExpense+=$expense->total;
            }
            
            $utility = $totalIncome-$totalExpense;
            
            return response()->json([
                'sales' => $productsReport,
                'totalIncome' => $totalIncome,
                'expenses' => $expensesReport,
                'totalExpense' => $totalExpense,
                'utility' => $utility,
            ],200);
        }

        return redirect('home');
    }
    
}
