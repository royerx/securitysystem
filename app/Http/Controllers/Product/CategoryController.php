<?php

namespace App\Http\Controllers\Product;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Product\Category;
use App\Http\Requests\Product\Category\StoreRequest;
use App\Http\Requests\Product\Category\UpdateRequest;

class CategoryController extends Controller
{
    public function __construct()
    {
        $this->authorizeResource(Category::class, 'category');
    }

    #-------------------------------------------------------
    #METODOS DE RECURSOS CON AUTORIZACION: authorizeResource
    #-------------------------------------------------------

    /**
     * Listar categorías
     * 
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response: Json con lista de categorías
     */    
    public function index(Request $request)
    {
        if($request->ajax()){
            $categories = Category::orderBy('id','DESC')->name($request->find)->paginate(env("PAGE_NUMBER",20));
            return  response()->json($categories,200);
        }

        return redirect('home');
        
    }

    /**
     * Guardar una categoría
     *
     * @param  \App\Http\Requests\Product\Category\StoreRequest  $request: request para guardado
     * @param  \App\Models\Product\Category  $category
     * @return \Illuminate\Http\Response: Json con mensaje de confirmacion y categoría guardado
     */
    public function store(StoreRequest $request,Category $category)
    {
        if($request->ajax()){
            $category= Category::create($request->all());
            return response()->json([
                'message' => 'Categoría creada correctamente.',
                'category' => $category
            ],200);
        }

        return redirect('home');
    }

    /**
     * Mostrar una categoría
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Models\Product\Category  $category
     * @return \Illuminate\Http\Response: Json con la categoría encontrada
     */
    public function show(Request $request,Category $category)
    {
        if($request->ajax()){
            return response()->json([
                'category' => $category
            ],200);
        }

        return redirect('home');
    }

    /**
     * Modificar una categoría
     *
     * @param  \App\Http\Requests\Product\Category\UpdateRequest  $request: request para modificado
     * @param  \App\Models\Product\Category  $category
     * @return \Illuminate\Http\Response: Json con mensaje de confirmacion y la categoría modificado
     */
    public function update(UpdateRequest $request, Category $category)
    {
        if($request->ajax()){
            $category=$category->update($request->all());
            return response()->json([
                'message' => 'Categoría actualizada correctamente.',
                'category' => $category
            ],200);
        }

        return redirect('home');
    }

    /**
     * Eliminar una categoría
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Models\Product\Category  $category
     * @return \Illuminate\Http\Response: Json con mensaje de confirmacion
     */
    public function destroy(Request $request,Category $category)
    {
        if($request->ajax()){
            $category=$category->delete();
            return response()->json([
                'message' => 'eliminada correctamente.',
                'category' => $category
            ],200);
        }

        return redirect('home');
    }

    #-----------------------------------
    #METODOS QUE REQUIEREN AUTORIZACION
    #-----------------------------------
    
    /**
     * Listar todas las categorias dada una cantidad determinada
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response: Json con mensaje de confirmacion con la lista de categorias
     */
    public function list(Request $request)
    {
        $this->authorize('list', Category::class);
        if($request->ajax()){

            if($request->quantity)
                $categories = Category::orderBy('id','DESC')->name($request->find)->take($request->quantity)->get();
            else
                $categories = Category::orderBy('id','DESC')->name($request->find)->get();

            return  response()->json($categories,200);
        }
        
        return redirect('home');
    }
}
