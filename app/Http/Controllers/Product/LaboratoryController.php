<?php

namespace App\Http\Controllers\Product;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Product\Laboratory;
use App\Http\Requests\Product\Laboratory\StoreRequest;
use App\Http\Requests\Product\Laboratory\UpdateRequest;

class LaboratoryController extends Controller
{
    public function __construct()
    {
        $this->authorizeResource(Laboratory::class, 'laboratory');
    }

    #-------------------------------------------------------
    #METODOS DE RECURSOS CON AUTORIZACION: authorizeResource
    #-------------------------------------------------------

    /**
     * Listar laboratorios
     * 
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response: Json con lista de laboratorios
     */    
    public function index(Request $request)
    {
        if($request->ajax()){
            $laboratories = Laboratory::orderBy('id','DESC')->name($request->find)->paginate(env("PAGE_NUMBER",20));
            return  response()->json($laboratories,200);
        }

        return redirect('home');
        
    }

    /**
     * Guardar una marca
     *
     * @param  \App\Http\Requests\Product\Laboratory\StoreRequest  $request: request para guardado
     * @param  \App\Models\Product\Laboratory  $laboratory
     * @return \Illuminate\Http\Response: Json con mensaje de confirmacion y marca guardado
     */
    public function store(StoreRequest $request,Laboratory $laboratory)
    {
        if($request->ajax()){
            $laboratory= Laboratory::create($request->all());
            return response()->json([
                'message' => 'Laboratorio creado correctamente.',
                'laboratory' => $laboratory
            ],200);
        }

        return redirect('home');
    }

    /**
     * Mostrar una marca
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Models\Product\Laboratory  $laboratory
     * @return \Illuminate\Http\Response: Json con la marca encontrada
     */
    public function show(Request $request,Laboratory $laboratory)
    {
        if($request->ajax()){
            return response()->json([
                'laboratory' => $laboratory
            ],200);
        }

        return redirect('home');
    }

    /**
     * Modificar una marca
     *
     * @param  \App\Http\Requests\Product\Laboratory\UpdateRequest  $request: request para modificado
     * @param  \App\Models\Product\Laboratory  $laboratory
     * @return \Illuminate\Http\Response: Json con mensaje de confirmacion y la marca modificado
     */
    public function update(UpdateRequest $request, Laboratory $laboratory)
    {
        if($request->ajax()){
            $laboratory=$laboratory->update($request->all());
            return response()->json([
                'message' => 'Laboratorio actualizado correctamente.',
                'laboratory' => $laboratory
            ],200);
        }

        return redirect('home');
    }

    /**
     * Eliminar una marca
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Models\Product\Laboratory  $laboratory
     * @return \Illuminate\Http\Response: Json con mensaje de confirmacion
     */
    public function destroy(Request $request,Laboratory $laboratory)
    {
        if($request->ajax()){
            $laboratory=$laboratory->delete();
            return response()->json([
                'message' => 'eliminado correctamente.',
                'laboratory' => $laboratory
            ],200);
        }

        return redirect('home');
    }

    #-----------------------------------
    #METODOS QUE REQUIEREN AUTORIZACION
    #-----------------------------------

    /**
     * Listar todos los laboratorios dada una cantidad determinada
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response: Json con mensaje de confirmacion con la lista de laboratorios
     */
    public function list(Request $request)
    {
        $this->authorize('list', Laboratory::class);
        if($request->ajax()){

            if($request->quantity)
                $laboratories = Laboratory::orderBy('id','DESC')->name($request->find)->take($request->quantity)->get();
            else
                $laboratories = Laboratory::orderBy('id','DESC')->name($request->find)->get();

            return  response()->json($laboratories,200);
        }
        
        return redirect('home');
    }
}
