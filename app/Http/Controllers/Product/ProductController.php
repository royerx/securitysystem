<?php

namespace App\Http\Controllers\Product;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Product\Product;
use App\Models\Product\Category;
use App\Models\Product\Measure;
use App\Models\Product\Presentation;
use App\Models\Product\PresentationProduct;
use App\Http\Requests\Product\Product\StoreRequest;
use App\Http\Requests\Product\Product\UpdateRequest;
use App\Http\Requests\Product\Product\PresentationRequest;
use Illuminate\Support\Facades\DB;

class ProductController extends Controller
{
    public function __construct()
    {
        $this->authorizeResource(Product::class, 'product');
    }

    #-------------------------------------------------------
    #METODOS DE RECURSOS CON AUTORIZACION: authorizeResource
    #-------------------------------------------------------

    /**
     * Listar productos
     * 
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response: Json con lista de productos
     */    
    public function index(Request $request)
    {
        if($request->ajax()){
            $products=Product::orderBy('id','DESC')->name($request->find)->code($request->find)
                                ->with('category','laboratory','presentationProducts.presentation')
                                ->paginate(env("PAGE_NUMBER",20));
            return  response()->json($products,200);
        }

        return redirect('home');
        
    }

    /**
     * Guardar un producto
     *
     * @param  \App\Http\Requests\Product\Product\StoreRequest  $request: request para guardado
     * @param  \App\Models\Product\Product  $product
     * @return \Illuminate\Http\Response: Json con mensaje de confirmacion y producto guardado
     */
    public function store(StoreRequest $request,Product $product)
    {
        if($request->ajax()){
            $product= $product->store($request);
            if($product){
                return response()->json([
                    'message' => 'Producto creado correctamente.',
                    'product' => $product->load('category','laboratory','presentationProducts.presentation')
                ],200);
            }
            else{
                return abort(500);
            }
        }

        return redirect('home');
    }

    /**
     * Mostrar un producto
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Models\Product\Product  $product
     * @return \Illuminate\Http\Response: Json con el producto encontrada
     */
    public function show(Request $request,Product $product)
    {
        if($request->ajax()){
            return response()->json([
                'product' => $product->load('category','laboratory','presentationProducts.presentation')
            ],200);
        }

        return redirect('home');
    }

    /**
     * Modificar una producto
     *
     * @param  \App\Http\Requests\Product\Product\UpdateRequest  $request: request para modificado
     * @param  \App\Models\Product\Product  $product
     * @return \Illuminate\Http\Response: Json con mensaje de confirmacion y el producto modificado
     */
    public function update(UpdateRequest $request, Product $product)
    {
        if($request->ajax()){
            $product->update($request->all());
            return response()->json([
                'message' => 'Producto actualizado correctamente.',
                'product' => $product
            ],200);
        }

        return redirect('home');
    }

    /**
     * Eliminar un producto
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Models\Product\Product  $product
     * @return \Illuminate\Http\Response: Json con mensaje de confirmacion
     */
    public function destroy(Request $request,Product $product)
    {
        if($request->ajax()){
            $product=$product->remove($product);
            return response()->json([
                'message' => 'eliminado correctamente.',
                'product' => $product
            ],200);
        }

        return redirect('home');
    }

    #-----------------------------------
    #METODOS QUE REQUIEREN AUTORIZACION
    #-----------------------------------

    /**
     * Guardar imagen del uproducto
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Models\Product\Product  $product
     * @return \Illuminate\Http\Response: Json con mensaje de confirmacion con la imagen actualizada 
     */
    public function updatePhoto(Request $request,Product $product)
    {
        $this->authorize('updatePhoto', $product);
        if($request->ajax()){
            if($request->photo){
                $product->picture = $product->savePicture($request->photo,$product->picture);
                $product->save();
                return response()->json([
                    'message' => 'Imagen actualizada correctamente.',
                    'picture' => $product->picture
                ],200);
            }else{
                return response()->json([
                    'message' => 'Error interno',
                ],500);
            }
        }

        return redirect('home');
        
    }

    /**
     * listar presentacion del producto
     *
     * @param  \App\Http\Requests\Product\Product\UpdateRequest $request para presentaciones
     * @return \Illuminate\Http\Response: Json con lista de productos
     */
    public function listPresentation(Request $request)
    {
        $this->authorize('listPresentation', Product::class);
        if($request->ajax()){
            if($request->quantity)
                $products=PresentationProduct::orderBy('id','DESC')->productName($request->find)
                        ->with('product','presentation')->take($request->quantity)->get();
            else
                $products=PresentationProduct::orderBy('id','DESC')->productName($request->find)
                        ->with('product','presentation')->get();
            return $products;
        }

        return redirect('home');
        
    }

    /**
     * Guardar presentacion del producto
     *
     * @param  \App\Http\Requests\Product\Product\UpdateRequest $request para presentaciones
     * @param  \App\Models\Product\Product  $product
     * @return \Illuminate\Http\Response: Json con mensaje de confirmacion con la creacion de la presentacion
     */
    public function createPresentation(PresentationRequest $request,Product $product)
    {
        $this->authorize('createPresentation', $product);
        if($request->ajax()){
            $product->presentations()->attach($request->presentation_id, ['quantity' => $request->quantity, 'buyPrice' => $request->buyPrice, 'salePrice' => $request->salePrice]);
            $product->save();
            return response()->json([
                'message' => 'Presentación agregada correctamente.',
                'product' => $product->load('category','laboratory','presentationProducts.presentation')
            ],200);
        }

        return redirect('home');
        
    }

    /**
     * Modificar presentacion del producto
     *
     * @param  \App\Http\Requests\Product\Product\UpdateRequest $request para presentaciones
     * @param  \App\Models\Product\Product  $product
     * @return \Illuminate\Http\Response: Json con mensaje de confirmacion con la modificacion de la presentacion
     */
    public function updatePresentation(PresentationRequest $request,Product $product)
    {
        $this->authorize('updatePresentation', $product);
        if($request->ajax()){
            $product->presentations()->wherePivot('id', $request->id)->updateExistingPivot($request->presentation_id, ['quantity' => $request->quantity, 'buyPrice' => $request->buyPrice,'salePrice' => $request->salePrice]);
            $product->save();
            return response()->json([
                'message' => 'Presentación modificada correctamente.',
                'product' => $product->load('category','laboratory','presentationProducts.presentation')
            ],200);
        }

        return redirect('home');
        
    }

    /**
     * Eliminar presentacion del producto
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Models\Product\Product  $product
     * @return \Illuminate\Http\Response: Json con mensaje de confirmacion
     */
    public function deletePresentation(Request $request,Product $product)
    {
        $this->authorize('deletePresentation', $product);
        if($request->ajax()){
            $product->presentations()->wherePivot('id', $request->id)->detach($request->presentation_id);
            $product->save();
            return response()->json([
                'message' => 'Presentación eliminada correctamente.',
                'product' => $product
            ],200);
        }

        return redirect('home');
        
    }

     /**
     * Listar todos los productos dada una cantidad determinada
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response: Json con mensaje de confirmacion con la lista de productos
     */
    public function list(Request $request)
    {
        $this->authorize('list', Product::class);
        if($request->ajax()){
            if($request->quantity)
                $products = DB::table('products as P')
                        ->join('categories as C','P.category_id','=','C.id')
                        ->join('laboratories as L','P.laboratory_id','=','L.id')
                        ->join('presentation_product as R','P.id', '=', 'R.product_id')
                        ->join('presentations as PR','R.presentation_id','=','PR.id')
                        ->selectRaw("P.code,C.name category,P.name, L.name laboratory, 
                                    IF(PR.name='Individual',CONCAT(R.quantity,' Unidad'),CONCAT(PR.name,' (',R.quantity,' Unidades)')) presentation,
                                    P.stock/R.quantity as stock,
                                    R.salePrice,R.buyPrice,P.detail")
                        ->orderBy("P.id",'desc')
                        ->orWhere('C.name','LIKE',"%$request->find%")
                        ->orWhere('P.name','LIKE',"%$request->find%")
                        ->take($request->quantity)->get();
            else
                $products = DB::table('products as P')
                        ->join('categories as C','P.category_id','=','C.id')
                        ->join('laboratories as L','P.laboratory_id','=','L.id')
                        ->join('presentation_product as R','P.id', '=', 'R.product_id')
                        ->join('presentations as PR','R.presentation_id','=','PR.id')
                        ->selectRaw("P.code,C.name category,P.name, L.name laboratory, 
                                    IF(PR.name='Individual',CONCAT(R.quantity,' Unidad'),CONCAT(PR.name,' (',R.quantity,' Unidades)')) presentation,
                                    P.stock/R.quantity as stock,
                                    R.salePrice,R.buyPrice,P.detail")
                        ->orderBy("P.id",'desc')
                        ->orWhere('C.name','LIKE',"%$request->find%")
                        ->orWhere('P.name','LIKE',"%$request->find%")
                        ->get();

            return  response()->json($products,200);
        }
        
        return redirect('home');
    }
}
