<?php

namespace App\Http\Controllers\Product;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Product\Presentation;
use App\Http\Requests\Product\Presentation\StoreRequest;
use App\Http\Requests\Product\Presentation\UpdateRequest;

class PresentationController extends Controller
{
    public function __construct()
    {
        $this->authorizeResource(Presentation::class, 'presentation');
    }

    #-------------------------------------------------------
    #METODOS DE RECURSOS CON AUTORIZACION: authorizeResource
    #-------------------------------------------------------

    /**
     * Listar presentaciones
     * 
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response: Json con lista de presentaciones
     */    
    public function index(Request $request)
    {
        if($request->ajax()){
            $presentations = Presentation::orderBy('id','DESC')->name($request->find)->paginate(env("PAGE_NUMBER",20));
            return  response()->json($presentations,200);
        }

        return redirect('home');
        
    }

    /**
     * Guardar una presentacion
     *
     * @param  \App\Http\Requests\Product\Presentation\StoreRequest  $request: request para guardado
     * @param  \App\Models\Product\Presentation  $presentation
     * @return \Illuminate\Http\Response: Json con mensaje de confirmacion y presentacion guardado
     */
    public function store(StoreRequest $request,Presentation $presentation)
    {
        if($request->ajax()){
            $presentation= Presentation::create($request->all());
            return response()->json([
                'message' => 'Presentación creada correctamente.',
                'presentation' => $presentation
            ],200);
        }

        return redirect('home');
    }

    /**
     * Mostrar una presentacion
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Models\Product\Presentation  $presentation
     * @return \Illuminate\Http\Response: Json con la presentacion encontrada
     */
    public function show(Request $request,Presentation $presentation)
    {
        if($request->ajax()){
            return response()->json([
                'presentation' => $presentation
            ],200);
        }

        return redirect('home');
    }

    /**
     * Modificar una presentacion
     *
     * @param  \App\Http\Requests\Product\Presentation\UpdateRequest  $request: request para modificado
     * @param  \App\Models\Product\Presentation  $presentation
     * @return \Illuminate\Http\Response: Json con mensaje de confirmacion y la presentacion modificado
     */
    public function update(UpdateRequest $request, Presentation $presentation)
    {
        if($request->ajax()){
            $presentation=$presentation->update($request->all());
            return response()->json([
                'message' => 'Presentación actualizada correctamente.',
                'presentation' => $presentation
            ],200);
        }

        return redirect('home');
    }

    /**
     * Eliminar una presentacion
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Models\Product\Presentation  $presentation
     * @return \Illuminate\Http\Response: Json con mensaje de confirmacion
     */
    public function destroy(Request $request,Presentation $presentation)
    {
        if($request->ajax()){
            $presentation=$presentation->delete();
            return response()->json([
                'message' => 'eliminada correctamente.',
                'presentation' => $presentation
            ],200);
        }

        return redirect('home');
    }

    #-----------------------------------
    #METODOS QUE REQUIEREN AUTORIZACION
    #-----------------------------------

    /**
     * Listar todas las presentaciones dada una cantidad determinada
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response: Json con mensaje de confirmacion con la lista de presentaciones
     */
    public function list(Request $request)
    {
        $this->authorize('list', Presentation::class);
        if($request->ajax()){

            if($request->quantity)
                $presentations = Presentation::orderBy('id','DESC')->name($request->find)->take($request->quantity)->get();
            else
                $presentations = Presentation::orderBy('id','DESC')->name($request->find)->get();

            return  response()->json($presentations,200);
        }
        
        return redirect('home');
    }
}
