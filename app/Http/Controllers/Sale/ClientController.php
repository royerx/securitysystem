<?php

namespace App\Http\Controllers\Sale;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Sale\Client;
use App\Http\Requests\Sale\Client\StoreRequest;
use App\Http\Requests\Sale\Client\UpdateRequest;

class ClientController extends Controller
{
    public function __construct()
    {
        $this->authorizeResource(Client::class, 'client');
    }

    #-------------------------------------------------------
    #METODOS DE RECURSOS CON AUTORIZACION: authorizeResource
    #-------------------------------------------------------

    /**
     * Listar clientes
     * 
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response: Json con lista de clientes
     */    
    public function index(Request $request)
    {
        if($request->ajax()){
            $clinets = Client::orderBy('id','DESC')
                            ->firstName($request->find)
                            ->lastName($request->find)
                            ->documentNumber($request->find)
                            ->paginate(env("PAGE_NUMBER",20));
            
            return  response()->json($clinets,200);
        }

        return redirect('home');
        
    }

    /**
     * Guardar un cliente
     *
     * @param  \App\Http\Requests\Sale\Client\StoreRequest $request: request para guardado
     * @param  \App\Models\Sale\Client  $client
     * @return \Illuminate\Http\Response: Json con mensaje de confirmacion y cliente guardado
     */
    public function store(StoreRequest $request,Client $client)
    {
        if($request->ajax()){
            $client= $client->store($request);
            return response()->json([
                'message' => 'Cliente creado correctamente.',
                'client' => $client
            ],200);
        }

        return redirect('home');
    }

    /**
     * Mostrar un cliente
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Models\Sale\Client  $client
     * @return \Illuminate\Http\Response: Json con el cliente encontrado
     */
    public function show(Request $request,Client $client)
    {
        if($request->ajax()){
            return response()->json([
                'client' => $client
            ],200);
        }

        return redirect('home');
    }

    /**
     * Modificar un cliente
     *
     * @param  \App\Http\Requests\Sale\Client\UpdateRequest  $request: request para modificado
     * @param  \App\Models\Sale\Client  $client
     * @return \Illuminate\Http\Response: Json con mensaje de confirmacion y el client modificado
     */
    public function update(UpdateRequest $request, Client $client)
    {
        if($request->ajax()){
            $client=$client->update($request->all());
            return response()->json([
                'message' => 'Cliente actualizado correctamente.',
                'client' => $client
            ],200);
        }

        return redirect('home');
    }

    /**
     * Eliminar un cliente
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Models\Sale\Client  $client
     * @return \Illuminate\Http\Response: Json con mensaje de confirmacion
     */
    public function destroy(Request $request,Client $client)
    {
        if($request->ajax()){
            $client=$client->delete();
            return response()->json([
                'message' => 'Cliente eliminado correctamente.',
                'client' => $client
            ],200);
        }

        return redirect('home');
    }

    #-----------------------------------
    #METODOS QUE REQUIEREN AUTORIZACION
    #-----------------------------------

    /**
     * Listar todas los proveedores dada una cantidad determinada
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response: Json con mensaje de confirmacion con la lista de proveedores
     */
    public function list(Request $request)
    {
        $this->authorize('list', Client::class);
        if($request->ajax()){

            if($request->quantity)
                $clients = Client::orderBy('id','DESC')
                            ->firstName($request->find)
                            ->lastName($request->find)
                            ->documentNumber($request->find)
                            ->take($request->quantity)->get();
            else
                $clients = Client::orderBy('id','DESC')
                            ->firstName($request->find)
                            ->lastName($request->find)
                            ->documentNumber($request->find)
                            ->get();

            return  response()->json($clients,200);
        }
        
        return redirect('home');
    }
}
