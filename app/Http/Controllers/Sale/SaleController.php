<?php

namespace App\Http\Controllers\Sale;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Sale\Sale;
use App\Models\Sale\SaleDetail;
use App\Http\Requests\Sale\Sale\StoreRequest;
use Illuminate\Support\Facades\DB;
use PDF;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Session;
use App\Models\System\SunatInvoice;

class SaleController extends Controller
{
    public function __construct()
    {
        $this->authorizeResource(Sale::class, 'sale');
    }

    #-------------------------------------------------------
    #METODOS DE RECURSOS CON AUTORIZACION: authorizeResource
    #-------------------------------------------------------

    /**
     * Listar ventas
     * 
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response: Json con lista de ventas
     */    
    public function index(Request $request)
    {
        /*$sale=Sale::find(149);
        $sale->load(['client',
                    'user',
                    'sunatInvoices'=> function ($query) {
                        $query->where('cancel', false)->with('details.saleDetail.presentationProduct.product',
                                                            'details.saleDetail.presentationProduct.presentation');
                    }
                ]);
        //$sunatInvoice = $sale->where('cancel',false)->get();
        return $sale;*/
        if($request->ajax()){
            Sale::doesntHave('saleDetails')->delete();
            $sales=Sale::orderBy('id','DESC')->client($request->find)->serie($request->find)->correlative($request->find)
                                ->whereBetween('date',[$request->beginDate,$request->endDate])
                                ->with(['client','user','sunatInvoices'=> function ($query) {
                                                $query->where('cancel', false);}])
                                ->paginate(env("PAGE_NUMBER",20));
            return  response()->json($sales,200);
            //return  response()->json(Sale::whereBetween('date',[$request->beginDate,$request->endDate])->with('client','user','saleDetails.presentationProduct.product.measure','saleDetails.presentationProduct.presentation')->orderBy('id','DESC')->get(),200);
        }

        return redirect('home');
        
    }

    /**
     * Guardar una venta
     *
     * @param  \App\Http\Requests\Sale\Sale\StoreRequest  $request: request para guardado
     * @param  \App\Models\Sale\Sale  $sale
     * @return \Illuminate\Http\Response: Json con mensaje de confirmacion y venta guardado
     */
    public function store(StoreRequest $request,Sale $sale)
    {
        if($request->ajax()){
            $sale= $sale->store($request);
           if($sale){
               if($sale['add']=="No"){
                   return response()->json([
                    'add' => "No",
                    'message' => $sale['message'],
                ],200);
               }else{
                   return response()->json([
                       'add' => "Yes",
                       'message' => 'Venta realizada correctamente.',
                       'sale' => $sale->load(['client',
                                            'user',
                                            'sunatInvoices'=> function ($query) {
                                                $query->where('cancel', false)->with('details.presentationProduct.product',
                                                                                    'details.presentationProduct.presentation');
                                            },
                                            'saleDetails.presentationProduct.product','saleDetails.presentationProduct.presentation'])
                    ],200);
               }
                
            }else{
                return response()->json([
                    'message' => 'Error interno',
                ],500);
            }
        }

        return redirect('home');
    }

    /**
     * Mostrar una venta
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Models\Sale\Sale  $sale
     * @return \Illuminate\Http\Response: Json con la venta encontrada
     */
    public function show(Request $request,Sale $sale)
    {
        if($request->ajax()){
            return response()->json([
                'sale' => $sale->load(['client',
                                    'user',
                                    'sunatInvoices'=> function ($query) {
                                                $query->where('cancel', false)->with('details.presentationProduct.product',
                                                                                    'details.presentationProduct.presentation');
                                            },
                                    'saleDetails.presentationProduct.product',
                                    'saleDetails.presentationProduct.presentation'])
            ],200);
        }

        return redirect('home');
    }

    /**
     * Modificar una venta
     *
     * @param  \App\Http\Requests\Sale\Sale\StoreRequest  $request: request para guardado
     * @param  \App\Models\Sale\Sale  $sale
     * @return \Illuminate\Http\Response: Json con mensaje de confirmacion y la venta modificada
     */
    public function update(StoreRequest $request, Sale $sale)
    {
        if($request->ajax()){
            $sale=$sale->my_update($request);
            if($sale){
                return response()->json([
                    'message' => 'Venta actualizada correctamente.',
                    'sale' => $sale
                ],200);
            }else{
                return response()->json([
                    'message' => 'Error interno',
                ],500);
            }
        }

        return redirect('home');
    }

    /**
     * Eliminar una venta
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Models\Sale\Sale  $sale
     * @return \Illuminate\Http\Response: Json con mensaje de confirmacion
     */
    public function destroy(Request $request,Sale $sale)
    {
        if($request->ajax()){
            $sale=$sale->remove($sale);
            if($sale){
                return response()->json([
                    'message' => 'Venta eliminada correctamente.',
                    'answer' => 'yes'
                ],200);
            }else{
                return response()->json([
                    'message' => 'No se pudo eliminar la venta',
                    'answer' => 'no'
                ],200);
            }
        }

        return redirect('home');
    }

    #-----------------------------------
    #METODOS QUE REQUIEREN AUTORIZACION
    #-----------------------------------

    /**
     * Generar PDF del ticket
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Models\Sale\Sale  $sale
     * @return PDF Generado
     */
    public function voucher(Request $request,Sale $sale)
    {
        $this->authorize('voucher', $sale);
        $sale = $sale->load(['client',
                                    'user',
                                    'sunatInvoices'=> function ($query) {
                                                $query->where('cancel', false)->with('details.presentationProduct.product',
                                                                                    'details.presentationProduct.presentation');
                                            },
                                    'saleDetails.presentationProduct.product',
                                    'saleDetails.presentationProduct.presentation']);
        $sale->total=0;
        foreach($sale->saleDetails as $detail){
            $sale->total+=$detail->price*$detail->quantity;
        }
        $pdf = PDF::loadView('pages.voucher',compact('sale'))->setPaper('c7');
        return $pdf->stream();
        
    }

    /**
     * Generar comprobante con el facturador de sunat
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Models\Sale\Sale  $sale
     * @return Json con la respuesta del facturador
     */
    public function generateInvoice(Request $request,Sale $sale)
    {
        $this->authorize('generateInvoice', $sale);
        
        if($request->ajax()){
            $sunatInvoice = new SunatInvoice();
            return $sunatInvoice->generateInvoice();
        }

        return redirect('home');
        
    }

    /**
     * Enviar comprobante con el facturador de sunat
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Models\Sale\Sale  $sale
     * @return Json con la respuesta del facturador
     */
    public function sendInvoice(Request $request,Sale $sale)
    {
        $this->authorize('sendInvoice', $sale);
        
        if($request->ajax()){
            $sunatInvoice = new SunatInvoice();
            return $sunatInvoice->sendInvoice();
        }

        return redirect('home');
        
    }

    #-----------------------------------
    #METODOS SIN AUTORIZACIÓN
    #-----------------------------------

    /**
     * Generar número de documento
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response: Json con numero de documento generado
     */
    public function generateDocumentNumber(Request $request)
    {
        if($request->ajax()){
            return Sale::getDocumentNumber($request->type);
        }

        return redirect('home');
        
    }

    /**
     * Verificar número de documento
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response: Json con la verificacion
     */
    public function verifyDocumentNumber(Request $request)
    {
        if($request->ajax()){
            if($request->type=="Ticket"){
                $quantity = Sale::where('serie',$request->serie)->where('correlative',$request->correlative)->count();
            }else{
                $quantity = SunatInvoice::where('type',$request->type)->where('serie',$request->serie)->where('correlative',$request->correlative)->count();
            }
            if($quantity>0){
                return response()->json([
                    'exist' => true
                ],200);
            }else{
                return response()->json([
                    'exist' => false
                ],200);
            }
        }

        return redirect('home');
        
    }
    /**
     * Verificar número de documento
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response: Json con la verificacion
     */
    public function verifyInvoiceXml(Request $request)
    {
        if($request->ajax()){
            switch($request->type){
                case "Boleta":
                    $prefix="B";
                    $documentType=SunatInvoice::IDENTITY_DOCUMENT_TYPE['DNI'];
                    break;
                case "Factura":
                    $prefix="F";
                    $documentType=SunatInvoice::IDENTITY_DOCUMENT_TYPE['RUC'];
                    break;
            }
            $verify = file_exists(Session::get('invoice')->path."/sunat_archivos/sfs/FIRMA/".Session::get('invoice')->ruc."-".SunatInvoice::DOCUMENT_TYPE[$request->type]."-".$prefix.$request->serie."-".$request->correlative.".xml");
            return response()->json([
                    'exist' => $verify
                ],200);
        }

        return redirect('home');
        
    }
}
