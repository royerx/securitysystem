<?php

namespace App\Http\Controllers\Security;

use App\Http\Controllers\Controller;
use App\Http\Requests\Security\Permission\StoreRequest;
use App\Http\Requests\Security\Permission\UpdateRequest;
use App\Models\Security\Permission;
use Illuminate\Support\Arr;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class PermissionController extends Controller
{
    public function __construct()
    {
        $this->authorizeResource(Permission::class, 'permission');
    }

    #-------------------------------------------------------
    #METODOS DE RECURSOS CON AUTORIZACION: authorizeResource
    #-------------------------------------------------------

    /**
     * Listar permisos
     * 
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response: Json con lista de permisos
     */    
    public function index(Request $request)
    {
        if($request->ajax()){
            $permissions = Permission::with('permissionActions')->orderBy('id','DESC')->name($request->find)->get();
            return  response()->json($permissions,200);
        }

        return redirect('home');
        
    }
}
