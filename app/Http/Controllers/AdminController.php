<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use App\Models\Security\User;
use App\Models\Security\Role;
use App\Models\Security\Permission;
use App\Models\Management\Person;
use App\Models\System\Condition;
use App\Models\Sale\Client;
use App\Models\Product\Product;
use App\Models\Product\PresentationProduct;
use Illuminate\Support\Facades\DB;
use App\Models\System\Igv;
use Carbon\Carbon;
use App\Models\System\Invoice;
use Illuminate\Support\Facades\Http;

class AdminController extends Controller
{
    public function __construct(){
        return $this->middleware('auth');
    }

     /**
     * Vista de página principal
     *
     * @return \Illuminate\Http\Response: Vista de la página principal
     */   
    public function index(){
        return view('pages.home');
    }

    /**
     * Obtener usuario logueado
     * 
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response: Json con el usuario y el rol asignados
     */    
    public function currentUser(Request $request)
    {
        if($request->ajax()){
            $user = User::findOrFail(Auth::user()->id);
            $person = Person::where('firstName',Person::DEFAULT_PERSON)->first();
            $igv = Igv::where('beginDate','<=',Carbon::now())->where('endDate','>=',Carbon::now())->first();
            Session::put([
                'igv' => $igv->percent/100,
                'invoice' => Invoice::first(),
            ]);
            if(session('role_id')){
                $role = Role::findOrFail(session('role_id'))->load('permissionActions');
            }else{
                $role = "";
            }
            
            return response()->json([
                'user' => $user,
                'role' => $role,
                'personDefault' => $person,
                'igv' =>  Session::get('igv'),
            ],200);
        }

        return redirect('home');
    }
    
    /**
     * Roles del usuario logueado
     * 
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response: Json con lista de roles del usuario logueado
     */    
    public function userRoles(Request $request)
    {
        if($request->ajax()){
            $user = Auth::user()->load('roles');
            return response()->json([
                'roles' => $user->roles
            ],200);
        }

        return redirect('home');
    }

    /**
     * Asignar rol al usuario logueado
     * 
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response: Json con mensaje de confirmacion
     */    
    public function assignRole(Request $request)
    {
        if($request->ajax()){
            Session::put([
                'role_id' => $request->id,
            ]);
            return response()->json([
                'message' => 'ok'
            ],200);
        }

        return redirect('home');
    }

    /**
     * Verificar existencia de rol seleccionado
     * 
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response: Json con mensaje de confirmacion
     */    
    public function selectedRole(Request $request){
        if($request->ajax()){
            if(session('role_id')){
                $msg = "yes";
            }else{
                $msg = "no";
            }
            return response()->json([
                "message" => $msg
            ],200);
        }

        return redirect('home');
    }

    /**
     * Metodo para mostrar el dashboard
     * 
     * @param  \Illuminate\Http\Request $request
     */    
    public function dashboard(Request $request)
    {
            if($request->ajax()){
            
            $clients = Client::all()->count();
            $products = Product::all()->count();
            $sunatInvoicesErrors = Condition::remarks('true')->count();

            $bestSales = PresentationProduct::with('product','presentation')->withCount(['saleDetails' => function($query) {
                $query->select(DB::raw('sum(quantity)'));
            }])->orderBy('sale_details_count','DESC')->take(10)->get();
            //$minStockProducts=[];
            $minStockProducts= Product::where('stock','<','minStock')->take(10)->get();
            /*foreach ($allProducts as $product){
                if($product->stock<$product->minStock){
                    $minStockProducts[]=$product;
                }
            }*/
            //$minStockProducts = collect($minStockProducts);
            $salesReport = DB::table('sales')
                 ->join('sale_details','sales.id', '=', 'sale_details.sale_id')
                 ->selectRaw('date,sum(quantity*price) as total')
                 ->groupByRaw('sales.date')
                 ->take(30)
                 ->get();
            return response()->json([
                'clients'=>$clients,
                'products'=>$products,
                'sunatInvoicesErrors'=>$sunatInvoicesErrors,
                'minStockProducts' => $minStockProducts->sortBy('stock')->values()->all(),
                'bestSales' => $bestSales,
                'salesReport' => $salesReport
            ],
            200);
        }

        return redirect('home');
    }

    public function checkSunatInvoice(Request $request){
        if($request->ajax()){
            try{
                $response = Http::get('http://localhost:9000/');
                return response()->json([ "state" => $response->ok()]);
            } catch (\Exception $e) {
                return response()->json([ "state" => false]);
            }
        }
    }
}
