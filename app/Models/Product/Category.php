<?php

namespace App\Models\Product;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Product\Product;

class Category extends Model
{
    use HasFactory;
    /**
     * Atributos que permiten asignacion masiva
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'detail'
    ];

     #RELACIONES

    /**
     * Relacion de Categoria con Producto
     *
     * @return collection \App\Models\Product\Product
     */
    public function products()
    {
        return $this->hasMany(Product::class);
    }

    #ALMACENAMIENTO
    
    #VALIDACIÓN
    
    #RECUPERACIÓN DE INFORMACIÓN
    
    /* Query Scopes*/
    public function scopeName($query,$find)
    {
        if($find)
            return $query->where('name','LIKE',"%$find%");
    }

    #OTRAS OPERACIONES
}
