<?php

namespace App\Models\Product;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Product\Product;
use App\Models\Product\Presentation;
use App\Models\Entry\BuyDetail;
use App\Models\Sale\SaleDetail;

class PresentationProduct extends Model
{
    use HasFactory;

    protected $table = 'presentation_product';

    /**
     * Atributos calculados o generados
     *
     * @var array
     */
    protected $appends=['stock'];

    #RELACIONES

    /**
     * Relacion de PresentacionProducto con Producto
     *
     * @return collection \App\Models\Product\Product
     */
    public function product()
    {
        return $this->belongsTo(Product::class);
    }

    /**
     * Relacion de PresentacionProducto con Presentacion
     *
     * @return collection \App\Models\Product\Presentation
     */
    public function presentation()
    {
        return $this->belongsTo(Presentation::class);
    }

    /**
     * Relacion de PresentacionProducto con Compra
     *
     * @return collection \App\Models\Entry\BuyDetail
     */
    public function buyDetails()
    {
        return $this->hasMany(BuyDetail::class);
    }

     /**
     * Relacion de PresentacionProducto con Venta
     *
     * @return collection \App\Models\Sale\SaleDetail
     */
    public function saleDetails()
    {
        return $this->hasMany(SaleDetail::class);
    }
    /**
     * Relacion de PresentacionProducto con Venta
     *
     * @return collection \App\Models\Sale\SaleDetail
     */
    public function suntaInvoiceDetails()
    {
        return $this->hasMany(SaleDetail::class);
    }
    #ALMACENAMIENTO
    
    #VALIDACIÓN
    
    #RECUPERACIÓN DE INFORMACIÓN
    /**
     * Obtener el stock de los productos por presentacion
     *
     * @return double stock
     */
    public function getStockAttribute()
    {
        return $this->product()->get()[0]->stock/$this->quantity;
    }

    /* Query Scopes*/
    public function scopeProductName($query,$find)
    {
        if($find)
            return $query->orWhereHas('product',function($query) use($find){$query->where('name','LIKE',"%$find%")->orWhere('code','LIKE',"%$find%");});
    }

    #OTRAS OPERACIONES
}
