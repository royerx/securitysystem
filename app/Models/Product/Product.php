<?php

namespace App\Models\Product;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Product\Category;
use App\Models\Product\Laboratory;
use App\Models\Product\Presentation;
use App\Models\Product\PresentationProduct;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\DB;

class Product extends Model
{
    use HasFactory;
    const PICTURE_PATH = '/img/products';
    const DEFAULT_PICTURE = '/img/products/product.png';

    /**
     * Atributos que permiten asignacion masiva
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'minStock',
        'detail',
        'picture',
        'code',
        'stock',
        'category_id',
        'laboratory_id',
    ];
    /**
     * Atributos calculados o generados
     *
     * @var array
     */
    //protected $appends=['fullName','stock'];
    //protected $appends=['fullName'];
    
    #RELACIONES

    /**
     * Relacion de Producto con Categoria
     *
     * @return collection \App\Models\Product\Category
     */
    public function category()
    {
        return $this->belongsTo(Category::class);
    }
    /**
     * Relacion de Producto con Laboratorio
     *
     * @return collection \App\Models\Product\Laboratory
     */
    public function laboratory()
    {
        return $this->belongsTo(Laboratory::class);
    }
    /**
     * Relacion de Producto con Presentacion
     *
     * @return collection \App\Models\Product\Presentation
     */
    public function presentations()
    {
        return $this->belongsToMany(Presentation::class)->withPivot('id','quantity','buyPrice','salePrice')->withTimestamps();
    }
    /**
     * Relacion de Producto con Presentacion de Producto
     *
     * @return collection \App\Models\Product\PresentationProduct
     */
    public function presentationProducts()
    {
        return $this->hasMany(PresentationProduct::class);
    }
    
    #ALMACENAMIENTO
    /**
     * Guardar Producto
     *
     * @param  \Illuminate\Http\Request $request
     * @return \App\Models\Product\Product
     */
    public function store($request)
     {
          try {
            
            DB::beginTransaction();
               if($request->photo){
                    $picture = $this->savePicture($request->photo);
                }else{
                    $picture=self::DEFAULT_PICTURE;
                }
                $product = self::create(array_merge($request->all(),[
                    'picture' => $picture,
                    'stock' => 0
                ]));
                $product->presentations()->attach(session('singlePresentation_id'), ['quantity' => 1, 'buyPrice' => $request->buyPrice,'salePrice' => $request->salePrice]);
                $product->save();
            DB::commit();
            
            return $product;
        } catch (\Exception $e) {
            DB::rollback();
            return null;
        }
        
     }

     /**
     * Eliminar Producto
     *
     * @param \App\Models\Product\Product
     * @return \App\Models\Product\Product
     */
     public function remove($product){
        try {
            
            DB::beginTransaction();
                $this->deletePicture($product->picture);
                $product->presentations()->detach();
                $product = $product->delete();
            DB::commit();
            
            return $product;
        } catch (\Exception $e) {
            DB::rollback();
            return null;
        }
     }
    
    #VALIDACIÓN
    
    #RECUPERACIÓN DE INFORMACIÓN

    /**
     * Obtener el nombreCompleto
     *
     * @return string path
     */
    /*public function getFullNameAttribute()
    {
        return $this->category()->get()[0]->name.' '.$this->brand()->get()[0]->name.' '.$this->name;
    }*/

    /* Query Scopes*/
    public function scopeName($query,$find)
    {
        if($find)
            return $query->orWhere('name','LIKE',"%$find%");
    }

    public function scopeCode($query,$find)
    {
        if($find)
            return $query->orWhere('code','LIKE',"%$find%");
    }
    /**
     * Obtener el stock de los productos
     *
     * @return double stock
     */
    public function getStock()
    {
        /*$presentation =  $this->presentationProducts()->get();
        $buy=0;
        $sale=0;
        foreach ($presentation as $p){
           $buy += $p->BuyDetails()->sum('quantity')*$p->quantity;
           $sale += $p->SaleDetails()->sum('quantity')*$p->quantity;
        }
        $result=$buy-$sale;
        return $result;*/
        $cantBuy = DB::table('buy_details as B')
                        ->join('presentation_product as P','B.presentation_product_id','=','P.id')
                        ->selectRaw("SUM(B.quantity*P.quantity) as buy")
                        ->Where('P.product_id',$this->id)->value('buy');
        $cantSale = DB::table('sale_details as S')
                        ->join('presentation_product as P','S.presentation_product_id','=','P.id')
                        ->selectRaw("SUM(S.quantity*P.quantity) as sale")
                        ->Where('P.product_id',$this->id)->value('sale');
        return $cantBuy-$cantSale;
    }
    #OTRAS OPERACIONES
    /**
     * Guardar foto
     *
     * @param  image $photo
     * @param  string $picture
     * @return string path guardado
     */
    public function savePicture($photo,$picture = NULL)
    {
        $this->deletePicture($picture);
        $extension= $photo->extension();
        $path=$photo->storeAs(self::PICTURE_PATH,time()."_".Str::random(20).".{$extension}",'public');
        return  $path;
    }
    
    /**
     * Eliminar foto
     *
     * @param  string $picture
     */
    public function deletePicture($picture){
        if($picture && $picture!=self::DEFAULT_PICTURE){
            Storage::disk('public')->delete("$picture");
        }
    }
}
