<?php

namespace App\Models\Product;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Product\Product;

class Presentation extends Model
{
    use HasFactory;
    
    const SINGLE_PRESENTATION = 'Individual';

    /**
     * Atributos que permiten asignacion masiva
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'detail'
    ];

     #RELACIONES

    /**
     * Relacion de Presentacion con Producto
     *
     * @return collection \App\Models\Product\Product
     */
    public function products()
    {
        return $this->belongsToMany(Product::class)->withPivot('id','quantity','buyPrice','salePrice')->withTimestamps();
    }

    #ALMACENAMIENTO
    
    #VALIDACIÓN
    
    #RECUPERACIÓN DE INFORMACIÓN
    
    /* Query Scopes*/
    public function scopeName($query,$find)
    {
        if($find)
            return $query->where('name','LIKE',"%$find%");
    }

    #OTRAS OPERACIONES
}
