<?php

namespace App\Models\Security;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Str;
use App\Models\Security\Role;
use App\Models\Security\Permission;
use App\Mail\UserRegistered;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable;

    const PICTURE_PATH = '/img/profiles';
    const DEFAULT_PICTURE = '/img/profiles/user.png';

    /**
     * Atributos que permiten asignacion masiva
     *
     * @var array
     */
    protected $fillable = [
        'lastName',
        'firstName',
        'dni',
        'address',
        'phone',
        'picture',
        'login',
        'email',
        'password',
    ];

    /**
     * Atributos que seran ocultos
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * Atributos convertidos a tipos nativos
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    #RELACIONES
      
    /**
     * Relacion de Usuario con Rol  
     *
     * @return collection \App\Models\Security\Role
     */
    public function roles()
    {
        return $this->belongsToMany(Role::class);
    }

    #ALMACENAMIENTO
        
    /**
     * Guardar Usuario
     *
     * @param  \Illuminate\Http\Request $request
     * @return \App\Models\Security\User
     */
    public function store($request)
     {
        $picture=self::DEFAULT_PICTURE;
        $password = bcrypt($request->password);
        $user = self::create(array_merge($request->all(),[
            'picture' => $picture, 
            'password' => $password
        ]));
        return $user;
     }
     
     /**
     * Eliminar Usuario
     *
     * @param \App\Models\Security\User
     * @return \App\Models\Security\User
     */
     public function remove($user){
        try {
            
            DB::beginTransaction();
                $this->deletePicture($user->picture);
                $user->roles()->detach();
                $user = $user->delete();
            DB::commit();
            
            return $user;
        } catch (\Exception $e) {
            DB::rollback();
            return null;
        }
     }
    #VALIDACIÓN
    
    #RECUPERACIÓN DE INFORMACIÓN

    /* Query Scopes*/
    public function scopeFirsName($query,$find)
    {
        if($find)
            return $query->orWhere('firstName','LIKE',"%$find%");
    }

    public function scopeLastName($query,$find)
    {
        if($find)
            return $query->orWhere('lastName','LIKE',"%$find%");
    }
    public function scopeDni($query,$find)
    {
        if($find)
            return $query->orWhere('dni','LIKE',"%$find%");
    }
    public function scopeLogin($query,$find)
    {
        if($find)
            return $query->orWhere('login','LIKE',"%$find%");
    }
    public function scopeEmail($query,$find)
    {
        if($find)
            return $query->orWhere('email','LIKE',"%$find%");
    }
    #OTRAS OPERACIONES

        
    /**
     * Guardar foto
     *
     * @param  image $photo
     * @param  string $picture
     * @return string path guardado
     */
    public function savePicture($photo,$picture = NULL)
    {
        $this->deletePicture($picture);
        $extension= $photo->extension();
        $path=$photo->storeAs(self::PICTURE_PATH,time()."_".Str::random(20).".{$extension}",'public');
        return  $path;
    }
    
    /**
     * Eliminar foto
     *
     * @param  string $picture
     */
    public function deletePicture($picture){
        if($picture && $picture!=self::DEFAULT_PICTURE){
            Storage::disk('public')->delete("$picture");
        }
    }
    
    /**
     * Verificar permiso
     *
     * @param  string $permission
     * @return boolean
     */
    public function hasPermission($permission)
    {
       
        $role = Role::whereId(session('role_id'))->first();
        if ($role->special=="all-access"){
            return true;
        }

        if ($role->special=="no-access"){
            return false;
        }

        foreach($role->permissionActions()->get() as $item){
            if($item->slug==$permission){
                return true;
            }
        }

        return false;

    }
    
    /**
     * Asignar rol
     *
     * @param  array \App\Models\Security\Role $roles
     * @return void
     */
    public function setRoleSession($roles){
        if($roles->count()==1){
            Session::put([
                'role_id' => $roles[0]->id,
            ]);
        }
    }
}
