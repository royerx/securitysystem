<?php

namespace App\Models\System;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\System\SunatInvoice;
use App\Models\Product\PresentationProduct;
use App\Models\Sale\SaleDetail;

class SunatInvoiceDetail extends Model
{
    use HasFactory;
    /**
     * Atributos que permiten asignacion masiva
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'description',
        'special'
    ];
    #RELACIONES

    /**
     * Relacion de Detalle con Facturacion sunat  
     *
     * @return collection \App\Models\System\SunatInvoice
     */
    public function sunatInvoice()
    {
        return $this->belongsTo(SunatInvoice::class);
    }

    /**
     * Relacion de Detalle de facturacion con detalle de venta  
     *
     * @return collection \App\Models\Sale\SaleDetail
     */
    public function saleDetail()
    {
        return $this->belongsTo(SaleDetail::class);
    }
    /**
     * Relacion de Detalle de Venta con Presentacion de Producto
     *
     * @return collection \App\Models\Sale\SaleDetail
     */
    public function presentationProduct()
    {
        return $this->belongsTo(PresentationProduct::class,'presentation_product_id');
    }
    #ALMACENAMIENTO

    /**
     * Guardar rol
     *
     * @param  \Illuminate\Http\Request $request
     * @return \App\Models\Security\Role
     */
    public function store($request)
     {
         return self::create($request->all());
     }
          
     /**
     * Modificar rol
     *
     * @param  \Illuminate\Http\Request $request
     * @param \App\Models\Security\Role
     * @return \App\Models\Security\Role
     */
     public function my_update($request)
     {
         return self::update($request->all());
     }
     /**
     * Eliminar Rol
     *
     * @param \App\Models\Security\Role
     * @return \App\Models\Security\Role
     */
     public function remove($role){
        try {
            
            DB::beginTransaction();
                $role->permissionActions()->detach();
                $role = $role->delete();
            DB::commit();
            
            return $role;
        } catch (\Exception $e) {
            DB::rollback();
            return null;
        }
     }
    #VALIDACIÓN
    
    #RECUPERACIÓN DE INFORMACIÓN
    
    /* Query Scopes*/
    public function scopeName($query,$find)
    {
        if($find)
            return $query->Where('name','LIKE',"%$find%");
    }
    
    #OTRAS OPERACIONES
}
