<?php

namespace App\Models\System;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\System\SunatInvoice;

class Condition extends Model
{
    use HasFactory;
    /**
     * The database connection used by the model.
     *
     * @var string
     */
     protected $connection = 'sqlite';

      /**
     * The database table used by the model.
     *
     * @var string
     */
     protected $table = 'DOCUMENTO';

     const STATUS = [
        '01'=>'Por Generar XML',
        '02'=>'XML Generado',
        '03'=>'Enviado y Aceptado SUNAT',
        '04'=>'Enviado y Aceptado SUNAT con Obs.',
        '05'=>'Rechazado por SUNAT',
        '06'=>'Con Errores',
        '07'=>'Por Validar XML',
        '08'=>'Enviado a SUNAT Por Procesar',
        '09'=>'Enviado a SUNAT Procesando',
        '10'=>'Rechazado por SUNAT',
        '11'=>'Enviado y Aceptado SUNAT',
        '12'=>'Enviado y Aceptado SUNAT con Obs'
        ];

        protected $appends=['state'];

    /**
     * Relacion de Estado de documento con Facturacion Electronica
     *
     * @return collection \App\Models\System\SunatInvoice
     */
    public function sunatInvoice()
    {
        return $this->belongsTo(SunatInvoice::class,'NUM_DOCU','documentNumber');
    }
    #RECUPERACIÓN DE INFORMACIÓN
    
    /**
     * Obtener el estado del documento electronico
     *
     * @return string estado
     */
    public function getStateAttribute()
    {
        return self::STATUS[$this->IND_SITU];
    }

    /* Query Scopes*/
    public function scopeDocumentNumber($query,$find)
    {
        if($find)
            return $query->orwhere('NUM_DOCU','LIKE',"%$find%");
    }

    public function scopeRemarks($query,$find)
    {
        if($find=='true')
            return $query->orWhere('DES_OBSE','<>',"-");
    }
}
