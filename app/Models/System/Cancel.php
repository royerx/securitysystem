<?php

namespace App\Models\System;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Carbon\Carbon;

class Cancel extends Model
{
    use HasFactory;
    /**
     * Atributos que permiten asignacion masiva
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'date',
        'correlative',
        'reason',
    ];
    #RELACIONES

    /**
     * Relacion de Dar de baja con documento electronico
     *
     * @return collection \App\Models\System\SunatInvoice
     */
    public function sunatInvoice()
    {
        return $this->belongsTo(SunatInvoice::class,'id');
    }

    #ALMACENAMIENTO

    /**
     * Guardar Venta y sus detalles
     *
     * @param  \Illuminate\Http\Request $request
     * @return \App\Models\Security\User
     */
    public function store($request)
     {
        try {
            DB::beginTransaction();
                if($request->sunatInvoice->type =="Boleta" || $request->sunatInvoice->type=="Factura"){
                    $sunatInvoice = SunatInvoice::find($request->sunatInvoice->id);
                    
                    $cancel = new Cancel();
                    $cancel->id=$sunatInvoice->id;
                    $cancel->date=Carbon::now();
                    $cancel->reason=$request->cancel['reason'];
                    
                    $correlative = Cancel::where('date',$cancel->date)->max('correlative');
                    $correlative = str_pad($correlative+1, 3, "0", STR_PAD_LEFT);
                    
                    $cancel->correlative = $correlative;

                    $cancel->generateFiles();
                    $sunatInvoice->updateTable();
                    $generate = $sunatInvoice->generateInvoice();
                    if($generate['validacion']=="EXITO" && strlen($generate['mensaje'])>0){
                        DB::rollback();
                        return null;
                    }
                    $sunatInvoice->sendInvoice();
                    $sunatInvoice->cancel=true;
                    $sunatInvoice->sale_id=null;
                    $sunatInvoice->save();
                    $cancel->save();
                }
            DB::commit();
            return $cancel;
        } catch (\Exception $e) {
            DB::rollback();
            return null;
        }
     }

    #VALIDACIÓN
    
    #RECUPERACIÓN DE INFORMACIÓN
    
    #OTRAS OPERACIONES
    /**
     * Generar archivos para dar de baja una boleta o una factura
     * 
     * @param $sale
     */
    public function generateFiles()
    {
        $this->load('sunatInvoice');
        #creacion del archivo de cabecera
        switch($this->sunatInvoice['type']){
            case "Boleta":
                $prefix="B";
                break;
            case "Factura":
                $prefix="F";
                break;
        }
        $fecGeneracion =$this->sunatInvoice['date'];
        $fecComunicacion =Carbon::parse($this->date)->format('Y-m-d');
        $tipDocBaja =SunatInvoice::DOCUMENT_TYPE[$this->sunatInvoice['type']];
        $numDocBaja = $prefix.$this->sunatInvoice['serie'].'-'.$this->sunatInvoice['correlative'];
        $desMotivoBaja =$this->reason;
        
        $file = fopen(Session::get('invoice')->path."/sunat_archivos/sfs/DATA/".Session::get('invoice')->ruc."-RA-".Carbon::parse($this->date)->format('Ymd')."-".$this->correlative.".CBA",'w');
        fwrite($file,$fecGeneracion.'|'.$fecComunicacion.'|'.$tipDocBaja.'|'.$numDocBaja.'|'.$desMotivoBaja);
        fclose($file);
    }
}
