<?php

namespace App\Models\System;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Session;
use App\Models\System\SunatInvoiceDetail;
use Carbon\Carbon;
use App\Models\System\Invoice;
use App\Models\Sale\Sale;
use Luecano\NumeroALetras\NumeroALetras;
use Illuminate\Support\Facades\Http;
use SimpleSoftwareIO\QrCode\Facades\QrCode;
use App\Models\Sale\Client;
use App\Models\Security\User;

class SunatInvoice extends Model
{
    use HasFactory;
    /**
     * Atributos que permiten asignacion masiva
     *
     * @var array
     */
    protected $fillable = [
        'type',
        'serie',
        'correlative',
        'documentNumber',
        'date',
        'time',
        'dueDate',
        'currency',
        'wayPay',
        'quota',
        'quotaAmount',
        'methodPay',
        'totalSaleGravada',
        'totalExonerated',
        'totalUnaffected',
        'igv',
        'total',
        'retention',
        'amountRetention',
        'pendingPay',
        'hash',
        'reference',
        'state',
        'cancel',
        'sale_id',
        'sale_detail_id'
    ];
    protected $connection = 'mysql';

    const QRCODE_PATH = 'img/qrcodes/';
    /**
     * Catalogos de Sunat
     *
     */
    
     # Tipo de comprobante de pago
     const DOCUMENT_TYPE = [
        'Factura' => '01',
        'Boleta' => '03',
        'Liquidación de compra' => '04',
        'Nota de crédito' => '07',
        'Nota de débito' => '08',
        'Guía de remisión remitente' => '09',

    ];

    #tipo de tributos
    const TAX_TYPE=[
        'Gravado'/*IGV Impuesto General a las Ventas*/ => [
            'Nombre' => 'IGV',
            'Codigo Internacional' => 'VAT',
            'Codigo' => '1000'
        ],
        'Impuesto a la Venta Arroz Pilado' => [
            'Nombre' => 'IVAP',
            'Codigo Internacional' => 'VAT',
            'Codigo' => '1016'
        ],
        'ISC Impuesto Selectivo al Consumo' => [
            'Nombre' => 'ISC',
            'Codigo Internacional' => 'EXC',
            'Codigo' => '2000'
        ],
        'Impuesto a la bolsa plastica' => [
            'Nombre' => 'ICBPER',
            'Codigo Internacional' => 'OTH',
            'Codigo' => '7152'
        ],
        'Exportación' => [
            'Nombre' => 'EXP',
            'Codigo Internacional' => 'FRE',
            'Codigo' => '9995'
        ],
        'Gratuito' => [
            'Nombre' => 'GRA',
            'Codigo Internacional' => 'FRE',
            'Codigo' => '9996'
        ],
        'Exonerado' => [
            'Nombre' => 'EXO',
            'Codigo Internacional' => 'VAT',
            'Codigo' => '9997'
        ],
        'Inafecto' => [
            'Nombre' => 'INA',
            'Codigo Internacional' => 'FRE',
            'Codigo' => '9998'
        ],
        'Otros tributos' => [
            'Nombre' => 'OTROS',
            'Codigo Internacional' => 'OTH',
            'Codigo' => '9999'
        ]

    ];

    #tipo de documento de identidad
    const IDENTITY_DOCUMENT_TYPE = [
        'DOC.TRIB.NO.DOM.SIN.RUC' => '0',
        'DNI' => '1',
        'Carnet de extranjería' => '4',
        'RUC' => '6',
        'Pasaporte' => '7'
    ];

    #tipo de afectacion de IGV
    const IGV_AFFECTATION = [    
        'Gravado - Operación Onerosa' => [
            'codigo tributo' => '1000',
            'codigo' => '10'
        ],        
        'Gravado - Retiro por premio' => [
            'codigo tributo' => '9996',
            'codigo' => '11'
        ],        
        'Gravado - Retiro por donación' => [
            'codigo tributo' => '9996',
            'codigo' => '12'
        ],        
        'Gravado - Retiro ' => [
            'codigo tributo' => '9996',
            'codigo' => '13'
        ],        
        'Gravado - Retiro por publicidad' => [
            'codigo tributo' => '9996',
            'codigo' => '14'
        ],        
        'Gravado - Bonificaciones' => [
            'codigo tributo' => '9996',
            'codigo' => '15'
        ],        
        'Gravado - Retiro por entrega a trabajadores' => [
            'codigo tributo' => '9996',
            'codigo' => '16'
        ],        
        'Gravado - IVAP' => [
            'codigo tributo' => '1016o 9996',
            'codigo' => '17'
        ],       
        'Exonerado - Operación Onerosa' => [
            'codigo tributo' => '9997',
            'codigo' => '20'
        ],        
        'Exonerado - Transferencia gratuita' => [
            'codigo tributo' => '9996',
            'codigo' => '21'
        ],        
        'Inafecto - Operación Onerosa' => [
            'codigo tributo' => '9998',
            'codigo' => '30'
        ],        
        'Inafecto - Retiro por Bonificación' => [
            'codigo tributo' => '9996',
            'codigo' => '31'
        ],        
        'Inafecto - Retiro' => [
            'codigo tributo' => '9996',
            'codigo' => '32'
        ],        
        'Inafecto - Retiro por Muestras Médicas' => [
            'codigo tributo' => '9996',
            'codigo' => '33'
        ],        
        'Inafecto - Retiro por Convenio Colectivo' => [
            'codigo tributo' => '9996',
            'codigo' => '34'
        ],        
        'Inafecto - Retiro por premio' => [
            'codigo tributo' => '9996',
            'codigo' => '35'
        ],        
        'Inafecto - Retiro por publicidad' => [
            'codigo tributo' => '9996',
            'codigo' => '36'
        ],        
        'Inafecto - Transferencia gratuita' => [
            'codigo tributo' => '9996',
            'codigo' => '37'
        ],        
        'Exportación de Bienes o Servicios' => [
            'codigo tributo' => '9995 o 9996' ,
            'codigo' => '40'
        ],
    ];

    # Tipo sistema de caculo del impuesto selectivo al consumo
    const ISC_CALC = [
        'Sistema al valor (Apéndice IV, lit. A - T.U.O IGV e ISC)' => [
            'tasa' => 1.00,
            'codigo' => '01'
        ],
        'Aplicación del Monto Fijo ( Sistema específico, bienes en el apéndice III, Apéndice IV, lit. B - T.U.O IGV e ISC)' => [
            'tasa' => 2.00,
            'codigo' => '02'
        ],
        'Sistema de Precios de Venta al Público (Apéndice IV, lit. C - T.U.O IGV e ISC)' => [
            'tasa' => 0.5,
            'codigo' => '03'
        ]
    ];

    #Tipo de nota de credito
    const CREDIT_NOTE_TYPE = [
        'Anulación de la operación' => '01',
        'Anulación por error en el RUC' => '02',
        'Corrección por error en la descripción' => '03',
        'Descuento global' => '04',
        'Descuento por ítem' => '05',
        'Devolución total' => '06',
        'Devolución por ítem' => '07',
        'Bonificación' => '08',
        'Disminución en el valor' => '09',
        'Otros Conceptos' => '10',
        'Ajustes de operaciones de exportación' => '11',
        'Ajustes afectos al IVAP' => '12'
    ];
    
    #Tipo de nota de debito
    const DEBIT_NOTE_TYPE = [
        'Intereses por mora' => '01',
        'Aumento en el valor' => '02',
        'Penalidades/otros conceptos' => '03',
        'Ajustes de operaciones de exportación' => '11',
        'Ajustes afectos al IVAP' => '12'
    ];
    
    #Tipo de operacion
    const OPERATION_TYPE = [
        'Venta interna' => '0101',
        'Venta Interna - Sustenta Gastos Deducibles Persona Natural'=> '0112',
        'Venta Interna-NRUS' => '0113',
        'Exportación de Bienes '=> '0200',
        'Exportación de Servicios - Prestación servicios realizados íntegramente en el país' => '0201',
        'Exportación de Servicios - Prestación de servicios de hospedaje No Domiciliado' => '0202',
        'Exportación de Servicios - Transporte de navieras' => '0203',
        'Exportación de Servicios - Servicios a naves y aeronaves de bandera extranjera' => '0204',
        'Exportación de Servicios - Servicios que conformen un Paquete Turístico' => '0205',
        'Exportación de Servicios - Servicios complementarios al transporte de carga' => '0206',
        'Exportación de Servicios - Suministro de energía eléctrica a favor de sujetos domiciliados en ZED' => '0207',
        'Exportación de Servicios - Prestación servicios realizados parcialmente en el extranjero' => '0208',
        'Operaciones con Carta de porte aéreo (emitidas en el ámbito nacional)' => '0301',
        'Operaciones de Transporte ferroviario de pasajeros' => '0302',
        'Operaciones de Pago de regalía petrolera' => '0303',
        'Ventas no domiciliados que no califican como exportación' => '0401',
        'Operación Sujeta a Detracción' => '1001',
        'Operación Sujeta a Detracción- Recursos Hidrobiológicos' => '1002',
        'Operación Sujeta a Detracción- Servicios de Transporte Pasajeros' => '1003',
        'Operación Sujeta a Detracción- Servicios de Transporte Carga' => '1004',
        'Operación Sujeta a Percepción' => '2001'
    ];

    #Codigos de Leyendas
    const LEGEND_CODE = [
        'Monto en Letras' => '1000',
        'TRANSFERENCIA GRATUITA DE UN BIEN Y/O SERVICIO PRESTADO GRATUITAMENTE' => '1002',
        'COMPROBANTE DE PERCEPCIÓN' => '2000',
        'Operación sujeta a detracción' => '2006',
        'Operación sujeta al IVAP' => '2007'
    ];

    #RELACIONES

    /**
     * Relacion de Facturacion Electronica con detalles
     *
     * @return collection \App\Models\System\SunatInvoiceDetail
     */
    public function details()
    {
        return $this->hasMany(SunatInvoiceDetail::class);
    }
    /**
     * Relacion de Facturacion Electronica con venta
     *
     * @return collection \App\Models\Sale\Sale
     */
    public function sale()
    {
        return $this->belongsTo(Sale::class);
    }

    /**
     * Relacion de documento electronico con Client
     *
     * @return collection \App\Models\Sale\Client
     */
    public function client()
    {
        return $this->belongsTo(Client::class);
    }

     /**
     * Relacion de documento electronico con Usuario
     *
     * @return collection \App\Models\Security\User
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * Relacion de documento electronico con Dar de baja
     *
     * @return collection \App\Models\System\Condition
     */
    public function condition()
    {
        return $this->hasOne(Condition::class,'NUM_DOCU','documentNumber');
    }

    /**
     * Relacion de documento electronico con Estado de documento
     *
     * @return collection \App\Models\System\Cancel
     */
    public function cancel()
    {
        return $this->hasOne(Cancel::class,'id');
    }
    #ALMACENAMIENTO
    /**
     * Guardar Venta y sus detalles
     *
     * @param  \Illuminate\Http\Request $request
     * @return \App\Models\Security\User
     */
    public function store($sale)
     {
        if($sale->type =="Boleta" || $sale->type=="Factura"){
            $sunatInvoice = new SunatInvoice();
            $sunatInvoice->type=$sale->type;
            $number = Sale::getDocumentNumber($sale->type);
            $sunatInvoice->serie=$number['serie'];
            $sunatInvoice->correlative=$number['correlative'];
            $sunatInvoice->documentNumber=substr($sale->type,0,1).$number['serie'].'-'.$number['correlative'];
            $sunatInvoice->date=$sale->date;
            $sunatInvoice->time=Carbon::now();
            $sunatInvoice->currency="PEN";
            $sunatInvoice->wayPay="Contado";
            $sunatInvoice->quota="1";
            $sunatInvoice->totalSaleGravada=0;
            $sunatInvoice->totalExonerated=0;
            $sunatInvoice->totalUnaffected=0;
            $sunatInvoice->cancel=false;
            $sunatInvoice->sale_id=$sale->id;
            $sunatInvoice->client_id=$sale->client_id;
            $sunatInvoice->user_id=$sale->user_id;
            $details=[];
            foreach($sale->saleDetails()->get() as $d){
                $sunatInvoiceDetail = new SunatInvoiceDetail();

                $sunatInvoiceDetail->measure="UNIDAD";
                $sunatInvoiceDetail->tax=$d['typeIgv'];
                $sunatInvoiceDetail->price=$d['price'];
                $sunatInvoiceDetail->quantity=$d['quantity'];

                $sunatInvoice->quotaAmount+=round($d['price']*$d['igv'],2);
                switch($d['typeIgv']){
                    case "Gravado":
                        $sunatInvoiceDetail->unitPrice=round($d['price']/(1+Session::get('igv')),2);
                        $sunatInvoiceDetail->subTotal = round(($sunatInvoiceDetail->unitPrice*$d['quantity']),2);
                        $sunatInvoiceDetail->amountTax = round($d['price']*$d['quantity']-$sunatInvoiceDetail->subTotal,2); 
                        $sunatInvoiceDetail->typeTaxAllocation="10";

                        $sunatInvoice->totalSaleGravada+=$sunatInvoiceDetail->subTotal;
                        $sunatInvoice->igv+=$sunatInvoiceDetail->amountTax;
                                
                        break;
                    case "Exonerado":
                        $sunatInvoiceDetail->subTotal = round($d['price']*$d['quantity'],2);
                        $sunatInvoiceDetail->amountTax=0.00;
                        $sunatInvoiceDetail->typeTaxAllocation="20";
                        $sunatInvoiceDetail->unitPrice=$d['price'];

                        $sunatInvoice->totalExonerated += $sunatInvoiceDetail->subTotal;
                                
                        break;
                    case "Inafecto":
                        $sunatInvoiceDetail->subTotal = round($d['price']*$d['quantity'],2);
                        $sunatInvoiceDetail->amountTax=0.00;
                        $sunatInvoiceDetail->typeTaxAllocation="30";
                        $sunatInvoiceDetail->unitPrice=$d['price'];

                        $sunatInvoice->totalUnaffected += $sunatInvoiceDetail->subTotal;
                                
                        break;
                }
                
                $sunatInvoice->total += round($d['price']*$d['quantity'],2);
                $sunatInvoiceDetail->sale_detail_id = $d['id'];
                $sunatInvoiceDetail->presentation_product_id=$d['presentation_product_id'];
                
                $details[]=$sunatInvoiceDetail;

            }
            $sunatInvoice->retention="No";
            $sunatInvoice->amountRetention=0.00;
            $sunatInvoice->pendingPay=0.00;
            $sunatInvoiceDetail->sale_detail_id = $d['id'];

            $sunatInvoice->save();
            $sunatInvoice->details()->saveMany($details);
            $sunatInvoice->generateFiles($sale);
            return $sunatInvoice;
        }
     }
    #VALIDACIÓN
    
    #RECUPERACIÓN DE INFORMACIÓN
    /* Query Scopes*/
    public function scopeClient($query,$find)
    {
        if($find)
            return $query->WhereHas('client',function($query) use($find){$query->Where('firstName','LIKE',"%$find%")->orWhere('lastName','LIKE',"%$find%")->orWhere('documentNumber','LIKE',"%$find%");});
    }

    public function scopeSerie($query,$find)
    {
        if($find)
            return $query->orwhere('serie','LIKE',"%$find%");
    }

    public function scopeCorrelative($query,$find)
    {
        if($find)
            return $query->orWhere('correlative','LIKE',"%$find%");
    }
    
    #OTRAS OPERACIONES
    /**
     * Generar archivos para generar documentos electronicos
     * 
     * @param $sale
     */
    public function generateFiles($sale)
    {
        $sale->load(['client',
                    'user',
                    'sunatInvoices'=> function ($query) {
                        $query->where('cancel', false)->with('details.presentationProduct.product',
                                                            'details.presentationProduct.presentation');
                    }
                ]);
        #creacion del archivo de cabecera
        switch($sale->type){
            case "Boleta":
                $prefix="B";
                $documentType=self::IDENTITY_DOCUMENT_TYPE['DNI'];
                break;
            case "Factura":
                $prefix="F";
                $documentType=self::IDENTITY_DOCUMENT_TYPE['RUC'];
                break;
        }
        $tipOperacion = '0101';
        $fecEmision = $sale->sunatInvoices[0]['date'];
        $horEmision = $sale->sunatInvoices[0]['time'];
        $fecVencimiento = '-';
        $codLocalEmisor = '0000';
        $tipDocUsuario = $documentType;
        $numDocUsuario = $sale->client['documentNumber'];
        $rznSocialUsuario = $sale->client['firstName'].' '.$sale->client['lastName'];
        $tipMoneda = $sale->sunatInvoices[0]['currency'];
        $sumTotTributos = $sale->sunatInvoices[0]['igv'];
        $sumTotValVenta = $sale->sunatInvoices[0]['totalSaleGravada'];
        $sumPrecioVenta = $sale->sunatInvoices[0]['total'];
        $sumDescTotal = 0.00;
        $sumOtrosCargos = 0.00;
        $sumTotalAnticipos = 0.00;
        $sumImpVenta = $sumPrecioVenta-$sumDescTotal+$sumOtrosCargos-$sumTotalAnticipos;
        $ublVersionId = "2.1";
        $customizationId = "2.0";
        
        $file = fopen(Session::get('invoice')->path."/sunat_archivos/sfs/DATA/".Session::get('invoice')->ruc."-".self::DOCUMENT_TYPE[$this->type]."-".$prefix.$this->serie."-".$this->correlative.".CAB",'w');
        fwrite($file,$tipOperacion.'|'.$fecEmision.'|'.$horEmision.'|'.$fecVencimiento.'|'.$codLocalEmisor.'|'.$tipDocUsuario.'|'.$numDocUsuario.'|'.$rznSocialUsuario.'|'.$tipMoneda.'|'.$sumTotTributos.'|'.$sumTotValVenta.'|'.$sumPrecioVenta.'|'.$sumDescTotal.'|'.$sumOtrosCargos.'|'.$sumTotalAnticipos.'|'.$sumImpVenta.'|'.$ublVersionId.'|'.$customizationId);
        fclose($file);

        #creacion del archivo detalles
        $file = fopen(Session::get('invoice')->path."/sunat_archivos/sfs/DATA/".Session::get('invoice')->ruc."-".self::DOCUMENT_TYPE[$this->type]."-".$prefix.$this->serie."-".$this->correlative.".DET",'w');
        foreach($sale->sunatInvoices[0]['details'] as $detail){
            $codUnidadMedida = 'NIU';
            $ctdUnidadItem = $detail['quantity'];
            $codProducto = '-';//$detail['presentationProduct']['product']['code'];
            $codProductoSUNAT = "-";
            $desItem = $detail['presentationProduct']['product']['name'];
            $mtoValorUnitario = $detail['unitPrice'];
            $codTriIGV = self::TAX_TYPE[$detail['tax']]['Codigo'];
            $mtoIgvItem = $detail['amountTax'];
            $mtoBaseIgvItem = $detail['subTotal'];
            $nomTributoIgvItem = self::TAX_TYPE[$detail['tax']]['Nombre'];
            $codTipTributoIgvItem = self::TAX_TYPE[$detail['tax']]['Codigo Internacional'];
            $tipAfeIGV = $detail['typeTaxAllocation'];
            if($detail['tax']=='Gravado')
                $porIgvItem = Session::get('igv')*100;
            else
                $porIgvItem = 0.00;
            
            $codTriISC = "-";
            $mtoIscItem = "";
            $mtoBaseIscItem = "";
            $nomTributoIscItem = "";
            $codTipTributoIscItem = "";
            $tipSisISC = "";
            $porIscItem = "";
            $codTriOtro = "-";
            $mtoTriOtroItem = "";
            $mtoBaseTriOtroItem = "";
            $nomTributoOtroItem = "";
            $codTipTributoOtroItem = "";
            $porTriOtroItem = "";
            $codTriIcbper = "-";
            $mtoTriIcbperItem = "";
            $ctdBolsasTriIcbperItem = "";
            $nomTributoIcbperItem = "";
            $codTipTributoIcbperItem = "";
            $mtoTriIcbperUnidad = "";
            if($detail['tax']=='Gravado')
                $mtoPrecioVentaUnitario = $detail['price'];
            else
                $mtoPrecioVentaUnitario = $detail['price']*(1+Session::get('igv'));
            $mtoValorVentaItem = $detail['subTotal'];
            $mtoValorReferencialUnitario = 0.00;

            $sumTotTributosItem = $mtoIgvItem;//+$mtoIscItem+$mtoTriOtroItem;

            fwrite($file,$codUnidadMedida.'|'.$ctdUnidadItem.'|'.$codProducto.'|'.$codProductoSUNAT.'|'.$desItem.'|'.$mtoValorUnitario.'|'.$sumTotTributosItem.'|'.$codTriIGV.'|'.$mtoIgvItem.'|'.$mtoBaseIgvItem.'|'.$nomTributoIgvItem.'|'.$codTipTributoIgvItem.'|'.$tipAfeIGV.'|'.$porIgvItem.'|'.$codTriISC.'|'.$mtoIscItem.'|'.$mtoBaseIscItem.'|'.$nomTributoIscItem.'|'.$codTipTributoIscItem.'|'.$tipSisISC.'|'.$porIscItem.'|'.$codTriOtro.'|'.$mtoTriOtroItem.'|'.$mtoBaseTriOtroItem.'|'.$nomTributoOtroItem.'|'.$codTipTributoOtroItem.'|'.$porTriOtroItem.'|'.$codTriIcbper.'|'.$mtoTriIcbperItem.'|'.$ctdBolsasTriIcbperItem.'|'.$nomTributoIcbperItem.'|'.$codTipTributoIcbperItem.'|'.$mtoTriIcbperUnidad.'|'.$mtoPrecioVentaUnitario.'|'.$mtoValorVentaItem.'|'.$mtoValorReferencialUnitario);
        }
        fclose($file);

        #creacion de archivos de tributos
        $file = fopen(Session::get('invoice')->path."/sunat_archivos/sfs/DATA/".Session::get('invoice')->ruc."-".self::DOCUMENT_TYPE[$sale->type]."-".$prefix.$this->serie."-".$this->correlative.".TRI",'w');
        if($sale->sunatInvoices[0]['totalSaleGravada']>0){
            $ideTributo = self::TAX_TYPE['Gravado']['Codigo'];
            $nomTributo = self::TAX_TYPE['Gravado']['Nombre'];
            $codTipTributo = self::TAX_TYPE['Gravado']['Codigo Internacional'];
            $mtoBaseImponible =$sale->sunatInvoices[0]['totalSaleGravada']-$sale->sunatInvoices[0]['totalExonerated']-$sale->sunatInvoices[0]['totalUnaffected'];
            $mtoTributo = $sale->sunatInvoices[0]['igv'];
            fwrite($file,$ideTributo.'|'.$nomTributo.'|'.$codTipTributo.'|'.$mtoBaseImponible.'|'.$mtoTributo);
        }
        if($sale->sunatInvoices[0]['totalExonerated']>0){
            $ideTributo = self::TAX_TYPE['Exonerado']['Codigo'];
            $nomTributo = self::TAX_TYPE['Exonerado']['Nombre'];
            $codTipTributo = self::TAX_TYPE['Exonerado']['Codigo Internacional'];
            $mtoBaseImponible =$sale->sunatInvoices[0]['totalExonerated'];
            $mtoTributo = 0.00;
            fwrite($file,$ideTributo.'|'.$nomTributo.'|'.$codTipTributo.'|'.$mtoBaseImponible.'|'.$mtoTributo);
        }
        if($sale->sunatInvoices[0]['totalUnaffected']>0){
            $ideTributo = self::TAX_TYPE['Inafecto']['Codigo'];
            $nomTributo = self::TAX_TYPE['Inafecto']['Nombre'];
            $codTipTributo = self::TAX_TYPE['Inafecto']['Codigo Internacional'];
            $mtoBaseImponible =$sale->sunatInvoices[0]['totalUnaffected'];
            $mtoTributo = 0.00;
            fwrite($file,$ideTributo.'|'.$nomTributo.'|'.$codTipTributo.'|'.$mtoBaseImponible.'|'.$mtoTributo);
        }
        fclose($file);

        #creacion de archivos de leyenda
        $file = fopen(Session::get('invoice')->path."/sunat_archivos/sfs/DATA/".Session::get('invoice')->ruc."-".self::DOCUMENT_TYPE[$sale->type]."-".$prefix.$this->serie."-".$this->correlative.".LEY",'w');
        $formatter = new NumeroALetras();
        $codLeyenda = self::LEGEND_CODE['Monto en Letras'];
        $desLeyenda = $formatter->toInvoice($sale->sunatInvoices[0]['total'], 2, 'soles');
        fwrite($file,$codLeyenda.'|'.$desLeyenda);
        fclose($file);

        #creacion del archivo de forma de pago
        $formaPago = $sale->sunatInvoices[0]['wayPay'];
        $mtoNetoPendientePago = $sale->sunatInvoices[0]['total'];
        $tipMonedaMtoNetoPendientePago = $sale->sunatInvoices[0]['currency'];
        
        if($sale->type == "Factura"){
            $file = fopen(Session::get('invoice')->path."/sunat_archivos/sfs/DATA/".Session::get('invoice')->ruc."-".self::DOCUMENT_TYPE[$sale->type]."-".$prefix.$this->serie."-".$this->correlative.".PAG",'w');
            fwrite($file,$formaPago.'|'.$mtoNetoPendientePago.'|'.$tipMonedaMtoNetoPendientePago);
            fclose($file);
        }
        #creacion del archivo de credito
        if($sale->sunatInvoices[0]['wayPay']=="Credito"){
            $file = fopen(Session::get('invoice')->path."/sunat_archivos/sfs/DATA/".Session::get('invoice')->ruc."-".self::DOCUMENT_TYPE[$sale->type]."-".$prefix.$this->serie."-".$this->correlative.".DPA",'w');
            $mtoCuotaPago = $sale->sunatInvoices[0]['quotaAmount'];
            $fecCuotaPago = $sale->sunatInvoices[0]['dueDate'];
            $tipMonedaCuotaPago = $sale->sunatInvoices[0]['currency'];
            for($i=1;$i<=$sale->sunatInvoices[0]['quota'];$i++){
                fwrite($file,$mtoCuotaPago.'|'.$fecCuotaPago.'|'.$tipMonedaCuotaPago);
            }
            fclose($file);
        }
    }

    public function backupFiles(){
        $pathData = Session::get('invoice')->path."/sunat_archivos/sfs/DATA";
        $pathSend = Session::get('invoice')->path."/sunat_archivos/sfs/ENVIO";

        $toData=Session::get('invoice')->path."/sunat_archivos/sfs/backup/DATA/";
        $toSend=Session::get('invoice')->path."/sunat_archivos/sfs/backup/ENVIO/";
        
        if (is_dir($pathData) && is_dir($pathSend)){
        $data = opendir($pathData);
        $send = opendir($pathSend);
        while (($sfile = readdir($send)) !== false)  {

            if ($sfile != "." && $sfile != "..") {
                while(($file = readdir($data)) !== false){
                    if ($file != "." && $file != "..") {
                        if(substr($sfile, 0, -4) == substr($file,0,-4)){
                            if(file_exists($pathSend.'/'.$sfile))
                                rename($pathSend.'/'.$sfile, $toSend . pathinfo($pathSend.'/'.$sfile, PATHINFO_BASENAME));
                            
                            if(file_exists($pathData.'/'.substr($file,0,-4).'.CAB'))
                                rename($pathData.'/'.substr($file,0,-4).'.CAB', $toData . pathinfo($pathData.'/'.substr($file,0,-4).'.CAB', PATHINFO_BASENAME));
                            
                            if(file_exists($pathData.'/'.substr($file,0,-4).'.DET'))
                                rename($pathData.'/'.substr($file,0,-4).'.DET', $toData . pathinfo($pathData.'/'.substr($file,0,-4).'.DET', PATHINFO_BASENAME));
                            
                            if(file_exists($pathData.'/'.substr($file,0,-4).'.LEY'))
                                rename($pathData.'/'.substr($file,0,-4).'.LEY', $toData . pathinfo($pathData.'/'.substr($file,0,-4).'.LEY', PATHINFO_BASENAME));
                            
                            if(file_exists($pathData.'/'.substr($file,0,-4).'.TRI'))
                                rename($pathData.'/'.substr($file,0,-4).'.TRI', $toData . pathinfo($pathData.'/'.substr($file,0,-4).'.TRI', PATHINFO_BASENAME));
                            
                            if(file_exists($pathData.'/'.substr($file,0,-4).'.PAG'))
                                rename($pathData.'/'.substr($file,0,-4).'.PAG', $toData . pathinfo($pathData.'/'.substr($file,0,-4).'.PAG', PATHINFO_BASENAME));
                            
                            if(file_exists($pathData.'/'.substr($file,0,-4).'.DPA'))
                                rename($pathData.'/'.substr($file,0,-4).'.DPA', $toData . pathinfo($pathData.'/'.substr($file,0,-4).'.DPA', PATHINFO_BASENAME));
                            
                            break;
                        }
                    }
                }
            }
           
        }
        
        // Cierra el gestor de directorios
        closedir($data);
        closedir($send);
        }  
    }
    /**
     * Actualizar tabla facturador de sunat
     * 
    */
    public function updateTable(){
        return Http::withHeaders([
            'Content-Type' => 'application/json; charset=utf-8',
            ])->post('http://localhost:9000/api/ActualizarPantalla.htm',[
                "txtSecuencia"=>"000"
        ]);
    }
    /**
     * Generar archivo xml del facturador de sunat
     * 
     *  @param $sale
    */
    public function generateInvoice(){
        switch($this->type){
            case "Boleta":
                $prefix="B";
                break;
            case "Factura":
                $prefix="F";
                break;
        }
        $number = $prefix.$this->serie."-".$this->correlative;
        return Http::withHeaders([
            'Content-Type' => 'application/json; charset=utf-8',
            ])->post('http://localhost:9000/api/GenerarComprobante.htm',[
                "num_ruc"=>Session::get('invoice')->ruc,
                "tip_docu"=>self::DOCUMENT_TYPE[$this->type],
                "num_docu"=>$number
        ]);
    }

    /**
    * Enviar comprobante a la sunat con el facturador de sunat
    * 
    */
    public function sendInvoice(){
        switch($this->type){
            case "Boleta":
                $prefix="B";
                break;
            case "Factura":
                $prefix="F";
                break;
        }
        $number = $prefix.$this->serie."-".$this->correlative;
        return Http::withHeaders([
            'Content-Type' => 'application/json; charset=utf-8',
            ])->post('http://localhost:9000/api/enviarXML.htm',[
                "num_ruc"=>Session::get('invoice')->ruc,
                "tip_docu"=>self::DOCUMENT_TYPE[$this->type],
                "num_docu"=>$number
        ]);
    }

    /**
    * obtener el hash del documento
    * 
    */
    public function getHash(){
        switch($this->type){
            case "Boleta":
                $prefix="B";
                break;
            case "Factura":
                $prefix="F";
                break;
        }
        $path = Session::get('invoice')->path."/sunat_archivos/sfs/FIRMA/".Session::get('invoice')->ruc."-".self::DOCUMENT_TYPE[$this->type]."-".$prefix.$this->serie."-".$this->correlative.".xml";

        $xml = simplexml_load_file($path);
        $namespaces = $xml->getNamespaces(true);
        $xml->registerXPathNamespace('ds',$namespaces['ds']);
        return $xml->xpath('//ds:DigestValue')[0];
    }

    /**
    * obtener el QR de la venta
    * 
    */
    public function getQR(){
        $qr = Session::get('invoice')->ruc.'|'.self::DOCUMENT_TYPE[$this->type].'|'.substr($this->type,0,1).$this->serie."-".$this->correlative.'|'.$this->date.'|'.$this->igv.'|'.$this->total.'|'.$this->hash;
        QrCode::size(130)->generate($qr,'../public/'.self::QRCODE_PATH.$this->id.".svg");
    }
}