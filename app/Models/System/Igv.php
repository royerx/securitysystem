<?php

namespace App\Models\System;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Igv extends Model
{
    use HasFactory;
    /**
     * Atributos que permiten asignacion masiva
     *
     * @var array
     */
    protected $fillable = [
        'percent',
        'beginDate',
        'endDate',
    ];
    #RELACIONES

    #ALMACENAMIENTO

    #VALIDACIÓN
    
    #RECUPERACIÓN DE INFORMACIÓN
    
    #OTRAS OPERACIONES
}
