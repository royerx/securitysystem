<?php

namespace App\Models\System;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Invoice extends Model
{
    use HasFactory;

    /**
     * Atributos que permiten asignacion masiva
     *
     * @var array
     */
    protected $fillable = [
        'ruc',
        'businessName',
        'address',
        'phone',
        'email',
        'path',
        'ticketSerie',
        'ticketCorrelative',
        'invoiceSerie',
        'invoiceCorrelative',
        'creditNoteTicketSerie',
        'creditNoteTicketCorrelative',
        'creditNoteInvoiceSerie',
        'creditNoteInvoiceCorrelative',
        'debitNoteTicketSerie',
        'debitNoteTicketCorrelative',
        'debitNoteInvoiceSerie',
        'debitNoteInvoiceCorrelative',
        'remittanceSerie',
        'remittanceCorrelative',
    ];
    #RELACIONES

    #ALMACENAMIENTO

    #VALIDACIÓN
    
    #RECUPERACIÓN DE INFORMACIÓN
    
    #OTRAS OPERACIONES
}
