<?php

namespace App\Models\Sale;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Management\Person;
use App\Models\Sale\Sale;

class Client extends Person
{
    use HasFactory;
    #RELACIONES

    /**
     * Relacion de Cliente con Venta
     *
     * @return collection \App\Models\Sale\Sale
     */
    public function sales()
    {
        return $this->hasMany(Sale::class);
    }

    #ALMACENAMIENTO
    
    #VALIDACIÓN
    
    #RECUPERACIÓN DE INFORMACIÓN
    /* Query Scopes*/
    public function scopeFirstName($query,$find)
    {
        if($find)
            return $query->orWhere('firstName','LIKE',"%$find%");
    }

    public function scopeLastName($query,$find)
    {
        if($find)
            return $query->orWhere('lastName','LIKE',"%$find%");
    }
    public function scopeDocumentNumber($query,$find)
    {
        if($find)
            return $query->orWhere('documentNumber','LIKE',"%$find%");
    }
    #OTRAS OPERACIONES
}
