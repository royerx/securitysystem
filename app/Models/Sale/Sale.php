<?php

namespace App\Models\Sale;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use App\Models\Sale\Client;
use App\Models\Security\User;
use App\Models\Sale\SaleDetails;
use App\Models\Product\Product;//prueba
use App\Models\Entry\BuyDetail;//prueba
use Illuminate\Support\Arr;
use App\Models\System\SunatInvoice;

class Sale extends Model
{
    use HasFactory;
    /**
     * Atributos que permiten asignacion masiva
     *
     * @var array
     */
    protected $fillable = [
        'date',
        'type',
        'serie',
        'correlative',
        'detail',
        'client_id',
        'user_id'
    ];

    #RELACIONES
    /**
     * Relacion de Venta con Client
     *
     * @return collection \App\Models\Sale\Client
     */
    public function client()
    {
        return $this->belongsTo(Client::class);
    }
    /**
     * Relacion de Venta con Usuario
     *
     * @return collection \App\Models\Security\User
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }
    /**
     * Relacion de Venta con Salida
     *
     * @return collection \App\Models\Security\Outside
     */
    public function outside()
    {
        return $this->belongsTo(Outside::class);
    }
    /**
     * Relacion de Venta con Detalle de Venta
     *
     * @return collection \App\Models\Sale\SaleDetail
     */
    public function saleDetails()
    {
        return $this->hasMany(SaleDetail::class);
    }

    /**
     * Relacion de Venta con Facturacion Electronica
     *
     * @return collection \App\Models\System\SunatInvoice
     */
    public function sunatInvoices()
    {
        return $this->hasMany(SunatInvoice::class);
    }
    #ALMACENAMIENTO
    /**
     * Guardar Venta y sus detalles
     *
     * @param  \Illuminate\Http\Request $request
     * @return \App\Models\Security\User
     */
    public function store($request)
     {
         
        try {
            DB::beginTransaction();
                $sale = new Sale();
                $sale->date = $request->date;
                $sale->type = $request->type;
                $sale->detail = $request->detail;
                $sale->client_id = $request->client['id'];
                $sale->user_id = Auth::user()->id;
                if($request->type=="Ticket"){
                    $sale->serie=$request->serie;
                    $sale->correlative=$request->correlative;
                }else{
                    $order = $this->getDocumentNumber('Ticket');
                    $sale->serie = $order['serie'];
                    $sale->correlative = $order['correlative'];
                }
                $sale->save();
                $details=[];
                foreach ($request->sale_details as $d){
                    $detail= new SaleDetail();
                    
                    $detail->presentation_product_id = $d['presentation_product']['id'];
                    $detail->price = $d['price'];
                    $detail->quantity = $d['quantity'];
                    $detail->typeIgv=$d['typeIgv'];

                    $quantitySale = $d['presentation_product']['quantity']*$detail->quantity;

                    $buyDetails = $this->getBuyDetails($d['presentation_product']['product_id'],$quantitySale);
                    if($buyDetails['add']=="No"){
                        DB::rollback();
                        return ["add"=>"No","message"=>$d['presentation_product']['product']["name"]." faltan ".$buyDetails['missing']." ".$d['presentation_product']['product']["measure"]["name"]." para la venta"];
                    }
                    $sale->saleDetails()->save($detail)->buyDetails()->attach($buyDetails['details']);  
                }
               
                if($sale->type=="Boleta" || $sale->type=="Factura"){
                    $sunatInvoice = new SunatInvoice();
                    $sunatInvoice = $sunatInvoice->store($sale);

                    $sunatInvoice->updateTable();
                    $generate = $sunatInvoice->generateInvoice();
                    if($generate['validacion']=="EXITO" && strlen($generate['mensaje'])>0){
                        DB::rollback();
                        return ["add"=>"No","message"=>$generate['mensaje']];
                    }
                    $sunatInvoice->sendInvoice();
                    
                    $sunatInvoice->hash=$sunatInvoice->getHash();
                    $sunatInvoice->save();
                }

                foreach ($request->sale_details as $d){
                    $product=Product::findOrFail($d['presentation_product']['product']['id']);
                    $product->stock=$product->getStock();
                    $product->save();
                }
                
            DB::commit();
            return $sale;
        } catch (\Exception $e) {
            DB::rollback();
            return null;
        }
     }

     /**
     * Modificar Compra y sus detalles
     *
     * @param  \Illuminate\Http\Request $request
     * @return \App\Models\Security\User
     */
    public function my_update($request)
     {
        try {
            DB::beginTransaction();

                $request->merge(['client_id'=>$request->client['id']]);
                $this->update($request->all());
                
                $detailsDB = $this->saleDetails()->with('presentationProduct')->get();

                $details=$detailsDB->except(Arr::pluck($request->sale_details, 'id'));

                $this->syncDetails($request->sale_details);
                /*foreach ($request->buy_details as $d){
                    $detail= new BuyDetail();

                    $detail->presentation_product_id = $d['presentation_product']['id'];
                    $detail->price = $d['price'];
                    $detail->quantity = $d['quantity'];
                    $detail->quantitySale=0;
                    $detail->dueDate=$d['dueDate'];
                    
                    $details[]=$detail;
                }
                $buy->save();
                $buy->buyDetails()->saveMany($details);*/
                if($request->type=="Boleta"||$request->type=="Factura"){
                   
                    $sunatInvoice = new SunatInvoice();
                    $sunatInvoice = $sunatInvoice->store($this);
                   
                    $sunatInvoice->updateTable();
                    $generate = $sunatInvoice->generateInvoice();
                    
                    if($generate['validacion']=="EXITO" && strlen($generate['mensaje'])>0){
                        DB::rollback();
                        return null;
                    }
                    $sunatInvoice->sendInvoice($this);
                    $sunatInvoice->hash=$sunatInvoice->getHash();
                    $sunatInvoice->save();
                }
                
                foreach ($request->sale_details as $d){
                    $product=Product::findOrFail($d['presentation_product']['product']['id']);
                    $product->stock=$product->getStock();
                    $product->save();
                }
                foreach ($details as $d){
                    $product=Product::findOrFail($d->presentationProduct['product_id']);
                    $product->stock=$product->getStock();
                    $product->save();
                }
            DB::commit();
            return $this;
        } catch (\Exception $e) {
            DB::rollback();
            return null;
        }
         
     }

    /**
     * Obtener detalles de compra para asignar el detalle de la venta
     * 
     * @param  integer $idProduct identificador del producto a ser vendido
     * @param  double $sale cantidad a vender
     *  @return \App\Models\Entry\BuyDetail $buyDetails detalles de compra a ser agregados al detalle de venta
     */
    public function getBuyDetails($idProduct,$sale)
     {
        try{
        $stock = Product::where('id',$idProduct)->get()[0]->stock;
        if($stock<$sale){
            return ["add"=>"No","missing"=>$sale-$stock];
        }

        $buyDetails=BuyDetail::with('saleDetails.presentationProduct','presentationProduct')->whereHas('presentationProduct', function ($query) use ($idProduct) {
            $query->where('product_id', $idProduct);
        })->where('quantityAviable','>',0)->orderBy('dueDate')->get();
        
        $buyDetailsSale=[];

            foreach($buyDetails as $buyDetail){

                //$quantitySale = $buyDetail->saleDetails->sum('pivot.quantitySale');
                //$quantityBuy = $buyDetail->presentationProduct->quantity*$buyDetail->quantity;
                //$quantityAvailable=$quantityBuy-$quantitySale;
                $quantityAvailable=$buyDetail->quantityAviable;
                if($quantityAvailable>0){
                    if($quantityAvailable > $sale){
                        if($sale>0){
                            $buyDetailsSale[$buyDetail->id]=['quantitySale'=>$sale];
                            $buyDetail->quantityAviable-=$sale;
                            $buyDetail->quantitySale+=$sale;
                            break 1;
                        }
                    }else{
                        $buyDetailsSale[$buyDetail->id ]=['quantitySale'=>$quantityAvailable];
                        $buyDetail->quantityAviable-=$quantityAvailable;
                        $buyDetail->quantitySale+=$quantityAvailable;
                        $sale-=$quantityAvailable;
                    }
                }
            }

        return ["add"=>"Si","details"=>$buyDetailsSale];
        } catch (\Exception $e) {
            DB::rollback();
            return $e;
        } 
     }
             

     /**
     * Eliminar venta y sus detalles
     *
     * @param  \App\Models\Sale\Sale $sale
     * @return \App\Models\Sale\Sale
     */
    public function remove($sale)
     {
        try {
            DB::beginTransaction();
                $sale->load('saleDetails.presentationProduct.product');
                $details = $sale->saleDetails;
                foreach ($sale->saleDetails()->get() as $detail){
                    $detail->buyDetails()->detach();
                }
                $sale->saleDetails()->delete();
                $sale->delete();
                foreach ($details as $d){
                    $product=Product::findOrFail($d->presentationProduct->product->id);
                    $product->stock=$product->getStock();
                    $product->save();
                }
            DB::commit();
            return $sale;
        } catch (\Exception $e) {
            DB::rollback();
            return false;
        } catch (\Throwable $e) {
            DB::rollback();
            return $e;
        }
         
     }
    #VALIDACIÓN
    
    #RECUPERACIÓN DE INFORMACIÓN
    /* Query Scopes*/
    public function scopeClient($query,$find)
    {
        if($find)
            return $query->WhereHas('client',function($query) use($find){$query->Where('firstName','LIKE',"%$find%")->orWhere('lastName','LIKE',"%$find%")->orWhere('documentNumber','LIKE',"%$find%");});
    }

    public function scopeSerie($query,$find)
    {
        if($find)
            return $query->orwhere('serie','LIKE',"%$find%");
    }

    public function scopeCorrelative($query,$find)
    {
        if($find)
            return $query->orWhere('correlative','LIKE',"%$find%");
    }
    
    #OTRAS OPERACIONES
    /**
     * Sincronizar detalles de una venta
     * 
     * @param array $saleDetails
     */
    public function syncDetails(array $saleDetails)
    {
        $children = $this->saleDetails;
        $saleDetails = collect($saleDetails);
        $deleted_ids = $children->filter(
            function ($child) use ($saleDetails) {
                return empty(
                    $saleDetails->where('id', $child->id)->first()
                );
            }
        )->map(function ($child) {
                $id = $child->id;
                $child->buyDetails()->detach();
                $child->delete();
                return $id;
            }
        );
        $attachments = $saleDetails->filter(
            function ($saleDetail) {
                return empty($saleDetail['id']);
            }
        )->map(function ($saleDetail) use ($deleted_ids) {
                $saleDetail['id'] = $deleted_ids->pop();
                $saleDetail['sale_id']=$this->id;
                $saleDetail['presentation_product_id'] = $saleDetail['presentation_product']['id'];
                $quantitySale = $saleDetail['presentation_product']['quantity']*$saleDetail['quantity'];
                $buyDetails = $this->getBuyDetails($saleDetail['presentation_product']['product_id'],$quantitySale);

                if($buyDetails['add']=="No"){
                    return ["add"=>"No","message"=>$saleDetail['presentation_product']['product']["name"]." faltan ".$saleDetails['missing']."unidad(es) para la venta"];
                }

                $saleDet= new SaleDetail($saleDetail);
                $saleDet->save();
                $saleDet->buyDetails()->attach($buyDetails['details']);   
                
                return $saleDet;
        });
        $this->saleDetails()->saveMany($attachments);
    }

    public static function getDocumentNumber($type){
        if($type=="Ticket"){
                $serie = Sale::max('serie');
                $correlative = Sale::where('serie','=',$serie)->max('correlative');    
            }else{
                $serie = SunatInvoice::Where('type','=',$type)->max('serie');
                $correlative = SunatInvoice::Where('type','=',$type)->where('serie','=',$serie)->max('correlative');
            }
            if($correlative>=99999999){
                $serie = str_pad($serie+1, 3, "0", STR_PAD_LEFT);
                $correlative = str_pad(1, 8, "0", STR_PAD_LEFT);
            }else{
                if($serie==0){
                    switch($type){
                        case "Boleta":
                            $serie = Session::get('invoice')->ticketSerie;
                            $correlative = Session::get('invoice')->ticketCorrelative;
                            break;
                        case "Factura":
                            $serie = Session::get('invoice')->ticketSerie;
                            $correlative = Session::get('invoice')->ticketCorrelative;
                            break;
                        default:
                            $serie = str_pad($serie+1, 3, "0", STR_PAD_LEFT);
                            $correlative = str_pad($correlative+1, 8, "0", STR_PAD_LEFT);
                            break;
                    }
                    
                }else{
                    $serie = str_pad($serie, 3, "0", STR_PAD_LEFT);
                    $correlative = str_pad($correlative+1, 8, "0", STR_PAD_LEFT);
                }
            }
            return [
                'type' => $type,
                'serie' => $serie,
                'correlative' => $correlative
            ];
    }
}
