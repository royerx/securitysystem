<?php

namespace App\Models\Sale;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Sale\Sale;
use App\Models\Entry\BuyDetail;
use App\Models\Product\PresentationProduct;
use App\Models\System\SunatInvoiceDetail;

class SaleDetail extends Model
{
    use HasFactory;
    /**
     * Atributos que permiten asignacion masiva
     *
     * @var array
     */
    protected $fillable = [
        'price',
        'quantity',
        'presentation_product_id',
        'sale_id',
        'typeIgv',
    ];

     #RELACIONES

    /**
     * Relacion de Detalle de Venta con Venta
     *
     * @return collection \App\Models\Sale\Sale
     */
    public function sale()
    {
        return $this->belongsTo(Sale::class);
    }

    /**
     * Relacion de Detalle de Venta con Detalle de Compra
     *
     * @return collection \App\Models\Buy\BuyDetail
     */
    public function buyDetails()
    {
        return $this->belongsToMany(BuyDetail::class)->withPivot('quantitySale')->withTimestamps();
    }
    /**
     * Relacion de Detalle de Venta con Presentacion de Producto
     *
     * @return collection \App\Models\Sale\SaleDetail
     */
    public function presentationProduct()
    {
        return $this->belongsTo(PresentationProduct::class,'presentation_product_id');
    }
    /**
     * Relacion de Detalle de Venta con Detalle se facturacion
     *
     * @return collection \App\Models\System\SunatInvoiceDetail
     */
    public function sunatInvoiceDetails()
    {
        return $this->hasMany(SunatInvoiceDetail::class);
    }
    #ALMACENAMIENTO
    
    #VALIDACIÓN
    
    #RECUPERACIÓN DE INFORMACIÓN
    
    #OTRAS OPERACIONES
}
