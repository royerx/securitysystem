<?php

namespace App\Models\Management;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Person extends Model
{
    use HasFactory;

    const DEFAULT_PICTURE = '/img/people/picture.png';
    const DEFAULT_PERSON = 'VARIOS';
    
    protected $table = 'people';
    
    /**
     * Atributos que permiten asignacion masiva
     *
     * @var array
     */
    protected $fillable = [
        'personType',
        'lastName',
        'firstName',
        'documentType',
        'documentNumber',
        'address',
        'phone',
        'email',
        'picture'
    ];

    /**
     * Atributos calculados o generados
     *
     * @var array
     */
    protected $appends=['fullName'];
    
    #RELACIONES

    /**
     * Guardar persona
     *
     * @param  \Illuminate\Http\Request $request
     * @return \App\Models\Security\User
     */
    public function store($request)
     {
        $picture='';
        if($request->photo){
            $picture = $this->savePicture($request->photo);
        }else{
            $picture=self::DEFAULT_PICTURE;
        }
        return self::create(array_merge($request->all(),[
            'picture' => $picture,
         ]));
     }
    #ALMACENAMIENTO
    
    #VALIDACIÓN
    
    #RECUPERACIÓN DE INFORMACIÓN
    /**
     * Obtener el nombreCompleto
     *
     * @return string path
     */
    public function getFullNameAttribute()
    {
        if($this->lastName){
            return $this->firstName.' '.$this->lastName;
        }else{
            return $this->firstName;
        }
        
    }
    
    #OTRAS OPERACIONES

}
