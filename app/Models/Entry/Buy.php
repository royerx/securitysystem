<?php

namespace App\Models\Entry;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use App\Models\Entry\Supplier;
use App\Models\Security\User;
use App\Models\Product\Product;
use App\Models\Entry\BuyDetail;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Arr;

class Buy extends Model
{
    use HasFactory;

    /**
     * Atributos que permiten asignacion masiva
     *
     * @var array
     */
    protected $fillable = [
        'date',
        'type',
        'serie',
        'correlative',
        'detail',
        'supplier_id',
        'user_id'
    ];

    #RELACIONES
    /**
     * Relacion de Compra con Proveedor
     *
     * @return collection \App\Models\Entry\Suppler
     */
    public function supplier()
    {
        return $this->belongsTo(Supplier::class,);
    }
    /**
     * Relacion de Compra con Usuario
     *
     * @return collection \App\Models\Security\User
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }
    /**
     * Relacion de Compra con Detalle de Compra
     *
     * @return collection \App\Models\Buy\BuyDetail
     */
    public function buyDetails()
    {
        return $this->hasMany(BuyDetail::class);
    }
    
    #ALMACENAMIENTO
    /**
     * Guardar Compra y sus detalles
     *
     * @param  \Illuminate\Http\Request $request
     * @return \App\Models\Security\User
     */
    public function store($request)
     {
        try {
            DB::beginTransaction();
                $buy = new Buy();
                $buy->date = $request->date;
                $buy->type = $request->type;
                $buy->serie = $request->serie;
                $buy->correlative = $request->correlative;
                $buy->detail = $request->detail;
                $buy->supplier_id = $request->supplier['id'];
                $buy->user_id = Auth::user()->id;
                $details=[];
                foreach ($request->buy_details as $d){
                    $detail= new BuyDetail();

                    $detail->presentation_product_id = $d['presentation_product']['id'];
                    $detail->price = $d['price'];
                    $detail->quantity = $d['quantity'];
                    $detail->quantitySale=0;
                    $detail->quantityAviable=$d['quantity']*$d['presentation_product']['quantity'];
                    $detail->dueDate=$d['dueDate'];
                    
                    $details[]=$detail;
                }
                $buy->save();
                $buy->buyDetails()->saveMany($details);
                foreach ($request->buy_details as $d){
                    $product=Product::findOrFail($d['presentation_product']['product']['id']);
                    $product->stock=$product->getStock();
                    $product->save();
                }
            DB::commit();
            return $buy;
        } catch (\Exception $e) {
            DB::rollback();
            return null;
        } 
         
     }

     /**
     * Modificar Compra y sus detalles
     *
     * @param  \Illuminate\Http\Request $request
     * @return \App\Models\Security\User
     */
    public function my_update($request)
     {
        try {
            DB::beginTransaction();

                $request->merge(['supplier_id'=>$request->supplier['id']]);
                $this->update($request->all());
                $detailsDB = $this->buyDetails()->with('presentationProduct')->get();
                
                $details=$detailsDB->except(Arr::pluck($request->buy_details, 'id'));

                $this->syncDetails($request->buy_details);
                /*foreach ($request->buy_details as $d){
                    $detail= new BuyDetail();

                    $detail->presentation_product_id = $d['presentation_product']['id'];
                    $detail->price = $d['price'];
                    $detail->quantity = $d['quantity'];
                    $detail->quantitySale=0;
                    $detail->dueDate=$d['dueDate'];
                    
                    $details[]=$detail;
                }
                $buy->save();
                $buy->buyDetails()->saveMany($details);*/
                foreach ($request->buy_details as $d){
                    $product=Product::findOrFail($d['presentation_product']['product']['id']);
                    $product->stock=$product->getStock();
                    $product->save();
                }
                foreach ($details as $d){
                    $product=Product::findOrFail($d->presentationProduct['product_id']);
                    $product->stock=$product->getStock();
                    $product->save();
                }
            DB::commit();
            return $this;
        } catch (\Exception $e) {
            DB::rollback();
            return null;
        }
         
     }

     /**
     * Eliminar compra y sus detalles
     *
     * @param  \App\Models\Entry\Buy $buy
     * @return \App\Models\Security\User
     */
    public function remove($buy)
     {
         
        try {
            DB::beginTransaction();
                $details=$buy->buyDetails()->get();
                $buy->buyDetails()->delete();
                $buy->delete();
                foreach ($details as $d){
                    $presentation=$d->presentationProduct()->with('product')->get();
                    $product = $presentation[0]->product;
                    $product->stock=$product->getStock();
                    $product->save();
                }
            DB::commit();
            return $buy;
        } catch (\Exception $e) {
            DB::rollback();
            return false;
        }
     }
     
    #VALIDACIÓN
    
    #RECUPERACIÓN DE INFORMACIÓN

    /* Query Scopes*/
    public function scopeSupplier($query,$find)
    {
        if($find)
            return $query->WhereHas('supplier',function($query) use($find){$query->Where('firstName','LIKE',"%$find%")->orWhere('lastName','LIKE',"%$find%")->orWhere('documentNumber','LIKE',"%$find%");});
    }

    public function scopeSerie($query,$find)
    {
        if($find)
            return $query->orwhere('serie','LIKE',"%$find%");
    }

    public function scopeCorrelative($query,$find)
    {
        if($find)
            return $query->orWhere('correlative','LIKE',"%$find%");
    }
    
    #OTRAS OPERACIONES
    /**
     * Sincronizar detalles de una compra
     * 
     * @param array $buyDetails
     */
    public function syncDetails(array $buyDetails)
    {
        $children = $this->buyDetails;
        $buyDetails = collect($buyDetails);
        $deleted_ids = $children->filter(
            function ($child) use ($buyDetails) {
                return empty(
                    $buyDetails->where('id', $child->id)->first()
                );
            }
        )->map(function ($child) {
                $id = $child->id;
                $child->delete();
                return $id;
            }
        );
        $attachments = $buyDetails->filter(
            function ($buyDetail) {
                return empty($buyDetail['id']);
            }
        )->map(function ($buyDetail) use ($deleted_ids) {
                $buyDetail['id'] = $deleted_ids->pop();
                $buyDetail['presentation_product_id'] = $buyDetail['presentation_product']['id'];
                $buyDetail['quantityAviable']=$buyDetail['quantity']*$buyDetail['presentation_product']['quantity'];
                $buyDetail['quantitySale']=0;
                return new BuyDetail($buyDetail);
        });
        $this->buyDetails()->saveMany($attachments);
    }
}
