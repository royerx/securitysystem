<?php

namespace App\Models\Entry;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Entry\Buy;
Use App\Models\Sale\SaleDetail;
Use App\Models\Product\PresentationProduct;

class BuyDetail extends Model
{
    use HasFactory;
    /**
     * Atributos que permiten asignacion masiva
     *
     * @var array
     */
    protected $fillable = [
        'price',
        'quantity',
        'dueDate',
        'quantityAviable',
        'quantitySale',
        'presentation_product_id'
    ];
    /**
     * Atributos calculados o generados
     *
     * @var array
     */
    protected $appends=['hasSale'];

     #RELACIONES

    /**
     * Relacion de Detalle de Compra con Compra
     *
     * @return collection \App\Models\Entry\Buy
     */
    public function buy()
    {
        return $this->belongsTo(Buy::class);
    }

    /**
     * Relacion de Detalle de Compra con Detalle de Venta
     *
     * @return collection \App\Models\Sale\SaleDetail
     */
    public function saleDetails()
    {
        return $this->belongsToMany(SaleDetail::class)->withPivot('quantitySale')->withTimestamps();
    }
    /**
     * Relacion de Detalle de Compra con Presentacion de Producto
     *
     * @return collection \App\Models\Sale\SaleDetail
     */
    public function presentationProduct()
    {
        return $this->belongsTo(PresentationProduct::class,'presentation_product_id');
    }
    #ALMACENAMIENTO
    
    #VALIDACIÓN
    
    #RECUPERACIÓN DE INFORMACIÓN
    /**
     * Verificar si tiene detalle de ventas asignadas
     *
     * @return string path
     */
    public function getHasSaleAttribute()
    {
        return $this->saleDetails()->get()->count();
    }
    #OTRAS OPERACIONES
}
