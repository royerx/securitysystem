<?php

namespace App\Models\Entry;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Management\Person;
use App\Models\Entry\Buy;
use Illuminate\Support\Facades\DB;

class Supplier extends Person
{
    use HasFactory;
    
    #RELACIONES

    /**
     * Relacion de Proveedor con Compra
     *
     * @return collection \App\Models\Entry\Buy
     */
    public function buys()
    {
        return $this->hasMany(Buy::class);
    }

    #ALMACENAMIENTO
    
    #VALIDACIÓN
    
    #RECUPERACIÓN DE INFORMACIÓN

    /* Query Scopes*/
    public function scopeFirstName($query,$find)
    {
        if($find)
            return $query->orWhere('firstName','LIKE',"%$find%");
    }

    public function scopeLastName($query,$find)
    {
        if($find)
            return $query->orWhere('lastName','LIKE',"%$find%");
    }
    public function scopeDocumentNumber($query,$find)
    {
        if($find)
            return $query->orWhere('documentNumber','LIKE',"%$find%");
    }
    
    #OTRAS OPERACIONES
    
}
