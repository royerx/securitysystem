<?php

namespace App\Models\Account;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Account\ExpenseDate;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class Expense extends Model
{
    use HasFactory;
    /**
     * Atributos que permiten asignacion masiva
     *
     * @var array
     */
    protected $fillable = [
        'type',
        'name',
        'amount',
    ];
    #RELACIONES
    /**
     * Relacion de Fechas de los Gastos
     *
     * @return collection \App\Models\Account\ExpenseDate
     */
    public function expenseDates()
    {
        return $this->hasMany(ExpenseDate::class);
    }

    #ALMACENAMIENTO
    /**
     * Guardar Gastos y sus fechas
     *
     * @param  \Illuminate\Http\Request $request
     * @return \App\Models\Account\Expense
     */
    public function store($request)
     {
        try {
            DB::beginTransaction();
                $expense = new Expense();
                $expense->type = $request->type;
                $expense->name = $request->name;
                $expense->amount = $request->amount;
                $dates=[];

                if($request->type=="Fijo"){
                   
                    $beginDate=Carbon::parse($request->beginDate);
                    $endDate=Carbon::parse($request->endDate);
                    $curDate = $beginDate->startOfMonth();
                    
                    while($curDate<=$endDate){
                        $date= new ExpenseDate();

                        $date->date = $curDate->format('Y-m-d');

                        $dates[]=$date;
                        
                        $curDate->addMonth()->startOfMonth();
                    }

                    $expense->save();
                    $expense->expenseDates()->saveMany($dates);
                }else{

                    $date= new ExpenseDate();

                    $date->date = $request->beginDate;

                    
                    $expense->save();
                    $expense->expenseDates()->save($date);
                }
            DB::commit();
            return $expense;
        } catch (\Exception $e) {
            DB::rollback();
            return null;
        } 
         
     }
     /**
     * Editar Gasto y sus fechas
     *
     * @param  \Illuminate\Http\Request $request
     * @return \App\Models\Account\Expense
     */
    public function my_update($request,$expense)
     {
        try {
            DB::beginTransaction();
                
                $expense->update($request->all());

                $dates=[];

                if($request->type=="Fijo"){

                    $beginDate=Carbon::parse($request->beginDate);
                    $endDate=Carbon::parse($request->endDate);

                    $curDate = $beginDate->startOfMonth();
                    
                    while($curDate<=$endDate){
                        $date= new ExpenseDate();

                        $date->date = $curDate->format('Y-m-d');

                        $dates[]=$date;
                        
                        $curDate->addMonth()->startOfMonth();
                    }

                    $expense->expenseDates()->delete();
                    $expense->expenseDates()->saveMany($dates);
                    
                }else{
                    
                    $date= new ExpenseDate();

                    $date->date = $request->beginDate;

                    $expense->expenseDates()->delete();
                    $expense->expenseDates()->save($date);
                }
                
            DB::commit();
            return $expense;
        } catch (\Exception $e) {
            DB::rollback();
            return null;
        } 
         
     }
     /**
     * Eliminar compra y sus detalles
     *
     * @param  \App\Models\Entry\Buy $buy
     * @return \App\Models\Security\User
     */
    public function remove($expense)
     {
        try {
            DB::beginTransaction();
                $expense->expenseDates()->delete();
                $expense->delete();
            DB::commit();
            return $expense;
        } catch (\Exception $e) {
            DB::rollback();
            return false;
        } 
         
     }
    #VALIDACIÓN
    
    #RECUPERACIÓN DE INFORMACIÓN
    
    #OTRAS OPERACIONES
}
