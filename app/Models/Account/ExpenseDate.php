<?php

namespace App\Models\Account;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Account\Expense;

class ExpenseDate extends Model
{
    use HasFactory;
    /**
     * Atributos que permiten asignacion masiva
     *
     * @var array
     */
    protected $fillable = [
        'date',
    ];
    #RELACIONES
    /**
     * Relacion con Gasto
     *
     * @return collection \App\Models\Account\Expense
     */
    public function expense()
    {
        return $this->belongsTo(Expense::class,);
    }
    #ALMACENAMIENTO
    
    #VALIDACIÓN
    
    #RECUPERACIÓN DE INFORMACIÓN
    
    #OTRAS OPERACIONES
}
