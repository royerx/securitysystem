<?php

namespace App\Policies\Sale;

use App\Models\Sale\Sale;
use App\Models\Security\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class SalePolicy
{
    use HandlesAuthorization;

    /**
     * Permiso para el metodo index para el modelo Sale
     *
     * @param  \App\Models\Security\User  $user
     * @return boolean
     */
    public function viewAny(User $user)
    {
        return $user->hasPermission('sales.list');
    }

    /**
     * Permiso para el metodo show para el modelo Sale
     *
     * @param  \App\Models\Security\User  $user
     * @param  \App\Models\Sale\Sale  $sale
     * @return boolean
     */
    public function view(User $user, Sale $sale)
    {
        return $user->hasPermission('sales.show');
    }

    /**
     * Permiso para el metodo create para el modelo Sale
     *
     * @param  \App\Models\Security\User  $user
     * @return boolean
     */
    public function create(User $user)
    {
        return $user->hasPermission('sales.create');
    }

    /**
     * Permiso para el metodo update para el modelo Sale
     *
     * @param  \App\Models\Security\User  $user
     * @param  \App\Models\Sale\Sale  $sale
     * @return boolean
     */
    public function update(User $user, Sale $sale)
    {
        return $user->hasPermission('sales.update');
    }

    /**
     * Permiso para el metodo delete para el modelo Sale
     *
     * @param  \App\Models\Security\User  $user
     * @param  \App\Models\Sale\Sale  $sale
     * @return boolean
     */
    public function delete(User $user, Sale $sale)
    {
        return $user->hasPermission('sales.delete');
    }

     /**
     * Permiso para el metodo createDetail para el modelo Sale
     *
     * @param  \App\Models\Security\User  $user
     * @param  \App\Models\Sale\Sale  $sale
     * @return boolean
     */
    public function createDetail(User $user, Sale $sale)
    {
        return $user->hasPermission('sales.createDetail');
    }

    /**
     * Permiso para el metodo deleteDetail para el modelo Sale
     *
     * @param  \App\Models\Security\User  $user
     * @param  \App\Models\Sale\Sale  $sale
     * @return boolean
     */
    public function deleteDetail(User $user, Sale $sale)
    {
        return $user->hasPermission('sales.deleteDetail');
    }

    /**
     * Permiso para generar ticket
     *
     * @param  \App\Models\Security\User  $user
     * @param  \App\Models\Sale\Sale  $sale
     * @return boolean
     */
    public function voucher(User $user, Sale $sale)
    {
        return $user->hasPermission('sales.voucher');
    }

    /**
     * Permiso para generar comprobante con el facturador de sunat
     *
     * @param  \App\Models\Security\User  $user
     * @param  \App\Models\Sale\Sale  $sale
     * @return boolean
     */
    public function generateInvoice(User $user, Sale $sale)
    {
        return $user->hasPermission('sales.generate');
    }
    /**
     * Permiso para enviar comprobante con el facturador de sunat
     *
     * @param  \App\Models\Security\User  $user
     * @param  \App\Models\Sale\Sale  $sale
     * @return boolean
     */
    public function sendInvoice(User $user, Sale $sale)
    {
        return $user->hasPermission('sales.send');
    }
}
