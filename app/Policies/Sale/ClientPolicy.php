<?php

namespace App\Policies\Sale;

use App\Models\Sale\Client;
use App\Models\Security\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class ClientPolicy
{
    use HandlesAuthorization;

    /**
     * Permiso para el metodo index para el modelo Client
     *
     * @param  \App\Models\Security\User  $user
     * @return boolean
     */
    public function viewAny(User $user)
    {
        return $user->hasPermission('clients.list');
    }

    /**
     * Permiso para el metodo show para el modelo Client
     *
     * @param  \App\Models\Security\User  $user
     * @param  \App\Models\Sale\Client  $client
     * @return boolean
     */
    public function view(User $user, Client $client)
    {
        return $user->hasPermission('clients.show');
    }

    /**
     * Permiso para el metodo create para el modelo Client
     *
     * @param  \App\Models\Security\User  $user
     * @return boolean
     */
    public function create(User $user)
    {
        return $user->hasPermission('clients.create');
    }

    /**
     * Permiso para el metodo update para el modelo Client
     *
     * @param  \App\Models\Security\User  $user
     * @param  \App\Models\Sale\Client  $client
     * @return boolean
     */
    public function update(User $user, Client $client)
    {
        return $user->hasPermission('clients.update');
    }

    /**
     * Permiso para el metodo delete para el modelo Client
     *
     * @param  \App\Models\Security\User  $user
     * @param  \App\Models\Sale\Client  $client
     * @return boolean
     */
    public function delete(User $user, Client $client)
    {
        return $user->hasPermission('clients.delete');
    }

    /**
     * Permiso para el metodo list para el modelo Client
     *
     * @param  \App\Models\Security\User  $user
     * @return boolean
     */
    public function list(User $user)
    {
        return $user->hasPermission('clients.list');
    }
}
