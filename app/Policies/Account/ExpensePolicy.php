<?php

namespace App\Policies\Account;

use App\Models\Account\Expense;
use App\Models\Security\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class ExpensePolicy
{
    use HandlesAuthorization;

/**
     * Permiso para el metodo index para el modelo Expense
     *
     * @param  \App\Models\Security\User  $user
     * @return boolean
     */
    public function viewAny(User $user)
    {
        return $user->hasPermission('expenses.list');
    }

    /**
     * Permiso para el metodo show para el modelo Expense
     *
     * @param  \App\Models\Security\User  $user
     * @param  \App\Models\Account\Expense  $expense
     * @return boolean
     */
    public function view(User $user, Expense $expense)
    {
        return $user->hasPermission('expenses.show');
    }

    /**
     * Permiso para el metodo create para el modelo Expense
     *
     * @param  \App\Models\Security\User  $user
     * @return boolean
     */
    public function create(User $user)
    {
        return $user->hasPermission('expenses.create');
    }

    /**
     * Permiso para el metodo update para el modelo Expense
     *
     * @param  \App\Models\Security\User  $user
     * @param  \App\Models\Account\Expense  $expense
     * @return boolean
     */
    public function update(User $user, Expense $expense)
    {
        return $user->hasPermission('expenses.update');
    }

    /**
     * Permiso para el metodo delete para el modelo Expense
     *
     * @param  \App\Models\Security\User  $user
     * @param  \App\Models\Account\Expense  $expense
     * @return boolean
     */
    public function delete(User $user, Expense $expense)
    {
        return $user->hasPermission('expenses.delete');
    }
}
