<?php

namespace App\Policies\Entry;

use App\Models\Entry\Buy;
use App\Models\Security\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class BuyPolicy
{
    use HandlesAuthorization;

    /**
     * Permiso para el metodo index para el modelo Buy
     *
     * @param  \App\Models\Security\User  $user
     * @return boolean
     */
    public function viewAny(User $user)
    {
        return $user->hasPermission('buys.list');
    }

    /**
     * Permiso para el metodo show para el modelo Buy
     *
     * @param  \App\Models\Security\User  $user
     * @param  \App\Models\Entry\Buy  $buy
     * @return boolean
     */
    public function view(User $user, Buy $buy)
    {
        return $user->hasPermission('buys.show');
    }

    /**
     * Permiso para el metodo create para el modelo Buy
     *
     * @param  \App\Models\Security\User  $user
     * @return boolean
     */
    public function create(User $user)
    {
        return $user->hasPermission('buys.create');
    }

    /**
     * Permiso para el metodo update para el modelo Buy
     *
     * @param  \App\Models\Security\User  $user
     * @param  \App\Models\Entry\Buy  $buy
     * @return boolean
     */
    public function update(User $user, Buy $buy)
    {
        return $user->hasPermission('buys.update');
    }

    /**
     * Permiso para el metodo delete para el modelo Buy
     *
     * @param  \App\Models\Security\User  $user
     * @param  \App\Models\Entry\Buy  $buy
     * @return boolean
     */
    public function delete(User $user, Buy $buy)
    {
        return $user->hasPermission('buys.delete');
    }
}
