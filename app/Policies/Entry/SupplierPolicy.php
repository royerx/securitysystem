<?php

namespace App\Policies\Entry;

use App\Models\Entry\Supplier;
use App\Models\Security\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class SupplierPolicy
{
    use HandlesAuthorization;

    /**
     * Permiso para el metodo index para el modelo Supplier
     *
     * @param  \App\Models\Security\User  $user
     * @return boolean
     */
    public function viewAny(User $user)
    {
        return $user->hasPermission('suppliers.list');
    }

    /**
     * Permiso para el metodo show para el modelo Supplier
     *
     * @param  \App\Models\Security\User  $user
     * @param  \App\Models\Entry\Supplier  $supplier
     * @return boolean
     */
    public function view(User $user, Supplier $supplier)
    {
        return $user->hasPermission('suppliers.show');
    }

    /**
     * Permiso para el metodo create para el modelo Supplier
     *
     * @param  \App\Models\Security\User  $user
     * @return boolean
     */
    public function create(User $user)
    {
        return $user->hasPermission('suppliers.create');
    }

    /**
     * Permiso para el metodo update para el modelo Supplier
     *
     * @param  \App\Models\Security\User  $user
     * @param  \App\Models\Entry\Supplier  $supplier
     * @return boolean
     */
    public function update(User $user, Supplier $supplier)
    {
        return $user->hasPermission('suppliers.update');
    }

    /**
     * Permiso para el metodo delete para el modelo Supplier
     *
     * @param  \App\Models\Security\User  $user
     * @param  \App\Models\Entry\Supplier  $supplier
     * @return boolean
     */
    public function delete(User $user, Supplier $supplier)
    {
        return $user->hasPermission('suppliers.delete');
    }

    /**
     * Permiso para el metodo list para el modelo Supplier
     *
     * @param  \App\Models\Security\User  $user
     * @return boolean
     */
    public function list(User $user)
    {
        return $user->hasPermission('suppliers.list');
    }
}
