<?php

namespace App\Policies\Product;

use App\Models\Product\Category;
use App\Models\Security\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class CategoryPolicy
{
    use HandlesAuthorization;

    /**
     * Permiso para el metodo index para el modelo Category
     *
     * @param  \App\Models\Security\User  $user
     * @return boolean
     */
    public function viewAny(User $user)
    {
        return $user->hasPermission('categories.list');
    }

    /**
     * Permiso para el metodo show para el modelo Category
     *
     * @param  \App\Models\Security\User  $user
     * @param  \App\Models\Product\Category  $category
     * @return boolean
     */
    public function view(User $user, Category $category)
    {
        return $user->hasPermission('categories.show');
    }

    /**
     * Permiso para el metodo create para el modelo Category
     *
     * @param  \App\Models\Security\User  $user
     * @return boolean
     */
    public function create(User $user)
    {
        return $user->hasPermission('categories.create');
    }

    /**
     * Permiso para el metodo update para el modelo Category
     *
     * @param  \App\Models\Security\User  $user
     * @param  \App\Models\Product\Category  $category
     * @return boolean
     */
    public function update(User $user, Category $category)
    {
        return $user->hasPermission('categories.update');
    }

    /**
     * Permiso para el metodo delete para el modelo Category
     *
     * @param  \App\Models\Security\User  $user
     * @param  \App\Models\Product\Category  $category
     * @return boolean
     */
    public function delete(User $user, Category $category)
    {
        return $user->hasPermission('categories.delete');
    }

    /**
     * Permiso para el metodo list para el modelo Category
     *
     * @param  \App\Models\Security\User  $user
     * @return boolean
     */
    public function list(User $user)
    {
        return $user->hasPermission('categories.list');
    }
}
