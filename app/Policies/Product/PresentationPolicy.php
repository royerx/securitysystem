<?php

namespace App\Policies\Product;

use App\Models\Product\Presentation;
use App\Models\Security\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class PresentationPolicy
{
    use HandlesAuthorization;

    /**
     * Permiso para el metodo index para el modelo Presentation
     *
     * @param  \App\Models\Security\User  $user
     * @return boolean
     */
    public function viewAny(User $user)
    {
        return $user->hasPermission('presentations.list');
    }

    /**
     * Permiso para el metodo show para el modelo Presentation
     *
     * @param  \App\Models\Security\User  $user
     * @param  \App\Models\Product\Presentation  $presentation
     * @return boolean
     */
    public function view(User $user, Presentation $presentation)
    {
        return $user->hasPermission('presentations.show');
    }

    /**
     * Permiso para el metodo create para el modelo Presentation
     *
     * @param  \App\Models\Security\User  $user
     * @return boolean
     */
    public function create(User $user)
    {
        return $user->hasPermission('presentations.create');
    }

    /**
     * Permiso para el metodo update para el modelo Presentation
     *
     * @param  \App\Models\Security\User  $user
     * @param  \App\Models\Product\Presentation  $presentation
     * @return boolean
     */
    public function update(User $user, Presentation $presentation)
    {
        return $user->hasPermission('presentations.update');
    }

    /**
     * Permiso para el metodo delete para el modelo Presentation
     *
     * @param  \App\Models\Security\User  $user
     * @param  \App\Models\Product\Presentation  $presentation
     * @return boolean
     */
    public function delete(User $user, Presentation $presentation)
    {
        return $user->hasPermission('presentations.delete');
    }

    /**
     * Permiso para el metodo list para el modelo Presentation
     *
     * @param  \App\Models\Security\User  $user
     * @return boolean
     */
    public function list(User $user)
    {
        return $user->hasPermission('presentations.list');
    }
}
