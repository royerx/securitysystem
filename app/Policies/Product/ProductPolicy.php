<?php

namespace App\Policies\Product;

use App\Models\Product\Product;
use App\Models\Security\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class ProductPolicy
{
    use HandlesAuthorization;

    /**
     * Permiso para el metodo index para el modelo Product
     *
     * @param  \App\Models\Security\User  $user
     * @return boolean
     */
    public function viewAny(User $user)
    {
        return $user->hasPermission('products.list');
    }

    /**
     * Permiso para el metodo show para el modelo Product
     *
     * @param  \App\Models\Security\User  $user
     * @param  \App\Models\Product\Product  $product
     * @return boolean
     */
    public function view(User $user, Product $product)
    {
        return $user->hasPermission('products.show');
    }

    /**
     * Permiso para el metodo create para el modelo Product
     *
     * @param  \App\Models\Security\User  $user
     * @return boolean
     */
    public function create(User $user)
    {
        return $user->hasPermission('products.create');
    }

    /**
     * Permiso para el metodo update para el modelo Product
     *
     * @param  \App\Models\Security\User  $user
     * @param  \App\Models\Product\Product  $product
     * @return boolean
     */
    public function update(User $user, Product $product)
    {
        return $user->hasPermission('products.update');
    }

    /**
     * Permiso para el metodo delete para el modelo Product
     *
     * @param  \App\Models\Security\User  $user
     * @param  \App\Models\Product\Product  $product
     * @return boolean
     */
    public function delete(User $user, Product $product)
    {
        return $user->hasPermission('products.delete');
    }

    /**
     * Permiso para el metodo updatePhoto para el modelo Product
     *
     * @param  \App\Models\Security\User  $user
     * @param  \App\Models\Product\Product  $product
     * @return boolean
     */
    public function updatePhoto(User $user, Product $product)
    {
        return $user->hasPermission('products.picture');
    }

    /**
     * Permiso para el metodo listPresentation para el modelo Product
     *
     * @param  \App\Models\Security\User  $user
     * @param  \App\Models\Product\Product  $product
     * @return boolean
     */
    public function listPresentation(User $user)
    {
        return $user->hasPermission('products.listPresentation');
    }
    /**
     * Permiso para el metodo createPresentation para el modelo Product
     *
     * @param  \App\Models\Security\User  $user
     * @param  \App\Models\Product\Product  $product
     * @return boolean
     */
    public function createPresentation(User $user, Product $product)
    {
        return $user->hasPermission('products.createPresentation');
    }

    /**
     * Permiso para el metodo updatePresentation para el modelo Product
     *
     * @param  \App\Models\Security\User  $user
     * @param  \App\Models\Product\Product  $product
     * @return boolean
     */
    public function updatePresentation(User $user, Product $product)
    {
        return $user->hasPermission('products.updatePresentation');
    }

    /**
     * Permiso para el metodo deletePresentation para el modelo Product
     *
     * @param  \App\Models\Security\User  $user
     * @param  \App\Models\Product\Product  $product
     * @return boolean
     */
    public function deletePresentation(User $user, Product $product)
    {
        return $user->hasPermission('products.deletePresentation');
    }

    /**
     * Permiso para el metodo list para el modelo Product
     *
     * @param  \App\Models\Security\User  $user
     * @return boolean
     */
    public function list(User $user)
    {
        return $user->hasPermission('products.list');
    }
}
