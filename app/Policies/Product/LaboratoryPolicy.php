<?php

namespace App\Policies\Product;

use App\Models\Product\Laboratory;
use App\Models\Security\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class LaboratoryPolicy
{
    use HandlesAuthorization;

    /**
     * Permiso para el metodo index para el modelo Laboratory
     *
     * @param  \App\Models\Security\User  $user
     * @return boolean
     */
    public function viewAny(User $user)
    {
        return $user->hasPermission('laboratories.list');
    }

    /**
     * Permiso para el metodo show para el modelo Laboratory
     *
     * @param  \App\Models\Security\User  $user
     * @param  \App\Models\Product\Laboratory  $laboratory
     * @return boolean
     */
    public function view(User $user, Laboratory $laboratory)
    {
        return $user->hasPermission('laboratories.show');
    }

    /**
     * Permiso para el metodo create para el modelo Laboratory
     *
     * @param  \App\Models\Security\User  $user
     * @return boolean
     */
    public function create(User $user)
    {
        return $user->hasPermission('laboratories.create');
    }

    /**
     * Permiso para el metodo update para el modelo Laboratory
     *
     * @param  \App\Models\Security\User  $user
     * @param  \App\Models\Product\Laboratory  $laboratory
     * @return boolean
     */
    public function update(User $user, Laboratory $laboratory)
    {
        return $user->hasPermission('laboratories.update');
    }

    /**
     * Permiso para el metodo delete para el modelo Laboratory
     *
     * @param  \App\Models\Security\User  $user
     * @param  \App\Models\Product\Laboratory  $laboratory
     * @return boolean
     */
    public function delete(User $user, Laboratory $laboratory)
    {
        return $user->hasPermission('laboratories.delete');
    }

    /**
     * Permiso para el metodo list para el modelo Laboratory
     *
     * @param  \App\Models\Security\User  $user
     * @return boolean
     */
    public function list(User $user)
    {
        return $user->hasPermission('laboratories.list');
    }
}
