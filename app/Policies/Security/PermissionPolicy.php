<?php

namespace App\Policies\Security;

use App\Models\Security\Permission;
use App\Models\Security\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class PermissionPolicy
{
    use HandlesAuthorization;

    /**
     * Permiso para el metodo index para el modelo Permission
     *
     * @param  \App\Models\Security\User  $user
     * @return boolean
     */
    public function viewAny(User $user)
    {
        return $user->hasPermission('roles.create')||$user->hasPermission('roles.update');
    }
}
