<?php

namespace App\Policies\System;

use App\Models\Security\User;
use App\Models\System\Condition;
use Illuminate\Auth\Access\HandlesAuthorization;

class ConditionPolicy
{
    use HandlesAuthorization;

    /**
     * Permiso para el metodo index para el modelo Condition
     *
     * @param  \App\Models\Security\User  $user
     * @return boolean
     */
    public function viewAny(User $user)
    {
        return $user->hasPermission('sunatinvoices.list');
    }

    /**
     * Permiso para el metodo show para el modelo Condition
     *
     * @param  \App\Models\Security\User  $user
     * @param  \App\Models\Security\Condition  $condition
     * @return boolean
     */
    public function view(User $user, Condition $condition)
    {
        return $user->hasPermission('sunatinvoices.show');
    }

    /**
     * Permiso para el metodo create para el modelo Condition
     *
     * @param  \App\Models\Security\User  $user
     * @return boolean
     */
    public function create(User $user)
    {
        return $user->hasPermission('sunatinvoices.create');
    }

    /**
     * Permiso para el metodo update para el modelo Condition
     *
     * @param  \App\Models\Security\User  $user
     * @param  \App\Models\Security\Condition  $condition
     * @return boolean
     */
    public function update(User $user, Condition $condition)
    {
        return $user->hasPermission('sunatinvoices.update');
    }

    /**
     * Permiso para el metodo delete para el modelo Condition
     *
     * @param  \App\Models\Security\User  $user
     * @param  \App\Models\Security\Condition  $condition
     * @return boolean
     */
    public function delete(User $user, Condition $condition)
    {
        return $user->hasPermission('sunatinvoices.delete');
    }

    /**
     * Permiso para el metodo reenviar para el modelo Condition
     *
     * @param  \App\Models\Security\User  $user
     * @param  \App\Models\Security\Condition  $condition
     * @return boolean
     */
    public function resend(User $user)
    {
        return $user->hasPermission('sunatinvoices.resend');
    }
}
