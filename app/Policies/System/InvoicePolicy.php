<?php

namespace App\Policies\System;

use App\Models\Security\User;
use App\Models\System\Invoice;
use Illuminate\Auth\Access\HandlesAuthorization;

class InvoicePolicy
{
    use HandlesAuthorization;

    /**
     * Permiso para el metodo index para el modelo Invoice
     *
     * @param  \App\Models\Security\User  $user
     * @return boolean
     */
    public function viewAny(User $user)
    {
        return $user->hasPermission('invoices.list');
    }

    /**
     * Permiso para el metodo show para el modelo Invoice
     *
     * @param  \App\Models\Security\User  $user
     * @param  \App\Models\Security\Invoice  $invoice
     * @return boolean
     */
    public function view(User $user, Invoice $invoice)
    {
        return $user->hasPermission('invoices.show');
    }

    /**
     * Permiso para el metodo create para el modelo Invoice
     *
     * @param  \App\Models\Security\User  $user
     * @return boolean
     */
    public function create(User $user)
    {
        return $user->hasPermission('invoices.create');
    }

    /**
     * Permiso para el metodo update para el modelo Invoice
     *
     * @param  \App\Models\Security\User  $user
     * @param  \App\Models\Security\Invoice  $invoice
     * @return boolean
     */
    public function update(User $user, Invoice $invoice)
    {
        return $user->hasPermission('invoices.update');
    }

    /**
     * Permiso para el metodo delete para el modelo Invoice
     *
     * @param  \App\Models\Security\User  $user
     * @param  \App\Models\Security\Invoice  $invoice
     * @return boolean
     */
    public function delete(User $user, Invoice $invoice)
    {
        return $user->hasPermission('invoices.delete');
    }

    /**
     * Permiso para el metodo savePermissions para el modelo Invoice
     *
     * @param  \App\Models\Security\User  $user
     * @param  \App\Models\Security\Invoice  $invoice
     * @return boolean
     */
    public function savePermissions(User $user, Invoice $invoice)
    {
        return $user->hasPermission('invoices.permissions');
    }

    /**
     * Permiso para el metodo list para el modelo Invoice
     *
     * @param  \App\Models\Security\User  $user
     * @return boolean
     */
    public function list(User $user)
    {
        return $user->hasPermission('invoices.list');
    }
}
