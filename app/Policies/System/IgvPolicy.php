<?php

namespace App\Policies\System;

use App\Models\Security\User;
use App\Models\System\Igv;
use Illuminate\Auth\Access\HandlesAuthorization;

class IgvPolicy
{
    use HandlesAuthorization;

    /**
     * Permiso para el metodo index para el modelo Igv
     *
     * @param  \App\Models\Security\User  $user
     * @return boolean
     */
    public function viewAny(User $user)
    {
        return $user->hasPermission('igvs.list');
    }

    /**
     * Permiso para el metodo show para el modelo Igv
     *
     * @param  \App\Models\Security\User  $user
     * @param  \App\Models\Security\Igv  $invoice
     * @return boolean
     */
    public function view(User $user, Igv $invoice)
    {
        return $user->hasPermission('igvs.show');
    }

    /**
     * Permiso para el metodo create para el modelo Igv
     *
     * @param  \App\Models\Security\User  $user
     * @return boolean
     */
    public function create(User $user)
    {
        return $user->hasPermission('igvs.create');
    }

    /**
     * Permiso para el metodo update para el modelo Igv
     *
     * @param  \App\Models\Security\User  $user
     * @param  \App\Models\Security\Igv  $invoice
     * @return boolean
     */
    public function update(User $user, Igv $invoice)
    {
        return $user->hasPermission('igvs.update');
    }

    /**
     * Permiso para el metodo delete para el modelo Igv
     *
     * @param  \App\Models\Security\User  $user
     * @param  \App\Models\Security\Igv  $invoice
     * @return boolean
     */
    public function delete(User $user, Igv $invoice)
    {
        return $user->hasPermission('igvs.delete');
    }
}
