<?php

namespace App\Policies\System;

use App\Models\Security\User;
use App\Models\System\SunatInvoice;
use Illuminate\Auth\Access\HandlesAuthorization;

class SunatInvoicePolicy
{
    use HandlesAuthorization;

    /**
     * Permiso para el metodo index para el modelo SunatInvoice
     *
     * @param  \App\Models\Security\User  $user
     * @return boolean
     */
    public function viewAny(User $user)
    {
        return $user->hasPermission('sunatinvoices.list');
    }

    /**
     * Permiso para el metodo show para el modelo SunatInvoice
     *
     * @param  \App\Models\Security\User  $user
     * @param  \App\Models\Security\SunatInvoice  $invoice
     * @return boolean
     */
    public function view(User $user, SunatInvoice $invoice)
    {
        return $user->hasPermission('sunatinvoices.show');
    }

    /**
     * Permiso para el metodo create para el modelo SunatInvoice
     *
     * @param  \App\Models\Security\User  $user
     * @return boolean
     */
    public function create(User $user)
    {
        return $user->hasPermission('sunatinvoices.create');
    }

    /**
     * Permiso para el metodo update para el modelo SunatInvoice
     *
     * @param  \App\Models\Security\User  $user
     * @param  \App\Models\Security\SunatInvoice  $invoice
     * @return boolean
     */
    public function update(User $user, SunatInvoice $invoice)
    {
        return $user->hasPermission('sunatinvoices.update');
    }

    /**
     * Permiso para el metodo delete para el modelo SunatInvoice
     *
     * @param  \App\Models\Security\User  $user
     * @param  \App\Models\Security\SunatInvoice  $invoice
     * @return boolean
     */
    public function delete(User $user, SunatInvoice $invoice)
    {
        return $user->hasPermission('sunatinvoices.delete');
    }

    /**
     * Permiso para el metodo savePermissions para el modelo SunatInvoice
     *
     * @param  \App\Models\Security\User  $user
     * @param  \App\Models\Security\SunatInvoice  $invoice
     * @return boolean
     */
    public function savePermissions(User $user, SunatInvoice $invoice)
    {
        return $user->hasPermission('sunatinvoices.permissions');
    }

    /**
     * Permiso para el metodo list para el modelo SunatInvoice
     *
     * @param  \App\Models\Security\User  $user
     * @return boolean
     */
    public function cancel(User $user)
    {
        return $user->hasPermission('sunatinvoices.cancel');
    }
}
