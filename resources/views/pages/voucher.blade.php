
<div class="text-center texto">
    <h3 class="separate">FARMACIA</h3>
    <h1 class="separate">La Vida</h1>
    <h4 class="separate">Direccion de la empresa 123</h4>
    @if ($sale->type=="Boleta" || $sale->type=="Factura")
        <h3 class="separate">
            {{$sale->type}} Electrónica:{{substr($sale->type,0,1).$sale->sunatInvoices[0]['serie']}}-{{$sale->sunatInvoices[0]['correlative']}} <br>
        </h3>
        <h4 class="separate">
            Fecha:{{\Carbon\Carbon::parse($sale->date)->format('d/m/Y')}}&nbsp;&nbsp;&nbsp;
            Hora: {{$sale->sunatInvoices[0]->time}}
        <hr>
        <p>
            <b>{{$sale->client->documentType}}:</b>{{$sale->client->documentNumber}}<br>
            <b>Cliente:</b>{{$sale->client->fullName}} <br>
            <b>Direccion:</b>{{$sale->client->address}}
        </p>
        <hr>
        <table class="table">
        <tr>
            <th>CANT.</th>
            <th>DESCRIPCIÓN</th>
            <th>PU</th>
            <th>TOTAL</th>
        </tr>
        @foreach($sale->sunatInvoices[0]['details'] as $detail)
        
        <tr>
            <td class="text-center">{{$detail->quantity}}</td>
            @if($detail->presentationProduct->presentation->name=='Individual')
                <td>{{$detail->presentationProduct->product->name}}
                {{'unidad'}}</td>
            @else
                <td>{{$detail->presentationProduct->product->name}}
                {{$detail->presentationProduct->presentation->name}}
                ({{$detail->presentationProduct->quantity}}{{'unidades'}})
                </td>
            @endif
            <td class="text-center">S/.{{$detail->unitPrice}}</td>
            <td class="text-center">S/.{{$detail->subTotal}}</td>
        </tr>
        @endforeach
        </table>
        <hr>
        <br>
        <p class="text-right">
            <b>Sub Total = </b>S/. {{$sale->sunatInvoices[0]->total-$sale->sunatInvoices[0]->igv}} <br>
            ------------------------------------------------------- <br>
            <b>Op. Gravada = </b>S/. {{$sale->sunatInvoices[0]->totalSaleGravada}} <br>
            <b>Op. Exonerada = </b>S/. {{$sale->sunatInvoices[0]->totalExonerated}} <br>
            <b>Op. Inafecta = </b> S/. {{$sale->sunatInvoices[0]->totalUnaffected}} <br>
            ------------------------------------------------------- <br>
            <b>IGV = </b> S/. {{$sale->sunatInvoices[0]->igv}} <br>
            <b>TOTAL = </b>S/.{{$sale->total}} <br>
            <b></b>
        </p>
        @if(!file_exists(App\Models\System\sunatInvoice::QRCODE_PATH.$sale->sunatInvoices[0]->id.".svg"))
            {{$sale->sunatInvoices[0]->getQR()}}
        @endif
        <img src="{{App\Models\System\sunatInvoice::QRCODE_PATH.$sale->sunatInvoices[0]->id.".svg"}}" alt="qr">
        <p>{{$sale->sunatInvoices[0]->hash}}</p>
        <p>Representación impresa de la BOLETA DE VENTA ELECTRÓNICA generada desde el sistema facturador SUNAT. Puede verificarla utilizando su clave SOL</p>
    @else
        <h3 class="separate">
            {{$sale->type}}:{{substr($sale->type,0,1).$sale->serie}}-{{$sale->correlative}}<br>
        </h3>
        <h4 class="separate">
            Fecha:{{\Carbon\Carbon::parse($sale->date)->format('d/m/Y')}}&nbsp;&nbsp;&nbsp;
        <hr>
        <p>
            <b>{{$sale->client->documentType}}:</b>{{$sale->client->documentNumber}}<br>
            <b>Cliente:</b>{{$sale->client->name}} <br>
            <b>Direccion:</b>{{$sale->client->address}}
        </p>
        <hr>
        <table class="table">
        <tr>
            <th>CANT.</th>
            <th>DESCRIPCIÓN</th>
            <th>PU</th>
            <th>TOTAL</th>
        </tr>
        @foreach($sale->saleDetails as $detail)
        
        <tr>
            <td class="text-center">{{$detail->quantity}}</td>
            @if($detail->presentationProduct->presentation->name=='Individual')
                <td>{{$detail->presentationProduct->product->name}}
                {{'unidad'}}</td>
            @else
                <td>{{$detail->presentationProduct->product->name}}
                {{$detail->presentationProduct->presentation->name}}
                ({{$detail->presentationProduct->quantity}}{{'unidades'}})
                </td>
            @endif
            <td class="text-center">S/.{{$detail->price}}</td>
            <td class="text-center">S/.{{$detail->price*$detail->quantity}}</td>
        </tr>
        @endforeach
        </table>
        <hr>
        <br>    
        <p class="text-right"><b>TOTAL: </b>S/.{{round($sale->total,2)}}</p>
        <br>
        <p>Este documento no es COMPROBANTE DE PAGO lo puede canjear por una Boleta al momento de cancelarlo</p>
    @endif
</div>

<style>
html {
  margin: 10px;
}
.text-center{
    text-align: center;
    font-family: sans-serif;
    font-size: 12px;
}
.separate{
    padding:0;
    margin:5px;
}
.text-right{
    text-align: right;
}
.table{
    font-size:12px;
}
th{
   border-bottom: 1px solid #999;
}
</style>