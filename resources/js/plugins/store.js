/**
 * Componente de Vuex
 */
export default {
    //strict:true,

    /** Variables globales */
    state: {
        user: {},
        role: {},
        snackbar: {
            visible: false,
            color: "success",
            text: null,
            timeout: 5000
        },
        unit: {
            name:'Unidad',
            plural:'Unidades',
        },
        personDefault: {},
        defaultIgv:0,
        security: {

            //permiso para menu gestion de ventas
            menuSale: 'menu.sale',

            //permisos para submenu ventas
            salesList: 'sales.list',
            salesCreate: 'sales.create',
            salesUpdate: 'sales.update',
            salesShow: 'sales.show',
            salesDelete: 'sales.delete',
            salesCreateDetail: 'sales.createdetail',
            salesDeleteDetail: 'sales.deletedetail',
            salesVoucher: 'sales.voucher',

            //permisos para submenu clientes
            clientsList: 'clients.list',
            clientsCreate: 'clients.create',
            clientsUpdate: 'clients.update',
            clientsShow: 'clients.show',
            clientsDelete: 'clients.delete',

            //permisos para submenu estado de envio
            sunatinvoicesList: 'sunatinvoices.list',
            sunatinvoicesVoucher: 'sunatinvoices.voucher',
            sunatinvoicesResend: 'sunatinvoices.resend',

            //permiso para menu gestion de compras
            menuBuy: 'menu.buy',

            //permisos para submenu compras
            buysList: 'buys.list',
            buysCreate: 'buys.create',
            buysUpdate: 'buys.update',
            buysShow: 'buys.show',
            buysDelete: 'buys.delete',
            buysCreateDetail: 'buys.createdetail',
            buysdeleteDetail: 'buys.deletedetail',

            //permisos para submenu proveedores
            suppliersList: 'suppliers.list',
            suppliersCreate: 'suppliers.create',
            suppliersUpdate: 'suppliers.update',
            suppliersShow: 'suppliers.show',
            suppliersDelete: 'suppliers.delete',

            //permiso para menu administrar productos
            menuProduct: 'menu.product',

            //permisos para submenu productos
            productsList: 'products.list',
            productsCreate: 'products.create',
            productsUpdate: 'products.update',
            productsShow: 'products.show',
            productsDelete: 'products.delete',
            productsPicture: 'products.picture',

            //permisos para submenu presentaciones
            presentationsList: 'presentations.list',
            presentationsCreate: 'presentations.create',
            presentationsUpdate: 'presentations.update',
            presentationsShow: 'presentations.show',
            presentationsDelete: 'presentations.delete',

            //permisos para submenu categorías
            categoriesList: 'categories.list',
            categoriesCreate: 'categories.create',
            categoriesUpdate: 'categories.update',
            categoriesShow: 'categories.show',
            categoriesDelete: 'categories.delete',

            //permisos para submenu laboratorios
            laboratoriesList: 'laboratories.list',
            laboratoriesCreate: 'laboratories.create',
            laboratoriesUpdate: 'laboratories.update',
            laboratoriesShow: 'laboratories.show',
            laboratoriesDelete: 'laboratories.delete',

            //permiso para menu reportes
            menuReport: 'menu.report',

            //permisos para submenu roles
            reportsSales: 'reports.sales',
            reportsBuys: 'reports.buys',
            reportsDayProducts: 'reports.dayproducts',
            reportsProducts: 'reports.products',
            reportsExpenses: 'reports.expenses',
            reportsBalance: 'reports.balance',

            //permiso para menu gastos
            menuSecurity: 'menu.expense',

            //permisos para submenu roles
            expensesList: 'expenses.list',
            expensesCreate: 'expenses.create',
            expensesUpdate: 'expenses.update',
            expensesShow: 'expenses.show',
            expensesDelete: 'expenses.delete',

            //permiso para menu seguridad
            menuSecurity: 'menu.security',

            //permisos para submenu roles
            rolesList: 'roles.list',
            rolesCreate: 'roles.create',
            rolesUpdate: 'roles.update',
            rolesShow: 'roles.show',
            rolesDelete: 'roles.delete',
            rolesPermission: 'roles.permissions',

            //permisos para submenu usuarios
            usersList: 'users.list',
            usersCreate: 'users.create',
            usersUpdate: 'users.update',
            usersShow: 'users.show',
            usersDelete: 'users.delete',
            usersPicture: 'users.picture',
            usersRole: 'users.role',

            //permiso para menu administrar productos
            menuConfig: 'menu.config',

            //permisos para submenu facturacion
            invoiceList: 'invoice.list',
            invoiceUpdate: 'invoice.update',
            invoiceShow: 'invoice.show',

            //permisos para submenu IGV
            igvsList: 'igvs.list',
            igvsCreate: 'igvs.create',
            igvsUpdate: 'igvs.update',
            igvsShow: 'igvs.show',
            igvsDelete: 'igvs.delete',
        }
    },

    /** Metodos para obtener valores de variables */
    getters: {
        user: state => state.user,
        role: state => state.role,
        snackbar: state => state.snackbar,
        unit: state => state.unit,
        personDefault: state => state.personDefault,
        defaultIgv:state => state.defaultIgv,
        security: state=>state.security,
        can: state => permission =>{
            if (state.role.special == "all-access") {
                return true
            }

            if (state.role.special == "no-access") {
                return false
            }
            
            if (state.role.permission_actions) {
                for (let element of state.role.permission_actions) {
                    if (element.slug == permission) {
                        return true
                    }
                }
            }
            return false
        }
    },

    /** Metodos globales*/
    mutations: {
        /**
         * Mostrar el snackbar
         * 
         * Muetra los mensajes de confirmacion, validacion o error en un modal flotante
         * 
         * @param {Object} state instancia de variables globales
         * @return {object} playload parametros de configuracion del snackbar
         * 
         */
        showAlert(state, payload) {

            state.snackbar.text = payload.text;

            //color del snackbar
            if (payload.color) {
                state.snackbar.color = payload.color;
            }

            // Tiempo de duración del snakbar
            if (payload.timeout) {
                state.snackbar.timeout = payload.timeout;
            }

            state.snackbar.visible = true;
        },

        /**
         * Cerrar el snackbar
         * 
         * Cierra el modal flotante y reinicia los valores del modal
         * 
         * @param {Object} state instancia de variables globales
        */
        closeAlert(state) {
            state.snackbar.visible = false;
            state.snackbar.multiline = false;
            state.snackbar.text = null;
        },

        /**
         * Setear usuario logueado
         * 
         * Recibe un objeto con la informacion del usuario y se lo asigna al usuario logueado
         * 
         * @param {Object} state instancia de variables globales
         * @return {object} data datos del usuario logueado
        */
        setUser(state, data) {
            state.user = data
        },

        /**
         * Setear role seleccionado
         * 
         * Recibe un objeto con la informacion del rol seleccionado del usuario logueado
         * 
         * @param {Object} state instancia de variables globales
         * @return {object} data datos del rol seleccionado del usuario logueado
        */
        setRole(state, data) {
            state.role = data
        },

        /**
         * Setear persona(cliente/proveedor) por defecto
        */
        setPersonDefault(state, data) {
            state.personDefault = data
        },

        /**
         * Setear defaultIgv
        */
        setDefaultIgv(state, data) {
            state.defaultIgv = data
        },
    },
    actions: {

    }
}