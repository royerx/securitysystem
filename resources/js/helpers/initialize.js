/**
 * Intersectar error de axios
 * Permite intercectar los responses de axios para mostrar los diferentes tipos de errores
 * 
 * @export {initializes}
 * @param {object} store - instancia de vuex
 * @returns {Promise} mensaje de error
 */

import router from './router'

export function initialize(store) {
    axios.interceptors.response.use(null, (error) => {
        const msg = {
            text: '',
            color: 'error',
            timeout: 3000
        }
        switch (error.response.status) {
            case 400:
                msg.text = 'Alguna instrucción no se interpretó adecuadamente'
                store.commit('showAlert', msg)
                break;
            case 401:
            case 403:
                msg.text = 'No cuenta con los permisos para esta acción'
                store.commit('showAlert', msg)
                window.location.href = 'home'
                break;
            case 404:
                msg.text = 'Recurso no encontrado'
                store.commit('showAlert', msg)
                break;
            case 408:
                msg.text = 'Se ha agotado el tiempo de respuesta del servidor'
                store.commit('showAlert', msg)
                break;
            case 500:
                msg.text = 'Error interno del servidor intente nuevamente'
                store.commit('showAlert', msg)
                break;
            case 502:
                msg.text = 'Se a detectado una respuesta inválida del servidor'
                store.commit('showAlert', msg)
                break;
            case 503:
                msg.text = 'El servicio no entrega una respuesta'
                store.commit('showAlert', msg)
                break;
        }

        return Promise.reject(error)
    })
}