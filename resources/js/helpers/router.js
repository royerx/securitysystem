import Vue from 'vue'
import Router from 'vue-router'

/**
 * Configuracion de rutas
 * 
 * Permite configurar las rutas y sus respectivos componentes
 *
 * @export {router} instancia de vue-router
 */

Vue.use(Router)

const routes = [

    {
        path: '/home',
        name: 'home',
        component: require("../views/pages/profile/Index").default
    },

    {
        path: '/',
        component: require("../views/pages/profile/Index").default
    },

    //rutas menu seguridad
    {
        path: '/seguridad/roles',
        name: 'role',
        component: require('../views/pages/security/role/Index.vue').default
    },
    {
        path: '/seguridad/usuarios',
        name: 'user',
        component: require('../views/pages/security/user/Index.vue').default
    },

    //rutas menu productos
    {
        path: '/producto',
        name: 'product',
        component: require('../views/pages/product/product/Index').default
    },
    {
        path: '/productos/presentaciones',
        name: 'presentation',
        component: require('../views/pages/product/presentation/Index').default
    },
    {
        path: '/productos/categorias',
        name: 'category',
        component: require('../views/pages/product/category/Index').default
    },
    {
        path: '/productos/laboratorios',
        name: 'laboratory',
        component: require('../views/pages/product/laboratory/Index').default
    },

    //rutas menu compras
    {
        path: '/compra',
        name: 'buy',
        component: require('../views/pages/entry/buy/Index').default
    },
    {
        path: '/compras/proveedores',
        name: 'supplier',
        component: require('../views/pages/entry/supplier/Index').default
    },

    //rutas menu ventas
    {
        path: '/venta',
        name: 'sale',
        component: require('../views/pages/sale/sale/Index').default
    },
    {
        path: '/ventas/clientes',
        name: 'client',
        component: require('../views/pages/sale/client/Index').default
    },
    {
        path: '/facturacion',
        name: 'sunatinvoice',
        component: require('../views/pages/system/sunatInvoice/Index').default
    },

    //rutas menu reportes
    {
        path: '/reporte/ventas',
        name: 'salesReport',
        component: require('../views/pages/report/Sales').default
    },
    {
        path: '/reporte/compras',
        name: 'buysReport',
        component: require('../views/pages/report/Buys').default
    },
    {
        path: '/reporte/productos',
        name: 'productsReport',
        component: require('../views/pages/report/Products').default
    },
    {
        path: '/reporte/gastos',
        name: 'expensesReport',
        component: require('../views/pages/report/Expenses').default
    },
    {
        path: '/reporte/productos-diario',
        name: 'dayproductsReport',
        component: require('../views/pages/report/DayProducts').default
    },
    {
        path: '/reporte/balance',
        name: 'balanceReport',
        component: require('../views/pages/report/Balance').default
    },

    //rutas menu gastos
    {
        path: '/egresos',
        name: 'expense',
        component: require('../views/pages/account/expense/Index').default
    },

    //rutas menu configuración
    {
        path: '/facturacion',
        name: 'invoice',
        component: require('../views/pages/system/invoice/Index').default
    },
    {
        path: '/igv',
        name: 'igv',
        component: require('../views/pages/system/igv/Index').default
    },

];

const router = new Router({
    mode: 'history',
    routes: routes
});

export default router