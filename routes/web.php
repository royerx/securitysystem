<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AdminController;
use App\Http\Controllers\Security\LoginController;
use App\Http\Controllers\Security\PermissionController;
use App\Http\Controllers\Security\RoleController;
use App\Http\Controllers\Security\UserController;
use App\Http\Controllers\Product\ProductController;
use App\Http\Controllers\Product\PresentationController;
use App\Http\Controllers\Product\CategoryController;
use App\Http\Controllers\Product\LaboratoryController;
use App\Http\Controllers\Entry\SupplierController;
use App\Http\Controllers\Entry\BuyController;
use App\Http\Controllers\Sale\ClientController;
use App\Http\Controllers\Sale\SaleController;
use App\Http\Controllers\Report\ReportController;
use App\Http\Controllers\Account\ExpenseController;
use App\Http\Controllers\System\InvoiceController;
use App\Http\Controllers\System\SunatInvoiceController;
use App\Http\Controllers\System\IgvController;
use App\Http\Controllers\System\ConditionController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
*/

#Rutas para el inicio de sesion
Route::get('login',[LoginController::class,'index'])->name('login');
Route::post('login',[LoginController::class,'login']);
Route::get('logout',[LoginController::class,'logout'])->name('logout');
Route::get('opensfs',[AdminController::class,'openSFS'])->name('opnesfs');

#Rutas para la asignacion de rol y pagina de inicio: usa el middleware auth en el controlador
Route::get('/', [AdminController::class,'index']);
Route::get('home',[AdminController::class,'index'])->name('home');
Route::get('current',[AdminController::class,'currentUser'])->name('user.current');
Route::get('authroles',[AdminController::class,'userRoles'])->name('auth.roles');
Route::get('selectedrole',[AdminController::class,'selectedRole'])->name('role.selected');
Route::post('assign',[AdminController::class,'assignRole'])->name('role.assign');
Route::get('dashboard',[AdminController::class,'dashboard'])->name('dashboard');
Route::get('checksunatinvoice',[AdminController::class,'checkSunatInvoice'])->name('check');

Route::middleware(['auth'])->group(function(){

    #Rutas para el modulo de seguridad
    Route::get('permission',[PermissionController::class,'index'])->name('permission.index');

    Route::resource('role', RoleController::class)->except(['create', 'edit']);
    Route::get('roles',[RoleController::class,'list'])->name('role.list');
    Route::post('role/{role}/permission',[RoleController::class,'savePermissions'])->name('role.permission');

    Route::resource('user', UserController::class)->except(['create', 'edit']);
    Route::post('user/{user}/photo',[UserController::class,'updatePhoto'])->name('user.picture');
    Route::post('user/{user}/role',[UserController::class,'saveRoles'])->name('user.role');

    //Rutas para el modulo de administracion de productos
    
    #Rutas para módulo de productos
    Route::resource('product', ProductController::class)->except(['create', 'edit']);
    Route::get('products',[ProductController::class,'list'])->name('products.list');
    Route::post('product/{product}/photo',[ProductController::class,'updatePhoto'])->name('product.picture');
    Route::get('product-presentation',[ProductController::class,'listPresentation'])->name('product.listpresentation');
    Route::post('product/{product}/presentation',[ProductController::class,'createPresentation'])->name('product.createpresentation');
    Route::put('product/{product}/presentation',[ProductController::class,'updatePresentation'])->name('product.updatepresentation');
    Route::delete('product/{product}/presentation',[ProductController::class,'deletePresentation'])->name('product.deletepresentation');

    #Rutas para módulo de presentaciones de productos
    Route::resource('presentation', PresentationController::class)->except(['create', 'edit']);
    Route::get('presentations',[PresentationController::class,'list'])->name('presentation.list');

    #Rutas para módulo de categorías
    Route::resource('category', CategoryController::class)->except(['create', 'edit']);
    Route::get('categories',[CategoryController::class,'list'])->name('category.list');

    #Rutas para módulo de laboratorios
    Route::resource('laboratory', LaboratoryController::class)->except(['create', 'edit']);
    Route::get('laboratories',[LaboratoryController::class,'list'])->name('laboratory.list');

    #Rutas para el modulo de proveedores
    Route::resource('supplier', SupplierController::class)->except(['create', 'edit']);
    Route::get('suppliers',[SupplierController::class,'list'])->name('supplier.list');

    #Rutas para el modulo de compras
    Route::resource('buy', BuyController::class)->except(['create', 'edit']);

    #Rutas para el modulo de clientes
    Route::resource('client', ClientController::class)->except(['create', 'edit']);
    Route::get('clients',[ClientController::class,'list'])->name('client.list');
    
    #Rutas para el modulo ventas
    Route::resource('sale', SaleController::class)->except(['create', 'edit']);
    Route::post('sale/{sale}/detail',[SaleController::class,'createDetail'])->name('sale.createdetail');
    Route::delete('sale/{sale}/detail',[SaleController::class,'deleteDetail'])->name('sale.deletedetail');
    Route::get('voucher/{sale}',[SaleController::class,'voucher'])->name('sale.voucher');
    Route::get('sales-report',[ReportController::class,'salesReport'])->name('report.sales');

    Route::get('generate-document-number',[SaleController::class,'generateDocumentNumber'])->name('sale.generatedocumentnumber');
    Route::get('verify-document-number',[SaleController::class,'verifyDocumentNumber'])->name('sale.verifydocumentnumber');
    Route::get('verify-invoice-xml',[SaleController::class,'verifyInvoiceXml'])->name('sale.verifyinvoicexml');
    Route::get('generate-invoice',[SaleController::class,'generateInvoice'])->name('sale.generateinvoice');
    Route::get('send-invoice',[SaleController::class,'sendInvoice'])->name('sale.sendinvoice');
    

    #Rutas para el móduo de reportes
    Route::get('sales-report',[ReportController::class,'salesReport'])->name('report.sales')->middleware('permission:reports.sales'); 
    Route::get('buys-report',[ReportController::class,'buysReport'])->name('report.buys')->middleware('permission:reports.buys'); 
    Route::get('products-report',[ReportController::class,'productsReport'])->name('report.produtcs')->middleware('permission:reports.products'); 
    Route::get('expenses-report',[ReportController::class,'expensesReport'])->name('report.expenses')->middleware('permission:reports.expenses'); 
    Route::get('dayproducts-report',[ReportController::class,'dayProductsReport'])->name('report.dayproducts')->middleware('permission:reports.dayproducts'); 
    Route::get('balance-report',[ReportController::class,'balanceReport'])->name('report.balance')->middleware('permission:reports.balance'); 

    #Rutas para el modulo de egresos
    Route::resource('expense', ExpenseController::class)->except(['create', 'edit']);
    
    #Rutas para el modulo de parametros de facturacion
    Route::resource('invoice', InvoiceController::class)->only(['index', 'show','update']);

    #Rutas para el modulo de IGV
    Route::resource('igv', IgvController::class)->except(['create', 'edit']);

    #Rutas para el modulo de documentos electronicos
    Route::resource('sunatinvoice', ConditionController::class)->only(['index','show']);
    Route::put('sunatinvoice/{sunatInvoice}', [ConditionController::class,'resend'])->name('sunatinvoice.resend');
});

Route::fallback(function () {
    return redirect()->route('home');
});